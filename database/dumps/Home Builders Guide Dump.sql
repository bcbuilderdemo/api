-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 52.60.104.219    Database: bcBuilder
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `home_builders_guide`
--

DROP TABLE IF EXISTS `home_builders_guide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home_builders_guide` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sctID` int(10) NOT NULL DEFAULT '1',
  `stepNum` int(11) NOT NULL,
  `stepName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stepDescription` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `home_builders_guide_stepnum_unique` (`stepNum`),
  KEY `home_builders_guide_sctid_foreign` (`sctID`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_builders_guide`
--

LOCK TABLES `home_builders_guide` WRITE;
/*!40000 ALTER TABLE `home_builders_guide` DISABLE KEYS */;
INSERT INTO `home_builders_guide` VALUES (1,2,1,'Architect \n','You need to hire a home drafting company to design the home plan that could suit your needs. Fred suggestions you might consider: open layouts & functional plans are very appealing, large windows bathe your home in light, high and designed ceiling draw a lot of attention, natural and built in lighting, windows and door trim provides the extra edge, a well-planned kitchen that supplements the surroundings and home style. \n',NULL,NULL),(2,84,2,'Truss Layout\n','After your home planner is done with the design, you should get the truss layout from your truss company\n',NULL,NULL),(3,23,3,'Engineering\n','Have your structural engineer prepare the design for building safety. \n',NULL,NULL),(4,80,4,'Surveyor\n','The surveyors will be the first one to go to the job site for marking, the excavator dig out. The surveyor has to come back again to mark it for the foundation. \nNote: your local bylaw might require you put a fence around your building area for safety. You can order your portable toilet to be at the job site at this time. \n',NULL,NULL),(5,19,5,'Excavator\n','Excavator will come and dig out the area the surveyor has pinned off. \n',NULL,NULL),(6,34,6,'Footings and Foundation (Framer)\n','Ensure your lumber supplier deliver the materials when framer starts the footing job. Also discuss the forms for the foundation with your framer, as you need to order it from the form rental company. \nNote: it is recommended to hire a cleanup contractor to keep the job site clean in a timely manner, keep in mind that most of the city inspectors refuse to inspect if the site is not safe. \n',NULL,NULL),(7,33,7,'Form Rental','No description provided',NULL,NULL),(8,76,8,'Steel Rebar','It is time for the rebar contractor to place the rebar. Then get the engineer for inspection and all the city hall for forms inspection. \rNote: try to line up the concrete ready mix beforehand (mostly concrete business like to have your account open, as it gets you a better price and non-COD terms\r',NULL,NULL),(9,14,9,'Concrete Supplier\n','Have your concrete placers place the concrete into the forms. Taking into account the weather, give it a minimum 24 hours to dry. \nNote: you should line up your form rental company to pick up forms. \n',NULL,NULL),(10,19,10,'Drain tile & inspection (Excavation)\r','Excavator will come for drain tiles and you should book city hall inspection as it gets done. \n',NULL,NULL),(11,17,11,'Damp proofing\r','Damp proofing is been done on the foundation which is below grade level. Most of the rebar contractors do the damp proofing. \n',NULL,NULL),(12,26,12,'Backfill and Grade (Excavator)\r','Your excavation contractor has to come again o do the backfill and grading of the lot. \n',NULL,NULL),(13,34,13,'Framing (Framer)\n','Make sure your lumber materials at the job site as your framer start the framing. You might want to place the truss order tat this point, generally truss suppliers take a bit of time. \rNote: discuss the time line with your framer for doors and windows\r',NULL,NULL),(14,84,14,'Trusses (Truss Layout)\r','Make sure your truss company delivers the trusses at the roof level otherwise you need a crane to get the trusses to the roof. \nNote: it would be a good idea to have your plumber and cabinet contractors to visit the job site, in case any alterations need to be done with framing. \n',NULL,NULL),(15,1,15,'Exterior Doors and Windows\r','As you install the exterior doors and windows, your house gets to the locked up stage. \n',NULL,NULL),(16,70,16,'Roofing \r','Mostly the roof style, color and material are finalized on the plan, have your roofing contractor complete the job. \n',NULL,NULL),(17,21,17,'Electrical\r','Before your electrical contractor starts the job, have a walk through to discuss the outlets. \nNote: you might want to look into the appliance specs, as your electrician can do the rough in wiring accordingly. \n',NULL,NULL),(18,71,18,'Security Alarm (Electrical Low Voltage)\r','Have your low voltage and vacuum contractor to do the rough-in for security alarm, cable, telephone, and built-in vacuum. You can also discuss other options like sound system, home automation etc. \n',NULL,NULL),(19,1,19,'Built In Vacuum\r','No description provided',NULL,NULL),(20,1,20,'Plumbing/Heating/Air Conditioning\n','It’s a good idea to have a walk through with your plumbing, heating and air conditioning contractors for outlets. Mostly plumbers do the heating job as well. \nNote: your insulation contractor needs to place insulation behind bathtubs and fireplaces points. \n',NULL,NULL),(21,1,21,'Slab or Skim Coat (Concrete)\r','Depending on the weather you can get the skim coat or slab poured\n',NULL,NULL),(22,1,22,'Fire Place\r','Have your fire place contractor to install the fire place. \n',NULL,NULL),(23,50,23,'Insulation \r','It is time for your insulation contractor to get the insulation done and book your city hall inspection after. \n',NULL,NULL),(24,14,24,'Concrete driveway and garage floor (concrete supplier) \r','If the lot has been graded, you can get the concrete driveway, sidewalks and garage floor done. You can also do this later, as it not in the way of other trades for now. \r',NULL,NULL),(25,20,25,'Drywall\r','After your insulation inspection is passed, drywall is ready to be installed.\nNote: depending on the weather taping and sanding time could vary or you can look into the heat alternatives. \n',NULL,NULL),(26,74,26,'Siding and Gutters\n','As per your home plan have the siding contractor install the siding, soffits, gutters and exterior woodwork.\rNote: not all the siding contractors do the exterior woodwork; most finishing carpenters do the exteriors as well. \rNote: if the drywall is completely done, you can start the primer at this point. You might want to line up your interior doors for the painter.  \rSiding and gutters\r',NULL,NULL),(27,78,27,'Stucco and Stone Work\r','Based on weather conditions, stucco can be started and you can get the stone work done after this. \n',NULL,NULL),(28,36,28,'Garage door\r','You can have your garage door installed now or later.\r',NULL,NULL),(29,60,29,'Painting\r','Have your painter to begin the painting. Make sure your house is properly clean for the painter to start. \n',NULL,NULL),(30,1,30,'Plumbing/Heating/Boiler/Furnace (Plumber)\r','Your contractor can install boiler and furnace. \n',NULL,NULL),(31,28,31,'Finishing carpenter\r','Have your finishing carpenter to start installing the finishing materials. \nNote: you should coordinate between your painter and finishing carpenter, as one of the finishing materials installed after it gets painted. Also discuss the stair railing with your painter as some stair railing contractors like to have the painters finish it. \n',NULL,NULL),(32,81,32,'Tile Installer\r','Your Tiles installer should start work now. \n',NULL,NULL),(33,1,33,'Cabinets\r','It is a good idea to have your cabinet company install the boxes first and put on the doors after the countertops are done. \n',NULL,NULL),(34,1,34,'Counter Tops (Cabinets)\r','Have the countertop company install the countertops.\n',NULL,NULL),(35,1,35,'Woodwork and Stair Railings\r','Have your woodworking contractor to start the stair railing and other decorative woodwork. \n',NULL,NULL),(36,1,36,'Plumbing Fixtures (Plumber)\n','The plumber can install the fixtures now. \r',NULL,NULL),(37,1,37,'Cabinets and Painting (Cabinets)\r','The cabinet contractor can install the doors and painter can do the second coat. \r',NULL,NULL),(38,73,38,'Shower Doors Mirrors and Closet Organizer\r','Your shower door and glass contractor can install the shower doors, mirrors and closet organizers. \n',NULL,NULL),(39,21,39,'Lighting Fixtures (Electrical)\r','Have the electrical contractor install the lighting fixtures. \nNote: make sure the house is clean form fine dust, as it could get on the lighting fixtures. \n',NULL,NULL),(40,1,40,'Appliances\r','Get your appliances installed. \n',NULL,NULL),(41,1,41,'Final Plumbing Electrical Inspection \r','Book your final plumbing and electrical inspection \n',NULL,NULL),(42,1,42,'Exterior Railings (Railing Company)\r','Get the exterior railings installed. \n',NULL,NULL),(43,60,43,'Painter\r','Get your painter to complete the job with final coats. \n',NULL,NULL),(44,31,44,'Flooring\n','It is time for the flooring contractor to install the carpet, hardwood or laminate floors.\n',NULL,NULL),(45,55,45,'Landscaping\r','You need to get the landscaping done for the final inspection \nNote: it would be a good idea to have a walk through and mark deficiencies\n',NULL,NULL);
/*!40000 ALTER TABLE `home_builders_guide` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-08 20:05:08
