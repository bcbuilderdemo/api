CREATE DATABASE  IF NOT EXISTS `bcBuilder2` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bcBuilder2`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: bcBuilder2
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abilities`
--

DROP TABLE IF EXISTS `abilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `entity_type` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `only_owned` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `abilities_unique_index` (`name`,`entity_id`,`entity_type`,`only_owned`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abilities`
--

LOCK TABLES `abilities` WRITE;
/*!40000 ALTER TABLE `abilities` DISABLE KEYS */;
INSERT INTO `abilities` VALUES (1,'*',NULL,0,'App\\Models\\CompanyInfo\\Profile',0,'2018-01-30 17:34:56','2018-01-30 17:34:56'),(2,'*',NULL,1,'App\\Project',0,'2018-01-30 17:52:21','2018-01-30 17:52:21'),(3,'*',NULL,1,'App\\Models\\Chat\\Chat',0,'2018-01-30 17:52:21','2018-01-30 17:52:21');
/*!40000 ALTER TABLE `abilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounting`
--

DROP TABLE IF EXISTS `accounting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `builderID` int(10) unsigned NOT NULL,
  `scID` int(10) unsigned NOT NULL,
  `projectID` int(10) unsigned NOT NULL,
  `bidID` int(10) unsigned NOT NULL,
  `bboID` int(10) unsigned NOT NULL,
  `scboID` int(10) unsigned NOT NULL,
  `scTradeID` int(10) unsigned NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `totalAmount` double(20,2) NOT NULL,
  `amountPaidSoFar` double(20,2) NOT NULL DEFAULT '0.00',
  `amountRemaining` double(20,2) NOT NULL DEFAULT '0.00',
  `paidInFull` tinyint(1) NOT NULL DEFAULT '0',
  `lastPaymentMadeOn` datetime DEFAULT NULL,
  `paidInFullOn` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accounting_builderid_foreign` (`builderID`),
  KEY `accounting_scid_foreign` (`scID`),
  KEY `accounting_projectid_foreign` (`projectID`),
  KEY `accounting_bidid_foreign` (`bidID`),
  KEY `accounting_scboid_foreign` (`scboID`),
  KEY `accounting_sctradeid_foreign` (`scTradeID`),
  CONSTRAINT `accounting_bidid_foreign` FOREIGN KEY (`bidID`) REFERENCES `bids` (`id`) ON DELETE CASCADE,
  CONSTRAINT `accounting_builderid_foreign` FOREIGN KEY (`builderID`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `accounting_projectid_foreign` FOREIGN KEY (`projectID`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `accounting_scboid_foreign` FOREIGN KEY (`scboID`) REFERENCES `sc_bids_out` (`id`) ON DELETE CASCADE,
  CONSTRAINT `accounting_scid_foreign` FOREIGN KEY (`scID`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `accounting_sctradeid_foreign` FOREIGN KEY (`scTradeID`) REFERENCES `subcontractor_trades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounting`
--

LOCK TABLES `accounting` WRITE;
/*!40000 ALTER TABLE `accounting` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounting_payments_made`
--

DROP TABLE IF EXISTS `accounting_payments_made`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounting_payments_made` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `accountingID` int(10) unsigned NOT NULL,
  `amountPaid` double(20,2) NOT NULL,
  `scConfirmedPayment` tinyint(1) NOT NULL DEFAULT '0',
  `scConfirmedPaymentOn` datetime DEFAULT NULL,
  `paymentMadeOn` datetime NOT NULL DEFAULT '2018-01-30 17:34:48',
  `scUnConfirmedPaymentOn` datetime DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accounting_payments_made_accountingid_foreign` (`accountingID`),
  CONSTRAINT `accounting_payments_made_accountingid_foreign` FOREIGN KEY (`accountingID`) REFERENCES `accounting` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounting_payments_made`
--

LOCK TABLES `accounting_payments_made` WRITE;
/*!40000 ALTER TABLE `accounting_payments_made` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounting_payments_made` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assigned_roles`
--

DROP TABLE IF EXISTS `assigned_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assigned_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `entity_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  KEY `assigned_roles_entity_id_entity_type_index` (`entity_id`,`entity_type`),
  KEY `assigned_roles_role_id_index` (`role_id`),
  CONSTRAINT `assigned_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assigned_roles`
--

LOCK TABLES `assigned_roles` WRITE;
/*!40000 ALTER TABLE `assigned_roles` DISABLE KEYS */;
INSERT INTO `assigned_roles` VALUES (1,1,'App\\User'),(1,2,'App\\User'),(1,3,'App\\User'),(1,4,'App\\User'),(1,5,'App\\User'),(2,6,'App\\User');
/*!40000 ALTER TABLE `assigned_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bids`
--

DROP TABLE IF EXISTS `bids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bids` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(10) unsigned NOT NULL,
  `builderID` int(10) unsigned NOT NULL,
  `scTradeID` int(10) unsigned NOT NULL,
  `additionalDetails` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'None Specified',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bids_builderid_foreign` (`builderID`),
  KEY `bids_projectid_foreign` (`projectID`),
  KEY `bids_sctradeid_foreign` (`scTradeID`),
  CONSTRAINT `bids_builderid_foreign` FOREIGN KEY (`builderID`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `bids_projectid_foreign` FOREIGN KEY (`projectID`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `bids_sctradeid_foreign` FOREIGN KEY (`scTradeID`) REFERENCES `subcontractor_trades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bids`
--

LOCK TABLES `bids` WRITE;
/*!40000 ALTER TABLE `bids` DISABLE KEYS */;
/*!40000 ALTER TABLE `bids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `builder_bids_out`
--

DROP TABLE IF EXISTS `builder_bids_out`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `builder_bids_out` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bidID` int(10) unsigned NOT NULL,
  `projectID` int(10) unsigned NOT NULL,
  `builderID` int(10) unsigned NOT NULL,
  `scID` int(10) unsigned DEFAULT NULL,
  `scResponded` tinyint(1) NOT NULL DEFAULT '0',
  `scViewed` tinyint(1) NOT NULL DEFAULT '0',
  `scViewedAt` datetime DEFAULT NULL,
  `updated` tinyint(1) NOT NULL DEFAULT '0',
  `bbo_accepted` tinyint(1) DEFAULT NULL,
  `accepted` tinyint(1) DEFAULT NULL,
  `acceptedSCBOID` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `builder_bids_out_bidid_foreign` (`bidID`),
  KEY `builder_bids_out_scid_foreign` (`scID`),
  KEY `builder_bids_out_builderid_foreign` (`builderID`),
  KEY `builder_bids_out_projectid_foreign` (`projectID`),
  KEY `builder_bids_out_acceptedscboid_foreign` (`acceptedSCBOID`),
  CONSTRAINT `builder_bids_out_acceptedscboid_foreign` FOREIGN KEY (`acceptedSCBOID`) REFERENCES `sc_bids_out` (`id`) ON DELETE CASCADE,
  CONSTRAINT `builder_bids_out_bidid_foreign` FOREIGN KEY (`bidID`) REFERENCES `bids` (`id`) ON DELETE CASCADE,
  CONSTRAINT `builder_bids_out_builderid_foreign` FOREIGN KEY (`builderID`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `builder_bids_out_projectid_foreign` FOREIGN KEY (`projectID`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `builder_bids_out_scid_foreign` FOREIGN KEY (`scID`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `builder_bids_out`
--

LOCK TABLES `builder_bids_out` WRITE;
/*!40000 ALTER TABLE `builder_bids_out` DISABLE KEYS */;
/*!40000 ALTER TABLE `builder_bids_out` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `builders`
--

DROP TABLE IF EXISTS `builders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `builders` (
  `userID` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `builders_userid_foreign` (`userID`),
  CONSTRAINT `builders_userid_foreign` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `builders`
--

LOCK TABLES `builders` WRITE;
/*!40000 ALTER TABLE `builders` DISABLE KEYS */;
INSERT INTO `builders` VALUES (4,NULL,NULL);
/*!40000 ALTER TABLE `builders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scID` int(10) unsigned NOT NULL,
  `builderID` int(10) unsigned NOT NULL,
  `projectID` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `chat_uuid_unique` (`uuid`),
  KEY `chat_scid_foreign` (`scID`),
  KEY `chat_builderid_foreign` (`builderID`),
  KEY `chat_projectid_foreign` (`projectID`),
  CONSTRAINT `chat_builderid_foreign` FOREIGN KEY (`builderID`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chat_projectid_foreign` FOREIGN KEY (`projectID`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chat_scid_foreign` FOREIGN KEY (`scID`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
INSERT INTO `chat` VALUES (1,'rHZxqFZNjxh7',6,4,1,NULL,NULL);
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_uploads`
--

DROP TABLE IF EXISTS `chat_uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat_uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chatUUID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `builderID` int(10) unsigned NOT NULL,
  `profileID` int(10) unsigned NOT NULL,
  `scID` int(10) unsigned NOT NULL,
  `projectID` int(10) unsigned DEFAULT NULL,
  `sentByID` int(10) unsigned NOT NULL,
  `fileUUID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filePath` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileURL` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mimeType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileSize` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chat_uploads_scid_foreign` (`scID`),
  KEY `chat_uploads_builderid_foreign` (`builderID`),
  KEY `chat_uploads_sentbyid_foreign` (`sentByID`),
  KEY `chat_uploads_chatuuid_foreign` (`chatUUID`),
  KEY `chat_uploads_projectid_foreign` (`projectID`),
  CONSTRAINT `chat_uploads_builderid_foreign` FOREIGN KEY (`builderID`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chat_uploads_chatuuid_foreign` FOREIGN KEY (`chatUUID`) REFERENCES `chat` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `chat_uploads_projectid_foreign` FOREIGN KEY (`projectID`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chat_uploads_scid_foreign` FOREIGN KEY (`scID`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chat_uploads_sentbyid_foreign` FOREIGN KEY (`sentByID`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_uploads`
--

LOCK TABLES `chat_uploads` WRITE;
/*!40000 ALTER TABLE `chat_uploads` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat_uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_info`
--

DROP TABLE IF EXISTS `company_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_info` (
  `userID` int(10) unsigned NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `companySlug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactFirstName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactLastName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phoneNumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phoneNumber2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postalCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uuid` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logoURL` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `company_info_companyslug_unique` (`companySlug`),
  UNIQUE KEY `company_info_uuid_unique` (`uuid`),
  CONSTRAINT `company_info_userid_foreign` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_info`
--

LOCK TABLES `company_info` WRITE;
/*!40000 ALTER TABLE `company_info` DISABLE KEYS */;
INSERT INTO `company_info` VALUES (4,'Test Co','test-co2','testing','testing','1234 Fake Street','604 555 1234','604 123 1233','NULL','Surrey','V4N 3X4','BC','m7R4MQDNPF85QxMg','assets/images/no-image-found.png',NULL,'2018-01-30 17:42:44','2018-01-30 17:42:44'),(6,'Arc Co','arc-co','testing','testing','1234 Fake Street','604 555 1234','604 123 1233','NULL','Surrey','V4N 3X4','BC','WqYOkGpxK9j5jzaq','assets/images/no-image-found.png',NULL,'2018-01-30 17:47:53','2018-01-30 17:47:53');
/*!40000 ALTER TABLE `company_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_uploads`
--

DROP TABLE IF EXISTS `file_uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fileID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bidID` int(10) unsigned NOT NULL,
  `uploadsID` int(10) unsigned NOT NULL,
  `fileName` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uniqueFileName` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filePath` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileSize` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileURL` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mimeType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `file_uploads_bidid_foreign` (`bidID`),
  KEY `file_uploads_uploadsid_foreign` (`uploadsID`),
  CONSTRAINT `file_uploads_bidid_foreign` FOREIGN KEY (`bidID`) REFERENCES `uploads` (`bidID`) ON DELETE CASCADE,
  CONSTRAINT `file_uploads_uploadsid_foreign` FOREIGN KEY (`uploadsID`) REFERENCES `uploads` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_uploads`
--

LOCK TABLES `file_uploads` WRITE;
/*!40000 ALTER TABLE `file_uploads` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_builders_guide`
--

DROP TABLE IF EXISTS `home_builders_guide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home_builders_guide` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sctID` int(10) unsigned NOT NULL,
  `stepNum` int(11) NOT NULL,
  `stepName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stepDescription` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `home_builders_guide_stepnum_unique` (`stepNum`),
  KEY `home_builders_guide_sctid_foreign` (`sctID`),
  CONSTRAINT `home_builders_guide_sctid_foreign` FOREIGN KEY (`sctID`) REFERENCES `subcontractor_trades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_builders_guide`
--

LOCK TABLES `home_builders_guide` WRITE;
/*!40000 ALTER TABLE `home_builders_guide` DISABLE KEYS */;
/*!40000 ALTER TABLE `home_builders_guide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chatID` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `builderID` int(10) unsigned NOT NULL,
  `scID` int(10) unsigned NOT NULL,
  `sentByID` int(10) unsigned NOT NULL,
  `message` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileUploaded` tinyint(1) NOT NULL DEFAULT '0',
  `chatUploadID` int(10) unsigned DEFAULT NULL,
  `readByBuilder` tinyint(1) NOT NULL DEFAULT '0',
  `readByBuilderAt` datetime DEFAULT NULL,
  `readBySC` tinyint(1) NOT NULL DEFAULT '0',
  `readBySCAt` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `messages_scid_foreign` (`scID`),
  KEY `messages_builderid_foreign` (`builderID`),
  KEY `messages_sentbyid_foreign` (`sentByID`),
  KEY `messages_chatid_foreign` (`chatID`),
  KEY `messages_chatuploadid_foreign` (`chatUploadID`),
  CONSTRAINT `messages_builderid_foreign` FOREIGN KEY (`builderID`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `messages_chatid_foreign` FOREIGN KEY (`chatID`) REFERENCES `chat` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `messages_chatuploadid_foreign` FOREIGN KEY (`chatUploadID`) REFERENCES `chat_uploads` (`id`) ON DELETE CASCADE,
  CONSTRAINT `messages_scid_foreign` FOREIGN KEY (`scID`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `messages_sentbyid_foreign` FOREIGN KEY (`sentByID`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_09_06_222650_create_company_infos_table',1),(4,'2017_10_09_105426_create_subcontractor_trades_table',1),(5,'2017_10_10_171901_create_projects_table',1),(6,'2017_10_10_175758_create_bids_table',1),(7,'2017_10_24_080933_create_s_c_bids_outs_table',1),(8,'2017_10_24_081249_create_s_c_bids_out_line_items_table',1),(9,'2017_10_24_142427_create_bouncer_tables',1),(10,'2017_10_26_074356_create_builder_bids_outs_table',1),(11,'2017_10_30_081817_create_subcontractors_table',1),(12,'2017_11_06_142136_create_uploads_table',1),(13,'2017_11_07_115728_create_file_uploads_table',1),(14,'2017_11_09_101826_create_builders_table',1),(15,'2017_11_29_133913_create_report_unauthorized_activities_table',1),(16,'2017_12_01_151802_create_accountings_table',1),(17,'2017_12_01_152055_create_accounting__payments__mades_table',1),(18,'2017_12_11_082557_create_profiles_table',1),(19,'2017_12_11_082702_create_profile_uploads_table',1),(20,'2017_12_17_160309_create_chats_table',1),(21,'2017_12_17_160321_create_messages_table',1),(22,'2017_12_21_073517_create_chat_uploads_table',1),(23,'2017_12_25_144411_create_jobs_table',1),(24,'2017_12_25_144421_create_failed_jobs_table',1),(25,'2017_12_26_155147_create_notifications_table',1),(26,'2018_01_08_103800_create_home_builders_guides_table',1),(27,'2099_10_30_154017_CreateForeignKeys',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int(10) unsigned NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `ability_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `entity_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `forbidden` tinyint(1) NOT NULL DEFAULT '0',
  KEY `permissions_entity_id_entity_type_index` (`entity_id`,`entity_type`),
  KEY `permissions_ability_id_index` (`ability_id`),
  CONSTRAINT `permissions_ability_id_foreign` FOREIGN KEY (`ability_id`) REFERENCES `abilities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,1,'App\\User',0),(1,2,'App\\User',0),(1,3,'App\\User',0),(1,4,'App\\User',0),(1,5,'App\\User',0),(1,6,'App\\User',0),(2,4,'App\\User',0),(3,4,'App\\User',0),(3,6,'App\\User',0);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `userID` int(10) unsigned NOT NULL,
  `aboutUs` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitterURL` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebookURL` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagramURL` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pinterestURL` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtubeURL` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `googlePlusURL` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userID`),
  CONSTRAINT `profile_userid_foreign` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_uploads`
--

DROP TABLE IF EXISTS `profile_uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profileID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `fileID` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isLogo` tinyint(1) NOT NULL DEFAULT '0',
  `fileName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filePath` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileURL` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mimeType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileSize` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_uploads_userid_foreign` (`userID`),
  KEY `profile_uploads_profileid_foreign` (`profileID`),
  CONSTRAINT `profile_uploads_profileid_foreign` FOREIGN KEY (`profileID`) REFERENCES `profile` (`userID`) ON DELETE CASCADE,
  CONSTRAINT `profile_uploads_userid_foreign` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_uploads`
--

LOCK TABLES `profile_uploads` WRITE;
/*!40000 ALTER TABLE `profile_uploads` DISABLE KEYS */;
INSERT INTO `profile_uploads` VALUES (28,4,4,'08a71f60-0628-11e8-8d08-e331e6ffe1aa',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,4,4,'08a787e0-0628-11e8-8df0-2f846801c686',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,4,4,'08a7d2b0-0628-11e8-b1ee-4f1c1cbf81a2',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,4,4,'08a84af0-0628-11e8-948a-e3b2bb3b10a4',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,4,4,'08a8b690-0628-11e8-9053-c969c9fbe795',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,4,4,'08a8f7c0-0628-11e8-bad8-fd60b9201026',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,4,4,'08a94080-0628-11e8-8a7c-7bc42e6dad28',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(35,4,4,'08a98300-0628-11e8-b8b3-e70a200baa03',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(36,4,4,'08a9d220-0628-11e8-af60-f7956a637d7f',1,NULL,NULL,'assets/images/no-image-found.png',NULL,NULL,NULL,NULL,NULL),(46,6,6,'c07077a0-0628-11e8-bc22-93b4e2304070',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(47,6,6,'c070da00-0628-11e8-92af-a5aaaa890268',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(48,6,6,'c07119f0-0628-11e8-af82-030703c46906',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(49,6,6,'c071cc70-0628-11e8-af76-e34f506a5777',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50,6,6,'c0721620-0628-11e8-8e7c-ef7620ee314a',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(51,6,6,'c0725880-0628-11e8-a086-6566ba52771c',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(52,6,6,'c072a6f0-0628-11e8-b008-eb27e48cf1db',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(53,6,6,'c072e280-0628-11e8-b251-55851dbebb24',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(54,6,6,'c0732280-0628-11e8-ac32-8b818dd48c88',1,NULL,NULL,'assets/images/no-image-found.png',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `profile_uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(10) unsigned NOT NULL,
  `firstName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postalCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `landType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projectType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numUnits` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lotSize` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `buildingSize` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `buildingType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zoning` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `buildValue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_userid_foreign` (`userID`),
  CONSTRAINT `projects_userid_foreign` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,4,'TEST','TEST','1234 Fake Street',NULL,'Surrey','BC','V3J 4J4','Canada','TESTING','industrial','Landlord Improvements','123','123,123','123,123','Building','Addition','$12,312','','2018-01-27 18:52:21','2018-01-27 18:52:21');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_unauthorized_activities`
--

DROP TABLE IF EXISTS `report_unauthorized_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_unauthorized_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(10) unsigned NOT NULL,
  `message` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `report_unauthorized_activities_userid_foreign` (`userID`),
  CONSTRAINT `report_unauthorized_activities_userid_foreign` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_unauthorized_activities`
--

LOCK TABLES `report_unauthorized_activities` WRITE;
/*!40000 ALTER TABLE `report_unauthorized_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_unauthorized_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'builder',NULL,NULL,NULL,NULL),(2,'subcontractor',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_bids_out`
--

DROP TABLE IF EXISTS `sc_bids_out`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_bids_out` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bidID` int(10) unsigned NOT NULL,
  `scID` int(10) unsigned NOT NULL,
  `builderID` int(10) unsigned NOT NULL,
  `projectID` int(10) unsigned NOT NULL,
  `accountingID` int(10) unsigned DEFAULT NULL,
  `subTotal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taxTotal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grandTotal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated` tinyint(1) NOT NULL DEFAULT '0',
  `builderViewed` tinyint(1) NOT NULL DEFAULT '0',
  `builderViewedAt` datetime DEFAULT NULL,
  `builderAccepted` tinyint(1) DEFAULT NULL,
  `builder_accepted_at` datetime DEFAULT NULL,
  `builder_rejected_at` datetime DEFAULT NULL,
  `sc_read` tinyint(1) DEFAULT NULL,
  `sc_read_at` datetime DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'waiting',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sc_bids_out_bidid_foreign` (`bidID`),
  KEY `sc_bids_out_scid_foreign` (`scID`),
  KEY `sc_bids_out_builderid_foreign` (`builderID`),
  KEY `sc_bids_out_accountingid_foreign` (`accountingID`),
  KEY `sc_bids_out_projectid_foreign` (`projectID`),
  CONSTRAINT `sc_bids_out_accountingid_foreign` FOREIGN KEY (`accountingID`) REFERENCES `accounting` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sc_bids_out_bidid_foreign` FOREIGN KEY (`bidID`) REFERENCES `bids` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sc_bids_out_builderid_foreign` FOREIGN KEY (`builderID`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sc_bids_out_projectid_foreign` FOREIGN KEY (`projectID`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sc_bids_out_scid_foreign` FOREIGN KEY (`scID`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_bids_out`
--

LOCK TABLES `sc_bids_out` WRITE;
/*!40000 ALTER TABLE `sc_bids_out` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_bids_out` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sc_bids_out_line_items`
--

DROP TABLE IF EXISTS `sc_bids_out_line_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sc_bids_out_line_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bidID` int(10) unsigned NOT NULL,
  `scBidOutID` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taxRate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lineSubTotal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lineTaxTotal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lineTotal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sc_bids_out_line_items_bidid_foreign` (`bidID`),
  KEY `sc_bids_out_line_items_scbidoutid_foreign` (`scBidOutID`),
  CONSTRAINT `sc_bids_out_line_items_bidid_foreign` FOREIGN KEY (`bidID`) REFERENCES `bids` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sc_bids_out_line_items_scbidoutid_foreign` FOREIGN KEY (`scBidOutID`) REFERENCES `sc_bids_out` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sc_bids_out_line_items`
--

LOCK TABLES `sc_bids_out_line_items` WRITE;
/*!40000 ALTER TABLE `sc_bids_out_line_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `sc_bids_out_line_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcontractor_trades`
--

DROP TABLE IF EXISTS `subcontractor_trades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcontractor_trades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tradeName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `sctNameSlug` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `imageName` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imageLocation` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imageURL` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imageType` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subcontractor_trades_tradename_unique` (`tradeName`),
  UNIQUE KEY `subcontractor_trades_sctnameslug_unique` (`sctNameSlug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcontractor_trades`
--

LOCK TABLES `subcontractor_trades` WRITE;
/*!40000 ALTER TABLE `subcontractor_trades` DISABLE KEYS */;
INSERT INTO `subcontractor_trades` VALUES (1,'Architect','NA','architect',NULL,NULL,NULL,NULL,NULL,NULL),(2,'Plumber','NA','plumber',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `subcontractor_trades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcontractors`
--

DROP TABLE IF EXISTS `subcontractors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcontractors` (
  `userID` int(10) unsigned NOT NULL,
  `scTradeID` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userID`),
  KEY `subcontractors_sctradeid_foreign` (`scTradeID`),
  CONSTRAINT `subcontractors_sctradeid_foreign` FOREIGN KEY (`scTradeID`) REFERENCES `subcontractor_trades` (`id`) ON DELETE CASCADE,
  CONSTRAINT `subcontractors_userid_foreign` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcontractors`
--

LOCK TABLES `subcontractors` WRITE;
/*!40000 ALTER TABLE `subcontractors` DISABLE KEYS */;
INSERT INTO `subcontractors` VALUES (6,1,NULL,NULL);
/*!40000 ALTER TABLE `subcontractors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uploads`
--

DROP TABLE IF EXISTS `uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bidID` int(10) unsigned NOT NULL,
  `userID` int(10) unsigned NOT NULL,
  `projectID` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uploads_bidid_foreign` (`bidID`),
  KEY `uploads_projectid_foreign` (`projectID`),
  CONSTRAINT `uploads_bidid_foreign` FOREIGN KEY (`bidID`) REFERENCES `bids` (`id`) ON DELETE CASCADE,
  CONSTRAINT `uploads_projectid_foreign` FOREIGN KEY (`projectID`) REFERENCES `projects` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uploads`
--

LOCK TABLES `uploads` WRITE;
/*!40000 ALTER TABLE `uploads` DISABLE KEYS */;
/*!40000 ALTER TABLE `uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trade` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `isSubContractor` tinyint(1) NOT NULL DEFAULT '0',
  `isBuilder` tinyint(1) NOT NULL DEFAULT '0',
  `userType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hasCreatedAProject` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (4,'testing@testing.com','$2y$10$3RhFT2ne3cb2l/4rmH23we6Qx8Ig6Z0wPJSAEFCNZONmUgl5b0.RS',NULL,0,0,1,'builder',1,'e3e60cb0fe6798aea955500539a2a85e80f27724cdfd7c4e6c44183e22ca72ff',1,0,0,NULL,NULL,NULL),(6,'architect@architect.com','$2y$10$x5cDGCl.Qm00y70WLBxcZOur1dFmJo3q9zwH38wSftmdyNTORBNQK',NULL,0,1,0,'subcontractor',1,'23d0b75a620186b15f83b7443db5495db70de0bf64b388f03c0ca7f1e2ac42f7',0,0,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-30 18:00:15
