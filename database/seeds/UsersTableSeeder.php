<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('testing123!'),
        ]);

        DB::table('users')->insert([
            'email' => 'admin@admin.com',
            'password' => bcrypt('testing123!'),
            'isAdmin' => true,
            'verified' => true
        ]);

        DB::table('users')->insert([
            'email' => 'user@user.com',
            'password' => bcrypt('testing123!'),
        ]);

        DB::table('users')->insert([
            'email' => 'company@company.com',
            'password' => bcrypt('testing123!'),
        ]);
    }
}
