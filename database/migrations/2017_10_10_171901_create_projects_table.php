<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $this->down();

        Schema::create('projects', function (Blueprint $table) {

            $table->engine = 'InnoDB';


            $table->increments('id');
            $table->integer('userID')->unsigned();
            $table->string('firstName');
            $table->string('lastName');
            $table->string('address');
            $table->string('address2')->nullable();
            $table->string('city');
            $table->string('province');
            $table->string('postalCode');
            $table->string('country');
            $table->string('description');
            $table->string('landType');
            $table->string('projectType');
            $table->string('numUnits')->nullable();
            $table->string('lotSize');
            $table->string('buildingSize');
            $table->string('buildingType');
            $table->string('zoning');
            $table->string('buildValue');
            $table->string('details');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
