<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSCBidsOutLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('sc_bids_out_line_items', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('bidID')->unsigned();
            $table->integer('scBidOutID')->unsigned();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('quantity');
            $table->string('uom')->nullable();
            $table->string('taxRate')->nullable();
            $table->string('price');
            $table->string('lineSubTotal');
            $table->string('lineTaxTotal');
            $table->string('lineTotal');
            $table->boolean('deleted')->default(0);
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_bids_out_line_items');
    }
}
