<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PostFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('posts', function (Blueprint $table) {
            $table->string('dirID');
        });

        Schema::create('post_files', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('postID')->unsigned();
            $table->string('fileID')->nullable()->default(null);
            $table->string('fileName')->nullable()->default(null);
            $table->string('fileType')->nullable()->default(null);
            $table->string('filePath')->nullable()->default(null);
            $table->string('fileSize', 25)->nullable()->default(null);
            $table->string('iconURL')->nullable()->default(null);
            $table->string('mimeType')->nullable()->default(null);

            $table->boolean('deleted')->default(false);

            $table->foreign('postID')
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_files');
    }
}
