<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            //$table->increments('id');
            $table->integer('userID')->unsigned();
            $table->string('aboutUs', 2000)->nullable()->default(null);
            $table->string('website')->nullable()->default(null);
            $table->string('twitterURL')->nullable()->default(null);
            $table->string('facebookURL')->nullable()->default(null);
            $table->string('instagramURL')->nullable()->default(null);
            $table->string('pinterestURL')->nullable()->default(null);
            $table->string('youtubeURL')->nullable()->default(null);
            $table->string('googlePlusURL')->nullable()->default(null);
            $table->timestamps();

            $table->primary('userID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
