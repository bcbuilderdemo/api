<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('bids', function (Blueprint $table) {
            // $table->dropForeign('bids_sctradeid_foreign');
            // $table->dropColumn('scTradeID');
            // $table->dropColumn('additionalDetails');
        });

        Schema::table('bids', function (Blueprint $table) {
            $table->integer('subprojectID')->unsigned();
            $table->foreign('subprojectID')
                ->references('id')
                ->on('subprojects')
                ->onDelete('cascade');
            $table->text('comments')->nullable()->default(null);
            $table->decimal('amount', 10, 2);
            $table->enum('status', array('accepted', 'rejected', 'renegotiate'))->nullable()->default(null);
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
