<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chatUUID');
            $table->integer('builderID')->unsigned();
            $table->integer('profileID')->unsigned();
            $table->integer('scID')->unsigned();
            $table->integer('projectID')->unsigned()->nullable();
            $table->integer('sentByID')->unsigned();
            $table->string('fileUUID');
            $table->string('fileName');
            $table->string('filePath');
            $table->string('fileURL');
            $table->string('mimeType');
            $table->string('fileType');
            $table->string('fileSize', 15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_uploads');
    }
}
