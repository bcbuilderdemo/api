<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuilderBidsOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('builder_bids_out', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('bidID')->unsigned();
            $table->integer('projectID')->unsigned();
            $table->integer('builderID')->unsigned();
            $table->integer('scID')->unsigned()->nullable();
            $table->boolean('scResponded')->default(0);
            $table->boolean('scViewed')->default(0);
            $table->dateTime('scViewedAt')->nullable();
            $table->boolean('updated')->default(0);
            $table->boolean('bbo_accepted')->nullable();
            $table->boolean('accepted')->nullable();
            $table->integer('acceptedSCBOID')->unsigned()->nullable();
            $table->timestamps();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('builder_bids_out');
    }
}
