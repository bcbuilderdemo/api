<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_uploads', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('fileID');
            $table->integer('bidID')->unsigned();
            $table->integer('uploadsID')->unsigned();
            $table->string('fileName', 250);
            $table->string('uniqueFileName', 250);
            $table->string('fileType');
            $table->string('filePath');
            $table->string('fileSize', 25);
            $table->string('fileURL');
            $table->string('mimeType');
            $table->boolean('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_uploads');
    }
}
