<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $this->down();
        
        Schema::create('bids', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('projectID')->unsigned();
            $table->integer('builderID')->unsigned();
            $table->integer('scTradeID')->unsigned();
            //$table->boolean('accepted')->nullable();
            //$table->integer('scID')->unsigned()->nullable();
            //$table->integer('acceptedSCBOID')->unsigned()->nullable();
            $table->string('additionalDetails')->default('None Specified');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bids');
    }
}
