<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcontractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $this->down();

        Schema::create('subcontractors', function (Blueprint $table) {

            $table->engine = 'InnoDB';

           // $table->increments('id');
            $table->integer('userID')->unsigned();
            $table->integer('scTradeID')->unsigned();
            $table->timestamps();
            $table->primary('userID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcontractors');
    }
}
