<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profileID')->unsigned();
            $table->integer('userID')->unsigned();
            $table->string('fileID')->nullable()->default(null);
            $table->boolean('isLogo')->default(0);
            $table->string('fileName')->nullable()->default(null);
            $table->string('filePath')->nullable()->default(null);
            $table->string('fileURL')->nullable()->default(null);
            $table->string('mimeType')->nullable()->default(null);
            $table->string('fileType')->nullable()->default(null);
            $table->string('fileSize', 25)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_uploads');
    }
}
