<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_info', function($table) {
            $table->foreign('userID')->references('id')->on('users')->onDelete('cascade');
        });


        Schema::table('projects', function($table) {
            $table->foreign('userID')->references('id')->on('users')->onDelete('cascade');

        });

        Schema::table('subcontractors', function($table) {
            $table->foreign('scTradeID')->references('id')->on('subcontractor_trades')->onDelete('cascade');
            $table->foreign('userID')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('builders', function($table) {
            $table->foreign('userID')->references('id')->on('users')->onDelete('cascade');
        });


        Schema::table('bids', function($table) {
            $table->foreign('builderID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('projectID')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('scTradeID')->references('id')->on('subcontractor_trades')->onDelete('cascade');
        });


        Schema::table('sc_bids_out', function($table) {
            $table->foreign('bidID')->references('id')->on('bids')->onDelete('cascade');
            $table->foreign('scID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('builderID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('accountingID')->references('id')->on('accounting')->onDelete('cascade');
            $table->foreign('projectID')->references('id')->on('projects')->onDelete('cascade');
        });


        Schema::table('sc_bids_out_line_items', function($table) {
            $table->foreign('bidID')->references('id')->on('bids')->onDelete('cascade');
            $table->foreign('scBidOutID')->references('id')->on('sc_bids_out')->onDelete('cascade');
        });


        Schema::table('builder_bids_out', function($table) {
            $table->foreign('bidID')->references('id')->on('bids')->onDelete('cascade');
            $table->foreign('scID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('builderID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('projectID')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('acceptedSCBOID')->references('id')->on('sc_bids_out')->onDelete('cascade');
        });

        Schema::table('uploads', function($table) {
            $table->foreign('bidID')->references('id')->on('bids')->onDelete('cascade');
            $table->foreign('projectID')->references('id')->on('projects')->onDelete('cascade');
        });

        Schema::table('file_uploads', function($table) {
            $table->foreign('bidID')->references('bidID')->on('uploads')->onDelete('cascade');
            $table->foreign('uploadsID')->references('id')->on('uploads')->onDelete('cascade');

        });

        Schema::table('report_unauthorized_activities', function($table) {
            $table->foreign('userID')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('accounting', function($table) {
            $table->foreign('builderID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('scID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('projectID')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('bidID')->references('id')->on('bids')->onDelete('cascade');
            $table->foreign('scboID')->references('id')->on('sc_bids_out')->onDelete('cascade');
            $table->foreign('scTradeID')->references('id')->on('subcontractor_trades')->onDelete('cascade');
        });

        Schema::table('accounting_payments_made', function($table) {
            $table->foreign('accountingID')->references('id')->on('accounting')->onDelete('cascade');
        });

        Schema::table('profile', function($table) {
            $table->foreign('userID')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('profile_uploads', function($table) {
            $table->foreign('userID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('profileID')->references('userID')->on('profile')->onDelete('cascade');
        });

        Schema::table('chat', function($table) {
            $table->foreign('scID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('builderID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('projectID')->references('id')->on('projects')->onDelete('cascade');
        });

        Schema::table('messages', function($table) {
            $table->foreign('scID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('builderID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('sentByID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('chatID')->references('uuid')->on('chat')->onDelete('cascade');
            $table->foreign('chatUploadID')->references('id')->on('chat_uploads')->onDelete('cascade');
        });

        Schema::table('chat_uploads', function($table) {
            $table->foreign('scID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('builderID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('sentByID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('chatUUID')->references('uuid')->on('chat')->onDelete('cascade');
            $table->foreign('projectID')->references('id')->on('projects')->onDelete('cascade');
        });

        Schema::table('home_builders_guide', function($table) {
            $table->foreign('sctID')->references('id')->on('subcontractor_trades')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    public function two()
    {


    }
}
