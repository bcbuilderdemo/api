<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $this->down();


        Schema::create('company_info', function (Blueprint $table) {
            $table->integer('userID')->unsigned();
            $table->string('company');
            $table->string('companySlug')->unique();
            $table->string('contactFirstName');
            $table->string('contactLastName');
            $table->string('address');
            $table->string('phoneNumber');
            $table->string('phoneNumber2');
            $table->string('address2')->nullable()->default('');
            $table->string('city');
            $table->string('postalCode');
            $table->string('province');
            $table->string('uuid', 40)->unique();
            $table->string('logoURL', 191);
            $table->softDeletes();
            $table->timestamps();

            $table->primary('userID');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companyInfo');
    }
}
