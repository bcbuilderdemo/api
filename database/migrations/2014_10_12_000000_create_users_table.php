<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $this->down();

        Schema::create('users', function (Blueprint $table) {

            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('trade')->nullable();
            $table->boolean('isAdmin')->default(0);
            $table->boolean('isSubContractor')->default(0);
            $table->boolean('isBuilder')->default(0);
            $table->string('userType');
            $table->boolean('verified')->default(0);
            $table->string('token')->nullable();
            $table->boolean('hasCreatedAProject')->default(0);
            $table->boolean('banned')->default(0);
            $table->boolean('deleted')->default(0);
            $table->rememberToken();
            $table->timestamps();

            //$table->foreign('scTradeID')->references('id')->on('subcontractor_trades')->onDelete('cascade');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
