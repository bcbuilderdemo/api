<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
class CreateAccountingPaymentsMadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_payments_made', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accountingID')->unsigned();
            $table->float('amountPaid', 20, 2);
            $table->boolean('scConfirmedPayment')->default(0);
            $table->dateTime('scConfirmedPaymentOn')->nullable();
            $table->dateTime('paymentMadeOn')->default(Carbon::now('America/Vancouver'));
            $table->dateTime('scUnConfirmedPaymentOn')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting__payments__made');
    }
}
