<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTradeBuilderGuideTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('trade_builder_guide', function (Blueprint $table) {
            $table->dropForeign('trade_builder_guide_st_id_foreign');
            $table->dropColumn('st_id');
        });
        Schema::enableForeignKeyConstraints();
        Schema::disableForeignKeyConstraints();
        Schema::table('trade_builder_guide', function (Blueprint $table) {
            $table->integer('st_id')->nullable()->unsigned()->default(null);
            $table->foreign('st_id')
                ->references('id')
                ->on('subcontractor_trades')
                ->onDelete('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
