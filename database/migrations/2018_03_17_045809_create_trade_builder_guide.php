<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradeBuilderGuide extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_builder_guide', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('hbg_id')->unsigned();
            $table->integer('st_id')->unsigned();

            $table->foreign('hbg_id')
                ->references('id')
                ->on('home_builders_guide')
                ->onDelete('cascade');

            $table->foreign('st_id')
                ->references('id')
                ->on('subcontractor_trades')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_builder_guide');
    }
}
