<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('builderID')->unsigned();
            $table->integer('scID')->unsigned();
            $table->integer('projectID')->unsigned();
            $table->integer('bidID')->unsigned();
            $table->integer('bboID')->unsigned();
            $table->integer('scboID')->unsigned();
            $table->integer('scTradeID')->unsigned();
            $table->boolean('deleted')->default(0);
            $table->float('totalAmount', 20, 2);
            $table->float('amountPaidSoFar', 20, 2)->default(0.00);
            $table->float('amountRemaining', 20, 2)->default(0.00);
            $table->boolean('paidInFull')->default(0);
            $table->dateTime('lastPaymentMadeOn')->nullable();
            $table->dateTime('paidInFullOn')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting');
    }
}
