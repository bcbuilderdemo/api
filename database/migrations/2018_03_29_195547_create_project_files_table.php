<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('dirID');
        });

        Schema::create('project_files', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('projectID')->unsigned();
            $table->string('fileID')->nullable()->default(null);
            $table->string('fileName')->nullable()->default(null);
            $table->string('fileType')->nullable()->default(null);
            $table->string('filePath')->nullable()->default(null);
            $table->string('fileSize', 25)->nullable()->default(null);
            $table->string('fileURL')->nullable()->default(null);
            $table->string('mimeType')->nullable()->default(null);

            $table->foreign('projectID')
                ->references('id')
                ->on('projects')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_files');
    }
}
