<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSCBidsOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('sc_bids_out', function (Blueprint $table) {

            $table->engine = 'InnoDB';


            $table->increments('id');
            $table->integer('bidID')->unsigned();
            $table->integer('scID')->unsigned();
            $table->integer('builderID')->unsigned();
            $table->integer('projectID')->unsigned();
            $table->integer('accountingID')->unsigned()->nullable();
            $table->string('subTotal');
            $table->string('taxTotal');
            $table->string('grandTotal');
            $table->string('comments', 500)->nullable();
            $table->boolean('updated')->default(0);
            $table->boolean('builderViewed')->default(0);
            $table->dateTime('builderViewedAt')->nullable();
            $table->boolean('builderAccepted')->nullable();
            $table->dateTime('builder_accepted_at')->nullable();
            $table->dateTime('builder_rejected_at')->nullable();
            $table->boolean('sc_read')->nullable();
            $table->dateTime('sc_read_at')->nullable();
            $table->string('status')->default('waiting');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_bids_out');
    }
}
