<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chatID', 12);
            $table->integer('builderID')->unsigned();
            $table->integer('scID')->unsigned();
            $table->integer('sentByID')->unsigned();
            $table->string('message', 500);
            $table->boolean('fileUploaded')->default(0);
            $table->integer('chatUploadID')->unsigned()->nullable();
            $table->boolean('readByBuilder')->default(0);
            $table->dateTime('readByBuilderAt')->nullable();
            $table->boolean('readBySC')->default(0);
            $table->dateTime('readBySCAt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
