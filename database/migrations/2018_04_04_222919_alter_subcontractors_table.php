<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSubcontractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subcontractors', function (Blueprint $table) {
            $table->integer('max_project_size')->unsigned()->default(1);
            $table->integer('min_employee')->unsigned()->default(1);
            $table->integer('max_employee')->unsigned()->default(1);
            $table->integer('rating')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subcontractors', function (Blueprint $table) {
            $table->dropColumn('max_project_size');
            $table->dropColumn('min_employee');
            $table->dropColumn('max_employee');
            $table->dropColumn('rating');
        });
    }
}
