var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cssmin = require('gulp-cssmin');
var htmlmin = require('gulp-htmlmin');
var babel = require("gulp-babel");
var gulpif = require('gulp-if');
var notify = require("gulp-notify");
var argv = require('yargs').argv;
var cssFile = [];

console.info('Is production env: ' + (isProduction() ? 'yes' : 'no'));

function isProduction() {
    // if we use gulp --production then "force" environment to prod
    // otherwise check env variable
    return argv.production || process.env.NODE_ENV === 'production';
}

function swallowError (error) {
    console.log(error.toString());
    this.emit('end');
}

gulp.task('vendor-js', function () {
    return gulp.src([
        './node_modules/lodash/lodash.js',
        './node_modules/angular/angular.js',
        './node_modules/angular-animate/angular-animate.js',
        './node_modules/angular-resource/angular-resource.js',
        './node_modules/angular-sanitize/angular-sanitize.js',
        './node_modules/angular-smart-table/dist/smart-table.min.js',
        './node_modules/ng-toast/dist/ngToast.js',
        './node_modules/angular-jwt/dist/angular-jwt.js',
        './node_modules/angular-ui-router/release/angular-ui-router.js',
        './node_modules/angular-route/angular-route.js',
        './node_modules/angular-local-storage/dist/angular-local-storage.js',
        './node_modules/select2/dist/js/select2.min.js'
    ])
        .pipe(concat('vendor.min.js'))
        .pipe(gulpif(isProduction(), uglify({mangle: false, output: {
            max_line_len: 5000000
        }})))
        .pipe(gulp.dest('public/js/vendor/'))
        .pipe(notify("vendor-js compiled"));
});

gulp.task('vendor-css', function () {
    return gulp.src([
        './node_modules/select2/dist/css/select2.min.css'
    ])
        .pipe(concat('vendor.min.css'))
        .pipe(gulp.dest('public/css/vendor/'))
        .pipe(notify("vendor-css compiled"));
});

gulp.task('fonts', function () {
    return gulp.src([
    ])
        .pipe(gulp.dest('public/css/vendor/font/'))
        .pipe(notify("fonts compiled"));
});

gulp.task('default', ['vendor-js', 'vendor-css', 'fonts']);