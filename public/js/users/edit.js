var app = angular.module('userEditApp', [], function($interpolateProvider) {
	$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');
}).config(function($sceDelegateProvider) {
});

function MainCtrl($rootScope,$scope, $http, $q) {
    $scope.loading = true;
    $scope.scts = [];
    $scope.id = $rootScope.config.id;
    $scope.user = {}
    $scope.company = {}

    $scope.init = function() {
        const apis = [
            $http.get('/admin/getUser?id=' + $scope.id),
            $http.get('/admin/getScts')
        ]
        $q.all(apis)
            .then((res) => {
                $scope.user = res[0].data.user;
                if ($scope.user.trade_info) {
                    $scope.user.trade_info.scTradeID = parseInt($scope.user.trade_info.scTradeID);
                    $scope.user.trade_info.max_project_size = parseInt($scope.user.trade_info.max_project_size);
                    $scope.user.trade_info.min_employee = parseInt($scope.user.trade_info.min_employee);
                    $scope.user.trade_info.max_employee = parseInt($scope.user.trade_info.max_employee);
                    $scope.user.trade_info.rating = parseInt($scope.user.trade_info.rating);
                }                
                $scope.company = $scope.user.company_info;
                $scope.scts = res[1].data.scts;
                $scope.loading = false
            }).catch(err => {
            })
    }

    $scope.save = function() {
        const apis = [
            $http.post('/admin/saveUser', this.user),
            $http.post('/admin/saveCompany', this.company)
        ]
        $q.all(apis)
            .then(response => {
                new PNotify({
                    title: "successfully saved",
                    type: "success"
                  });    
            }).catch(()=>{})
    }
    
    $scope.init();

    $scope.$watch(function(scope) {
        return scope.user.userType
    }, function(newVal, oldVal) {
        if (newVal === 'builder') {
            $scope.user.isBuilder = 1;
            $scope.user.isSubContractor = 0;
        } else if (newVal === 'subcontractor'){
            $scope.user.isBuilder = 0;
            $scope.user.isSubContractor = 1;
        }
    })
}

app.run(function($rootScope) {
    $rootScope._ = _;
    $rootScope.config = _config;
})

app.controller('editCtrl', MainCtrl)