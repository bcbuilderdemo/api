var app = angular.module('userCreateApp', [], function($interpolateProvider) {
	$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');
}).config(function($sceDelegateProvider) {
});

function MainCtrl($rootScope,$scope, $http, $q) {
    $scope.loading = true;
    $scope.scts = null;
    $scope.user = {
        userType: 'subcontractor',
        isBuilder: 0,
        isSubContractor: 1,
        trade_info: {
            max_project_size: 1,
            min_employee: 1,
            max_employee: 1,
            rating: 0
        },
        verified: 0
    }
    $scope.company = {};

    $scope.init = function() {
        const apis = [
            $http.get('/admin/getScts')
        ]

        $q.all(apis)
            .then((res) => {
                $scope.scts = res[0].data.scts;
                $scope.loading = false
            }).catch(err => {
            })
    }

    $scope.save = function() {
        let tradeID = $scope.tradeID;
        this.user.company_info = this.company;
        const apis = [
            $http.post('/admin/createUser', this.user)
        ]
        $q.all(apis)
            .then(response => {
                new PNotify({
                    title: "successfully created",
                    type: "success"
                });
                location.href='/admin/users';
            }).catch((err)=>{
                if (err.data.message === "The given data failed to pass validation.") {
                    new PNotify({
                        title: "Please fill out all details accurately",
                        type: "error"
                    });                    
                }
            })
    }
    
    $scope.init();

    $scope.$watch(function(scope) {
        return scope.user.userType
    }, function(newVal, oldVal) {
        if (newVal === 'builder') {
            $scope.user.isBuilder = 1;
            $scope.user.isSubContractor = 0;
        } else if (newVal === 'subcontractor'){
            $scope.user.isBuilder = 0;
            $scope.user.isSubContractor = 1;
        }
    })
}

app.run(function($rootScope) {
    $rootScope._ = _;
})

app.controller('createCtrl', MainCtrl)