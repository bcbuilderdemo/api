<?php

use Illuminate\Http\Request;
use App\Events\StatusLiked;

require '../../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/test', function () {
    //event(new App\Events\StatusLiked('Someone'));
    echo phpinfo();die;
    return "Event has been sent new!";
  //  $options = array(
  //   'cluster' => 'ap2',
  //   'encrypted' => true
  // );
  // $pusher = new Pusher\Pusher(
  //   '4516c7560031f4cf7803',
  //   '0495f24887ebda8c87ff',
  //   '546740',
  //   $options
  // );

  // $data['message'] = 'hello world';
  // $pusher->trigger('my-channel', 'my-event', $data);
});

// Route::get('/test', [
//        'uses' => 'ProjectController@testNotification'
//    ]);

Route::group(['prefix' => 'v1'], function () {





    // Get trades for dropdown
    Route::get('/trades', [
        'uses' => 'SubcontractorsController@getTrades'
    ]);

    Route::post('/register', [
        'uses' => 'Auth\RegisterController@create',
    ]);

    Route::post('/login', [
        'uses' => 'Auth\LoginController@login',
    ]);



            





    Route::group(['middleware' => ['auth.jwt']], function () {


        Route::post('/request/bid/out', [
            'uses' => 'ProjectController@addBid'
        ]);

        Route::get('/refresh', [
            'uses' => 'Auth\LoginController@refreshToken',
        ]);

        Route::get('/refresh/startup', [
            'uses' => 'Auth\AuthController@startupRefreshToken',
        ]);


Route::get('/notificationsnew/read/{id}', [
                'uses' => 'ProjectController@notificationsnewread'
                ]);

              Route::get('/notificationsnew/load', [
                'uses' => 'ProjectController@notificationsnewload'
                ]);
        /********************************************************
                                SUBPROJECT
         *******************************************************/
        Route::group(['prefix' => 'subproject'],function () {
            Route::post('/{id}', [
                'uses' => 'ProjectController@updateSubproject'
            ]);

            Route::get('/{subproject_id}/bids', [
                'uses' => 'ProjectController@getBidsBySubprojectID'
            ]);

            Route::get('/{subproject_id}/bids/{subcontractor_id}', [
                'uses' => 'ProjectController@getBidsForSubprojectFromSubcontractor'
            ]);

            Route::post('/invoice/set', [
                'uses' => 'ProjectController@invoiceSet'
            ]);

            Route::post('/requestbidout', [
                'uses' => 'ProjectController@addBidforSubprojectInvoice'
            ]);
        });
        /*************** END OF PROJECTS PREFIX*****************/

        /********************************************************
                                PROJECTS
         *******************************************************/
        Route::group(['prefix' => 'projects'],function () {
            Route::post('/new', [
                'uses' => 'ProjectController@create',
            ]);

            Route::post('/update', [
                'uses' => 'ProjectController@update',
            ]);

            Route::post('/upload/files', [
                'uses' => 'ProjectController@uploadFiles',
            ]);

            Route::post('/{projectID}/bid/create-or-update/{scTradeID}', [
                'uses' => 'BidController@createOrUpdate',
            ]);

//--------------------------jonas start
            Route::post('/{projectID}/bid-from-sct/create-or-update/{hbgID}', [
                'uses' => 'BidController@createOrUpdateFromSct',
            ]);
            Route::get('/withsubcontractors', [
                'uses' => 'ProjectController@getProjectWithSubProjects',
            ]);
//--------------------------jonas end
            Route::post('/{projectID}/bid/{bidID}/{scTradeID}/uploads/{fileID}/delete', [
                'uses' => 'ManageUploadsController@deleteFile',
            ]);

            Route::get('/user', [
                'uses' => 'ProjectController@getAll'
            ]);

            Route::get('/sidebar-projects', [
                'uses' => 'ProjectController@getSideBarProjects'
            ]);

//------------------------jonas start
            Route::get('/subprojectsByHbg/{hbgID}', [
                'uses' => 'ProjectController@getSubProjectsByHbg'
            ]);

            Route::get('/subprojectsByprojectID/{projectID}', [
                'uses' => 'ProjectController@getSubProjectsByProjectID'
            ]);

            Route::get('/subprojectsBySct/{sctID}', [
                'uses' => 'ProjectController@getSubProjectsBySct'
            ]);

            Route::get('/all-projects', [
                'uses' => 'ProjectController@getAllProjects'
            ]);

            Route::get('/gpo', [
                'uses' => 'ProjectController@checkProjectOwnership'
            ]);

            Route::get('/{id}', [
                'uses' => 'ProjectController@getProject'
            ]);

            Route::get('/getProjectByUserID', [
                'uses' => 'ProjectController@getProjectByUserID'
            ]);

            Route::get('/{projectID}/uploads/get-files-for-project', [
                'uses' => 'ProjectController@getFilesForProjectOrBid'
            ]);

            Route::get('/get-file/{id}', [
                'uses' => 'ProjectController@getFile'
            ]);

            Route::get('/delete-file/{id}', [
                'uses' => 'ProjectController@deleteFile'
            ]);

            Route::get('/delete-project/{id}', [
                'uses' => 'ProjectController@destroyProject'
            ]);

//------------------------jonas end


            Route::get('/{projectID}/scs/{scTID}/bids/type/{bidType}', [
                'uses' => 'SubcontractorsController@getSCForProject'
            ]);

            Route::get('/{projectID}/scs/{scTID}/bids', [
                'uses' => 'BidController@getBuilderBidOutFormByTrade'
            ]);

            Route::post('/{projectID}/scs/{scTID}/bid/{bidID}/send', [
                'uses' => 'BidController@sendBidToSelectedSCS'
            ]);

            Route::post('/{projectID}/sc/bid/{bidID}/respond', [
                'uses' => 'BidController@SCBidOut'
            ]);

            Route::get('/{projectID}/sc/bid/{bidID}', [
                'uses' => 'BidController@getSCBidOut'
            ]);

            Route::get('/{projectID}/scs/{scTID}/bid/get/{bidType}', [
                'uses' => 'BidController@getAllBidsByTrade'
            ]);

            Route::get('/{projectID}/sc/get-bids-for-project', [
                'uses' => 'BidController@getBidForProjectForSC'
            ]);

            Route::get('/{id}/bids/get-bbo-by-sct-trade/{scTID}', [
                'uses' => 'SubcontractorsController@getSCSAndBBO'
            ]);

            /*Route::get('/{id}/bids/bbo/new/{scTradeID}', [
                'uses' => 'ProjectController@getProjectInfoForBBO'
            ]);*/

            Route::get('/{projectID}/bids/trade/{scTradeID}', [
                'uses' => 'BidController@checkIfBidFormExistsForTrade'
            ]);


            /*Route::get('/{projectID}/bids/sct/{scTradeID}', [
                'uses' => 'BidController@getProjectInfoForBBO'
            ]);*/


            Route::get('/{projectID}/bids/bbo/{type}/sct/{sctID}', [
                'uses' => 'BidController@getBBOInfo'
            ]);

            /*******************************************************
                                    NOTIFICATIONS
             *******************************************************/

            Route::get('/{projectID}/notifications', 'NotificationsController@getAllNotificationsForProject');


            /********************************************************
                                Uploads
             *******************************************************/

            Route::get('/{id}/uploads/get-files-for-project-or-bid/{bidID?}', [
                'uses' => 'ManageUploadsController@getFilesForProjectOrBid'
            ]);

            /*****************************************************
                               Accounting
             ******************************************************/

            /*Route::get('/{projectID}/accounting/trade/{scTradeID}', [
                'uses' => 'AccountingController@getAccountingForProject'
            ]);*/

            Route::get('/{projectID}/accounting/{type}', [
                'uses' => 'AccountingController@getAccountingForProject'
            ]);

            Route::get('/{projectID}/accounting/{accountingID}/invoice', [
                'uses' => 'AccountingController@getAccountingAndInvoiceData'
            ]);


            Route::post('/{projectID}/accounting/{accountingID}/make-payment', [
                'uses' => 'AccountingController@makePayment'
            ]);

            Route::get('/{projectID}/accounting/reports/for-project/', [
                'uses' => 'AccountingController@getAccountingReportForProject'
            ]);

            Route::put('/{projectID}/sc/accounting/{accountingID}/payment/{apmID}', [
                'uses' => 'AccountingController@changePaymentStatus'
            ]);

            /*****************************************************
                            CHAT
             *****************************************************/

            Route::group(['prefix' => '{projectID}/chat'],function () {


                Route::get('/{uuid}', [
                    'uses' => 'ChatController@getMessages'
                ]);

                Route::post('/{uuid}', [
                    'uses' => 'ChatController@newMessage'
                ]);

                Route::post('/{uuid}/upload', [
                    'uses' => 'ChatController@uploadFile'
                ]);

                Route::get('/{uuid}/get-file/{fileUUID}', [
                    'uses' => 'ChatController@getUploadForChat'
                ]);

                Route::get('/messages', [
                    'uses' => 'ChatController@getAllMessagesForProject'
                ]);

            });


            /*****************************************************
                                SCTs
             *****************************************************/

            Route::get('/{projectID}/scs/trades', [
                'uses' => 'SubcontractorsController@getSCTsForProject',
            ]);

            /*****************************************************
                                    BIDS
             *****************************************************/
//--------------------------jonas start
            Route::get('/{projectID}/bids/all', [
                'uses' => 'BidController@getAllBidsForProject',
            ]);
//--------------------------jonas end
            /*****************************************************
                                NOTS
             *****************************************************/
            Route::group(['prefix' => '{projectID}/notifications'],function () {

                Route::put('bids/{bidID}/bbo/{bboID}/read', 'MarkAsReadController@markBBOAsReadBySC');
                Route::put('bids/{bidID}/scbo/{scboID}/read', 'MarkAsReadController@markSCBOAsReadByBuilder');
                Route::put('bids/{bidID}/scbo/{scboID}/read-by-sc', 'MarkAsReadController@markSCBOAsReadAfterBuilderChangesStatus');
                Route::put('messages/{chatUUID}/read', 'MarkAsReadController@markMessagesAsRead');

            });




            /*************** END OF PROJECTS PREFIX*****************/

        });


        /********************************************************
                                BIDS
         *******************************************************/
        Route::group(['prefix' => 'bids'],function () {
//--------------------------jonas start
            Route::get('/{bidID}/bids-line-items', [
                'uses' => 'BidController@getBidsLineItemsByBidID'
            ]);

            Route::post('/updateBid', [
                'uses' => 'BidController@updateBid'
            ]);

            Route::get('/{projectID}/all', [
                'uses' => 'BidController@getAllBidsByProjectID',
            ]);
//--------------------------jonas end
            Route::get('/in', [
                'uses' => 'BidController@bidsIn'
            ]);
            Route::get('/out', [
                'uses' => 'BidController@bidsOut'
            ]);

            /*Route::get('/test', [
                'uses' => 'BidController@test'
            ]);*/

            Route::get('{bidID}/files-and-proj-data/get/{projectID}', [
                'uses' => 'BidController@getFilesAndProjectDataForBid'
            ]);

            Route::get('{bidID}/bbo/{bboID}', [
                'uses' => 'BidController@getBBOAndInformation'
            ]);

            Route::get('{bidID}/builder-and-proj-data/get/{projectID}', [
                'uses' => 'BidController@getProjectDataAndBuilderInfo'
            ]);

            Route::get('/get-bids-for-sc/{type}/{projectID?}', [
                'uses' => 'BidController@getBidByTypeForSC'
            ]);

            // New Route - 2018-03-29
            Route::get('/get-bids-for-sub/{type}', [
                'uses' => 'BidController@v2_getAllBidsForSubcontractor'
            ]);
            
            Route::get('/get-bids-for-sub-count', [
                'uses' => 'BidController@v2_getAllBidsForSubcontractorWithCount'
            ]);

            Route::put('/{bidID}/accept', [
                'uses' => 'BidController@acceptBid'
            ]);

            Route::put('/{bidID}/reject', [
                'uses' => 'BidController@rejectBid'
            ]);

            Route::get('/scs/trades', [
                'uses' => 'SubcontractorsController@getSCTsForAllBids'
            ]);
        });


        /********************************************************
                                SUBCONTRACTORS
         *******************************************************/

        Route::group(['prefix' => 'scs'],function () {
//-------------------------------jonans start
            Route::get('/subcontractors', [
                'uses' => 'SubcontractorsController@getAllSubcontractors'
            ]);
            Route::get('/scTradeID', [
                'uses' => 'SubcontractorsController@getScTradeID'
            ]);
            Route::get('/trade/{scTradeID}', [
                'uses' => 'SubcontractorsController@getTradeBySubContractor'
            ]);
            Route::get('/hbgs/{scTradeID}', [
                'uses' => 'SubcontractorsController@getHbgBySctID'
            ]);
//-------------------------------jonans end
            Route::post('/create', [
                'uses' => 'SubcontractorsController@create'
            ]);
            Route::get('/get', [
                'uses' => 'SubcontractorsController@getAll'
            ]);

            Route::get('/trades', [
                'uses' => 'SubcontractorsController@getSCTs'
            ]);


            Route::get('/get/{id}/all', [
                'uses' => 'SubcontractorsController@getSCByTrade'
            ]);
        });

        /*****************************************************
         *                  PERMISSIONS
         *****************************************************/

        Route::group(['prefix' => 'permissions'],function () {


            Route::get('/get', [
                'uses' => 'Auth\AuthController@getPermissions'
            ]);

            Route::get('/bids/{bidID}/can-view-or-manage-bid', [
                'uses' => 'Auth\AuthController@checkIfSCOrBuilderCanViewOrManageBidOut'
            ]);
        });

        /*****************************************************
                            UPLOADS
         *****************************************************/

        Route::group(['prefix' => 'uploads'],function () {


            Route::get('/get-file/{id}', [
                'uses' => 'ManageUploadsController@getFile'
            ]);

        });



        /*****************************************************
                            Accounting
         *****************************************************/

        Route::get('/accounting/{type}', [
            'uses' => 'AccountingController@getAccountingForAllProjects'
        ]);

        Route::get('/accounting/reports/all-projects', [
            'uses' => 'AccountingController@getAccountingReportForAllProjects'
        ]);



        /********************************************************
                                    PROFILE
         *******************************************************/

        Route::get('/profile', [
            'uses' => 'ProfileController@getProfileInfo'
        ]);

        Route::get('/profile/{slug}', [
            'uses' => 'ProfileController@getProfileForCompany'
        ]);

        Route::get('/profile/user/current', [
            'uses' => 'ProfileController@getProfileForCurrentUser'
        ]);

        // Newly added by CIS
        Route::post('/profile/user/updateUserStatus', [
            'uses' => 'ProfileController@updateUserStatus'
        ]);

        Route::post('/profile/update', [
            'uses' => 'ProfileController@updateProfile'
        ]);

        Route::post('/profile/update/image', [
            'uses' => 'ProfileController@updateImage'
        ]);

        Route::post('/profile/delete/image', [
            'uses' => 'ProfileController@deleteImage'
        ]);

//----------------------------jonas start
        Route::get('/profile/company_info/{hbg_id}', [
            'uses' => 'ProfileController@getCompanyInfoByHbgID'
        ]);
//----------------------------jonas end
        /*******************************************************
        N               NOTIFICATIONS
         *******************************************************/

        Route::group(['prefix' => 'notifications'],function () {

            Route::get('/all', 'NotificationsController@getAll');

        });

        /*******************************************************
                                USER ACCOUNT STUFF
         *******************************************************/

        Route::put('/password/change', 'Auth\PasswordController@changePassword');
        Route::put('/email/change', 'Auth\PasswordController@changeEmail');
        Route::post('/password/reset/{token}', 'Auth\PasswordController@postReset');


        /*******************************************************
                                MESSAGES
         *******************************************************/

        Route::get('/messages', 'ChatController@getAllMessages');

        /*******************************************************
                    HOME BUILDERS GUIDE
         *******************************************************/

        Route::get('/hbg', 'HomeBuildersGuideController@get');


        /***********************************************
                        END OF JWT MIDDLEWARE
         ***********************************************/

        });



        /*******************************************************
                                USER ACCOUNT STUFF
         *******************************************************/

        Route::post('/password/forgot', 'Auth\PasswordController@postEmail');
        Route::get('/account/verify/{uuid}/{token}', 'Auth\RegisterController@verify');

        //Todo:: Add to admin route
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        /*******************************************************
                                POSTS
         *******************************************************/
        Route::resource('post', 'PostController');

        Route::post('posts/upload/files', [
                'uses' => 'PostController@uploadFiles',
            ]);

        Route::get('posts/get-file/{id}', [
                'uses' => 'PostController@getFile'
            ]);

       Route::get('posts/destroy-post/{id}', [
                'uses' => 'PostController@destroy'
            ]);


});
