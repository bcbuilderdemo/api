<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/test', function () {
   return  'echo <?php {{asset("css/app.css")}}?>;';
});
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

*/

Route::middleware(['auth'])->namespace('Backend')->prefix('admin')->group(function() {
    Route::resource('users', 'UsersController');
    Route::resource('hbg', 'HbgController');
    Route::resource('sct', 'SubcontractorTradesController');

// ------------------API-----------------//
    Route::get('getScts', 'UsersController@getScts');
    Route::get('getUser', 'UsersController@getUser');
    Route::post('saveUser', 'UsersController@saveUser');
    Route::post('createUser', 'UsersController@createUser');
    Route::post('saveCompany', 'UsersController@saveCompany');

    Route::post('getHbgs', ['uses' => 'HbgController@getHbgs', 'as' => 'hbg.getHbgs']);
    Route::post('changeSct', ['uses' => 'HbgController@changeSct', 'as' => 'hbg.changeSct']);
    Route::post('changeIsTrade', ['uses' => 'HbgController@changeIsTrade', 'as' => 'hbg.changeIsTrade']);
    Route::post('getScts', ['uses' => 'SubcontractorTradesController@getScts', 'as' => 'sct.getScts']);
});