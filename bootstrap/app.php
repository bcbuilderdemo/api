<?php
use Illuminate\Support\Facades\Log;
/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
    realpath(__DIR__.'/../')
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);


/*
 * LOGGING
 * https://jesperjarlskov.dk/laravel-file-logging-based-severity/
 */
/*$app->configureMonologUsing(function($monolog) {
    // Monolog writes to the first handler which accepts the given severity, so we flip the levels array, to register the highest severities first.
    $severities = array_flip(\Monolog\Logger::getLevels());
    foreach ($severities as $severity) {
        $lowercase = strtolower($severity);
        $file = "/logs/laravel_$lowercase.log";

        // Create a stream handler for each severity. Set a line formatter to keep the pretty output that we've come to know and love.
        $handler = new Monolog\Handler\StreamHandler(storage_path($file), constant("Monolog\Logger::$severity"), false);
        $handler->setFormatter(new \Monolog\Formatter\LineFormatter(null, null, true, true));

        $monolog->pushHandler($handler);
    }

//    if (!config('app.debug')) {
        $slackhandler = new \Monolog\Handler\SlackHandler(env('xoxp-259714674514-259767824884-259204916289-44290c872fa12814620546e2b00f26a4'), '#errors');
        $slackhandler->setLevel(\Monolog\Logger::ERROR);
        // The slackhandler should allow the log event to bubble, so we don't prevent the log file logger to work.
        $slackhandler->setBubble(true);
        $monolog->pushHandler($slackhandler);
//    }
});*/



/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
