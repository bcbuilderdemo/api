@component('mail::message')
# Introduction


Hello,

Thank you for singing up with BC Builder. Before you can get started we need to verify your account. Please
click the button below to verify your account

@component('mail::button', ['url' => 'google.ca'])
Verify My Account
@endcomponent

Thanks,<br>
BC Builder
@endcomponent
