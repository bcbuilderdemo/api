<div class="modal" tabindex="-1" role="dialog" id="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h5>
            </div>
            <div class="modal-body">
                <p>Are you sure?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <form method="post" action="" class="link" style="display: inline">
                    <input type="hidden" name="_method" value="DELETE">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger link">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
@isset ($isHbg)    
    <div class="modal" role="dialog" id="addBuilderGuideModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" action="" style="display: inline">
                    <div class="modal-header">
                        <h5 class="modal-title">Create Home Builder Guide
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label> Name </label>
                            <input type="text" class="form-control" name="stepName" required>
                        </div>
                        <div class="form-group">
                            <label> Description </label>
                            <input type="text" class="form-control" name="stepDescription" required>
                        </div>
                        <div class="form-group">
                            <label> SubContractorTrades </label>
                            <select class='add-scts form-control' name="sctID" name="stepDescription" required>
                                <option value>---Select Subcontractor Trade---</option>
                            @foreach($scts as $sct)
                                <option value="{{$sct->id}}">{{$sct->tradeName}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>                
                            {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary link">Create</button>                    
                    </div>
                </form>
            </div>
        </div>
    </div>
@endisset

@isset ($isScts)    
    <div class="modal" role="dialog" id="addSubcontractorTradesModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" action="" style="display: inline">
                    <div class="modal-header">
                        <h5 class="modal-title">Create Subcontractor Trade
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label> Name </label>
                            <input type="text" class="form-control" name="tradeName" required>
                        </div>
                        <div class="form-group">
                            <label> Description </label>
                            <input type="text" class="form-control" name="description" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>                
                            {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary link">Create</button>                    
                    </div>
                </form>
            </div>
        </div>
    </div>
@endisset