@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Home Builders Guide
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">Home Builders Guide</li>
        </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">Home Buiders Guide</div>
                    <button type="button" class="btn btn-sm btn-primary pull-right" onclick="addHbg()">Add Homebuilders Guide</button>
                </div>

                <div class="box-body">
                    <table class="table table-bordered table-hover" id='hbgs'>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>SubContractor Trades</th>
                                <th>Is Trade?</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>SubContractor Trades</th>
                                <th>Is Trade?</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_styles')
	<style type="text/css">
		#hbgs_filter {
            float: right;
		}
        .select2-container {
            width: 100% !important;
        }
        .select2-container .select2-selection--single {
            height: 35px !important;
        }
	</style>
@endsection
@section('after_scripts')
<script src="{{ asset('vendor/adminlte') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('vendor/adminlte') }}/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
	$(function () {
        $(".add-scts").select2({
            placeholder: "Enter a Subcontract trade name"
        });
		$("#hbgs").DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {                
                "url": "{{ route('hbg.getHbgs') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
            },
            "columns": [
                { "data": "stepName" },
                { "data": "stepDescription" },
                {
                    "data": "sctID",
                    "render": function(data, type, full, meta) {
                        var rendTag = '';
                        if (type == "display") {
                            renderTag = "<select class='scts' attr-id='" + data.id + "'><option value=null> ---Select Subcontractor Trade---</option>";
                            @foreach($scts as $sct)
                                renderTag += (data.sctID == {{$sct->id}}) ? 
                                "<option value='{{$sct->id}}' selected='selected'>{{$sct->tradeName}}</option>" :
                                "<option value='{{$sct->id}}'>{{$sct->tradeName}}</option>";
                            @endforeach
                            renderTag += "</select>";
                        }
                        return renderTag;
                    }
                },
                {
                    "data": "is_trade",
                    "render": function(data, type, full, meta) {
                        var rendTag = '';
                        if (type == "display") {
                            renderTag = "<select class='is_trade' attr-id='" + data.id + "'>";
                            renderTag += (data.is_trade == 0) ? 
                            "<option value='0' selected='selected'>No</option>" :
                            "<option value='0'>No</option>";
                            renderTag += (data.is_trade == 1) ? 
                            "<option value='1' selected='selected'>Yes</option>" :
                            "<option value='1'>Yes</option>";
                            renderTag += "</select>";
                        }
                        return renderTag;
                    }
                },
                { "data": "actions" }
            ],
            "columnDefs": [
                {
                    "targets": [2, 3],
                    "orderable": false
                }
            ]            
        });
        $("#hbgs").on('draw.dt', function() {
            $('.scts').select2({
                placeholder: "Enter a Subcontract trade name"
            });
            $('.scts').on('select2:selecting', function(e) {
                let hbgID = $(this).attr('attr-id');
                let data = e.params.args.data;
                $.ajax({
                    url: "{{ route('hbg.changeSct') }}",
                    type: "POST",
                    data: {
                        id: hbgID,
                        sctID: data.id
                    },
                    success: function(res) {
                        console.log(res);
                    }
                })
            })
            $(".is_trade").on('change', function() {
                let hbgID = $(this).attr('attr-id');
                let is_trade = $(this).val();
                $.ajax({
                    url: "{{ route('hbg.changeIsTrade') }}",
                    type: "POST",
                    data: {
                        id: hbgID,
                        is_trade: is_trade
                    },
                    success: function(res) {
                        console.log(res);
                    }
                })
            })
        })
    });
    
    var addHbg = function() {        
        $('#addBuilderGuideModal').modal({});
    }
</script>
@endsection