@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
        Subcontractor Trade
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">Subcontractor Trade</li>
        </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">Subcontractor Trade </div>
                    <button type="button" class="btn btn-sm btn-primary pull-right" onclick="addSct()">Add Subcontractor Trade</button>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover" id='scts'>
                        <thead>
                            <tr>
                                <th>Trade Name</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Trade Name</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_styles')
	<style type="text/css">
		#scts_filter {
            float: right;
		}
        .select2-container {
            width: 100% !important;
        }
        .select2-container .select2-selection--single {
            height: 35px !important;
        }
	</style>
@endsection
@section('after_scripts')
<script src="{{ asset('vendor/adminlte') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('vendor/adminlte') }}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $(function () {
        $("#scts").DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {                
                "url": "{{ route('sct.getScts') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
            },
            "columns": [
                { "data": "tradeName" },
                { "data": "description" },
                { "data": "actions" }
            ],
            "columnDefs": [
                {
                    "targets": [2],
                    "orderable": false
                }
            ]            
        });
    });

    var addSct = function() {        
        $('#addSubcontractorTradesModal').modal({});
    }
</script>
@endsection