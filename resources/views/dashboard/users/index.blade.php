@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Users
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">Users</li>
        </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">Users ({{ count($users) }})</div>
                    <button type="button" class="btn btn-sm btn-primary pull-right" onclick="addUser()">Add User</button>
                </div>

                <div class="box-body">
                    <table class="table table-bordered table-hover" id='users'>
                        <thead>
                            <tr>
                            	<th>Contact Name</th>
                                <th>Email</th>
                                <th>Type</th>
                                <th>Verified</th>
                                <th>Trade</th>
                                <th>Minimum Project Size</th>
                                <th>Minimum Employee</th>
                                <th>Max Employee</th>
                                <th>Rating</th>
                                <th>Created at</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            	@if($user->userType)
                                <tr>
                                	<td>{{ $user->company_info['contactFirstName']. ' '. $user->company_info['contactLastName'] }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->userType }}</td>
                                    <td>{{ $user->verified ? 'Yes' : 'No' }}
                                    <td>
                                        {{ $user->trade_info ? $user->trade_info['subcontractor_trades']['tradeName'] : ''}}
                                    </td>
                                    <td>
                                        {{ $user->trade_info ? '$ '. $user->trade_info['max_project_size'] : ''}}
                                    </td>
                                    <td>
                                        {{ $user->trade_info ? $user->trade_info['min_employee'] : ''}}
                                    </td>
                                    <td>
                                        {{ $user->trade_info ? $user->trade_info['max_employee'] : ''}}
                                    </td>
                                    <td>
                                        {{ $user->trade_info ? $user->trade_info['rating'] : ''}}
                                    </td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>
                                        <a class="btn btn-xs btn-danger pull-right remove" data-href="{{ route('users.destroy', $user->id) }}">
                                            <i class="fa fa-trash"></i> Delete
                                        </a>
                                        <a class="btn btn-xs btn-default pull-right" style="margin-right:10px;" href="{{ route('users.edit', $user->id) }}">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_styles')
	<style type="text/css">
		#users_filter {
            float: right;
		}
	</style>
@endsection
@section('after_scripts')
<script src="{{ asset('vendor/adminlte') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('vendor/adminlte') }}/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
	$(function () {
		$("#users").DataTable();
    });

    var addUser = function() {
        location.href="{{ route('users.create') }}";
    }
</script>
@endsection