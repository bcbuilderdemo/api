@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Edit User
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
            <li><a href="{{ route('users.index') }}">Users</a></li>
            <li class="active">Edit User</li>
        </ol>
    </section>
@endsection


@section('content')
    <div class="row" ng-app="userEditApp" ng-controller="editCtrl" ng-if="!loading">
        <div class="col-md-8 col-md-offset-2">
            <a href="{{ route('users.index') }}"><i class="fa fa-angle-double-left"></i> Back to Users</a>
            <br>
            <br>
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">Edit User - <% company['contactFirstName'] + ' ' + company['contactLastName'] %></div>
                    <div class="pull-right">
                        <label class="form-check-label" for="verified" style="margin-right: 10px;">Verified </label>
                        <input type="checkbox" class="form-check-input" ng-model="user.verified" ng-true-value='1' ng-false-value='0'>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <label>Email *</label>
                        <input type="text" class="form-control" ng-model="user.email">
                    </div>
                    <div class="form-group">
                        <label>User Type</label>
                        <select ng-model="user.userType" class="form-control">
                            <option value="builder">Builder</option>
                            <option value="subcontractor">Subcontractor</option>
                        </select>
                    </div>
                    <div class="form-group" ng-if="user.userType === 'subcontractor'">
                        <label>Trade</label>
                        <select ng-model="user.trade_info.scTradeID" class="form-control">
                            <option ng-repeat="sct in scts" ng-value="sct.id"><%sct.tradeName%></option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4" ng-if="user.userType === 'subcontractor'">
                            <label>Minimum Project Size</label>
                            <input type="number" class="form-control" ng-model="user.trade_info.max_project_size">
                        </div>
                        <div class="form-group col-md-3" ng-if="user.userType === 'subcontractor'">
                            <label>Min Employee</label>
                            <input type="number" class="form-control" ng-model="user.trade_info.min_employee">
                        </div>
                        <div class="form-group col-md-3" ng-if="user.userType === 'subcontractor'">
                            <label>Max Employee</label>
                            <input type="number" class="form-control" ng-model="user.trade_info.max_employee">
                        </div>
                        <div class="form-group col-md-2" ng-if="user.userType === 'subcontractor'">
                            <label>Rating</label>
                            <input type="number" class="form-control" max="100" ng-model="user.trade_info.rating">
                        </div>
                    </div>
                    
                    <div class="business_info">
                        <div class="form-group">
                            <label>Company Name</label>
                            <input type="text" class="form-control" placeholder="Company Name" tabindex="6" ng-model='company.company'>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Address 1</label>
                                <input type="text" class="form-control" placeholder="Address 1" tabindex="6" ng-model='company.address'>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Address 2</label>
                                <input type="text" class="form-control" placeholder="Address 2" tabindex="6" ng-model='company.address2'>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>City</label>
                                <input type="text" class="form-control" placeholder="City" tabindex="6" ng-model='company.city'>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Postal Code</label>
                                <input type="text" class="form-control" placeholder="Postal Code" tabindex="6" ng-model='company.postalCode'>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Province</label>
                                <input type="text" class="form-control" placeholder="Province" tabindex="6" ng-model='company.province'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Business Phone Number</label>
                            <input type="text" class="form-control" placeholder="Business Phone Number" tabindex="6" ng-model='company.phoneNumber'>
                        </div>
                    </div>
                    <div class="company_contact">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>First Name</label>
                                <input type="text" class="form-control" placeholder="First Name" tabindex="6" ng-model='company.contactFirstName'>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Last Name</label>
                                <input type="text" class="form-control" placeholder="Last Name" tabindex="6" ng-model='company.contactLastName'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Contacts Phone Number</label>
                            <input type="text" class="form-control" placeholder="Contacts Phone Number" tabindex="6" ng-model='company.phoneNumber2'>
                        </div>
                    </div>
                    <hr/>

 <div class="company_contact">                             
                       
                        <div class="form-group">
                            <label>Associations</label>
                            
                                <textarea class="form-control" placeholder="Associations" tabindex="6" ng-model='user.associations' ></textarea>
                        </div>

                        <div class="form-group">
                            <label>Certifications</label>
                             <textarea class="form-control" placeholder="Certifications" tabindex="6" ng-model='user.certifications' ></textarea>
                        </div>
                        <div class="form-group">
                            <label>References</label>
                            <textarea class="form-control" placeholder="References" tabindex="6" ng-model='user.references' ></textarea>
                        </div>

                         <div class="form-group">
                               <label>Is Tutorial Viewed </label>                                
                                <input type="checkbox" class="form-check-input" ng-model="user.is_tutorial_viewed">
                            </div>



                          <div class="form-group">
                               <label>Terms Accepted </label>                                
                                <input type="checkbox" class="form-check-input" ng-model="user.is_terms_accepted">
                            </div> 
  

                    </div>
                    <hr/>


                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-sm btn-primary" ng-click="save()">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after_styles')
    <style>
        .business_info {
            background-color: rgba(0, 0, 0, 0.02);
            margin: 0px -10px;
            padding: 10px 10px;
        }
        .company_contact {
            padding-top: 10px;
        }
    </style>
@endsection
@section('after_scripts')
    <script>
        var _config = {
            id: {{$id}}
        }
    </script>
@endsection

@section('after_vendor_scripts')
    <script src="{{ asset('js/users/edit.js') }}"></script>
@endsection