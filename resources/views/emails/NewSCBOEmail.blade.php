@component('mail::message')
@slot('header')
@endslot

# Hello {{$sciCompany}},
{{$bciCompany}} has just sent you a new bid. Please login to your BC Builder account to view more information.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
