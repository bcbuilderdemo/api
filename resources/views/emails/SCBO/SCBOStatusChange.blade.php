@component('mail::message')

# Hello {{$sciCompany}},
{{$bciCompany}} has just {{$status}} your Quote. Please login to your BC Builder account to view more information.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
