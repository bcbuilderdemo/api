@component('mail::layout')
@slot('header')
 <img src="<?php echo asset('/images/no-image-found.png'); ?>" width="480" height="150">
@endslot
# Hello {{$company}},

Thank you for singing up with BC Builder. Before you can get started we need to verify your account. Please
click the button below to verify your account

@component('mail::button', ['url' => "http://www.neilbrar.com/bcbAPI/api/v1/account/verify/$uuid/$token"])
    Verify My Account
@endcomponent

Thanks,<br>
BC Builder
@slot('footer')
    @component('mail::footer')
    @endcomponent
@endslot
@endcomponent
