@component('mail::message')

# Hello {{$bciCompany}},

Congratulations on accepting the quote from {{$sciCompany}} for {{$projectAddress}} {{$projectAddress2}}. You have a new invoice available! Log in to your BC Builder account
to check it out!

Thanks,<br>
{{ config('app.name') }}

@endcomponent
