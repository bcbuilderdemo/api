@component('mail::message')
{{--@slot('header')
<img src="<?php echo (asset('/images/logo/logo.jpg')); ?>" width="480" height="150">
@endslot--}}

# Hello {{$sci}}, <br/>

{{$bci}} has just indicated that they have made a payment of ${{$amountPaid}} to you. Please login
and mark this payment as confirmed or not yet received.

Thanks,<br>
{{ config('app.name') }}
{{--@@slot('footer')
    @component('mail::footer')
    @endcomponent
@endslot--}}
@endcomponent
