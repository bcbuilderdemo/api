@component('mail::message')
{{--@slot('header')
    <img src="<?php echo (asset('/images/logo/logo.jpg')); ?>" width="480" height="150">
@endslot--}}

# Hello {{$bci}}, <br/>

{{$sci}} has just marked the payment of {{$amountPaid}} as {{$status}}.

Thanks,<br>
{{ config('app.name') }}
{{--@@slot('footer')
    @component('mail::footer')
    @endcomponent
@endslot--}}
@endcomponent
