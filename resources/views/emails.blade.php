<!doctype html>
<html>
  <head>
  <meta charset="utf-8">
  <title>BC Builder</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <style type="text/css">
.preheader { display: none !important; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0; }
a:focus { outline: none; }
.white { color: #fff !important; }
.full-width img { width: 100% !important; }


@media only screen and (max-width:640px) {
.full-container { width: 100% !important; }
.no-br br { display: none; }
.full-width.height10 { height: 20px; }
.big-btn{font-size:14px !important;line-height:56px !important;}
.width58{width:15px !important; }
.less-space{height:15px !important;}
.user-space{padding-top:40px !important;}
}
 @media only screen and (max-width:480px) {
.width15 { width: 15px !important; }
.width10 { width: 5px !important; }
.font-11 { font-size: 11px !important; line-height: 16px !important; }
.font-10 { font-size: 10px !important; line-height: 15px !important; }
.img50 { width: 50px !important; margin: 0 auto !important; }
.img32 { width: 32px !important; margin: 0 auto !important; }
.font14 { font-size: 14px !important; line-height: 18px !important; }
.height10 { height: 10px !important; }
.height15 { height: 15px !important; }
.height5 { height: 5px !important; }
.width150 { width: 150px !important; }
.width50 { width: 50px !important; }
.btn-text { font-size: 12px !important; }
.account-outer th { width: 100% !important; display: block; }
.footer-navs a { padding: 0px 10px !important; }
.gap-cls { width: 10px !important; }
.accountable-cls { height: 15px !important; }
.learn-center { text-align: center !important; }
.tablefull, .tablefull tr,  .tablefull tr td{display:block !important;width:100% !important;font-size:13px !important;}
.testi-full, .testi-full > tbody,  .testi-full > tbody > tr,  .testi-full > tbody > tr > td{display:block !important;width:100% !important;font-size:13px !important;}
.heading-small{font-size:16px !important;}
.big-btn{line-height:24px !important;}
.tablefull .circle-area{clear:both;position:relative;width: 100% !important; }
.tablefull .circle-area::after{clear:both;content:'';display:block;}
.tablefull .circle-img{width:100% !important;text-align:center;}
.tablefull .circle-text{width:100% !important;text-align:center;}
.big-btn{font-size:13px !important;line-height:24px !important;}
}



</style>
  </head>

  <body style="background-color:#f3f3f3; margin:0; padding:0; font-family:Arial;">


<!-- PRE-HEADER TEXT -->
<table align="center" class="full-container" width="600" border="0" cellspacing="0" cellpadding="0" style="width:600px; margin:0 auto;font-family:Arial;font-size:13px;border:none;border-collapse:collapse; background:#ffffff;">
        <tbody>
            <tr>
                <td bgcolor="#ffffff" valign="top">
                 <table cellpadding="0" cellspacing="0" width="100%" style="width:100%; border:0; ">
                    <tr>
                        <td class="height15" height="25" style="height:25px;"></td>
                    </tr>
                    <tr>
                        <td><table cellpadding="0" cellspacing="0" width="100%" style="width:100%; border:0;">
                                <tr>
                                    <td class="width15" width="48" style="width:48px;"></td>
                                    <td><table cellpadding="0" cellspacing="0" width="100%" style="width:100%; border:0; ">
                                            <tr>
                                                <td><table cellpadding="0" cellspacing="0" width="100%" style="width:100%; border:0; ">
                                                        <tr>
                                                           <td style="color:#557840; font-size:28px; font-weight: 600;">New Project Available</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="height10" height="25" style="height:25px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>{{ $name }} has started a new project <b>Located at {{ $address }}!!</b> Click here to view project detail</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="height10" height="15" style="height:15px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>To access project detials, click on <a href="https://subcontractor.bcbuilder.ca/#/login" target="_blank"/>https://subcontractor.bcbuilder.ca/#/login </a> and sign into your online account with your login ID and password. </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="height10" height="15" style="height:15px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Once you are logged in, you can view the project details on the builders map and you’re ready to get started! </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td class="height10" height="15" style="height:15px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>When the builder is ready for your trade, the project will light up green. The Green icon indicates the builder is accepting bids for your trade.  </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td class="height55" height="55" style="height:55px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>As we continue to expand, we will keep you informed of the latest projects listed! </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="height10" height="15" style="height:15px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>For any questions, please feel free to contact us at customerservice@bcbuilder.ca </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td class="height60" height="60" style="height:60px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table style="width:100%; margin:0 auto;font-family:Arial;font-size:13px;border:none;border-collapse:collapse; background:#ffffff;">
                                                                <tr><td style="width:25%;"></td><td style="width:50%;"><img src="{{URL::asset('/api/public/images/logo.png')}}" width="100%" height="100%" alt="logo" style="width:100%;" class="logo-img"/></td><td style="width:25%;"></td></tr>
                                                                    
                                                                </table> 
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td class="height10" height="15" style="height:15px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:10px; text-align:center; color:#a4a4a4;">This email was sent by BC Builder because you are a registered user of BCBuilder.ca. We’d love to hear from you! <a href="javascript:void();" target="_blank">Send us feedback!</a> or contact us through our <a href="javascript:void();" target="_blank">help center.</a> </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:10px; text-align:center; color:#a4a4a4;"> <a href="javascript:void();" target="_blank">BCBuilder.ca</a></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:10px; text-align:center; color:#a4a4a4;">Court house buisiness center  </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:10px; text-align:center; color:#a4a4a4;">14225 57ave Surrey, BC V3X3A3 Canada. </td>
                                                        </tr>
                                                        
                                                        
                                                        
                                                    </table></td>
                                            </tr>
                                        </table></td>
                                    <td class="width15" width="48" style="width:48px;"></td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td class="height15" height="30" style="height:30px;"></td>
                    </tr>
                    
                    </table></td>
            </tr>
        
        <!--body html-->
        </tbody>
    </table>
</body>
</html>




