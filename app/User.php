<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use App\Models\Subcontractors\Subcontractors;
use App\Models\CompanyInfo\Profile;
use App\Models\Post;

class User extends Authenticatable implements CanResetPasswordContract
{
    use Notifiable, CanResetPassword, HasRolesAndAbilities;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'password_confirmation',
        'userType',
        'verified',
        'isSubContractor',
        'isBuilder',
        'is_terms_accepted',
        'is_tutorial_viewed',
        'associations',
        'certifications',
        'references'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'password_confirmation'
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Relation with companies.
     */
    public function company_info()
    {
        return $this->hasOne(CompanyInfo::class, 'userID');
    }

    /**
     * Relation with Subcontractors.
     */
    public function trade_info()
    {
        return $this->hasOne(Subcontractors::class, 'userID');
    }

    /**
     * Relation with Profile.
     */
    public function profile()
    {
        return $this->hasOne(Profile::class, 'userID');
    }

    /**
     * Relation with Post.
     */
    public function posts()
    {
        return $this->hasMany(Post::class, 'userID');
    }
}
