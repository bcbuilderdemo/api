<?php

namespace App\Notifications;

use App\Models\Accounting\Accounting;
use App\Models\Accounting\Accounting_Payments_Made;
use App\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BuilderHasSentPaymentNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $apm;
    protected $act;

    public function __construct(Accounting_Payments_Made $apm, Accounting $act)
    {
        $this->apm = $apm;
        $this->act = $act;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase($notifiable)
    {

        $project        = Project::find($this->act->projectID);

        $not = $notifiable
                    ->unreadNotifications
                    ->where('type', 'App\Notifications\BuilderHasSentPaymentNotification')
                    //->where('data.apmID', $this->apm->id)
                    ->where('data.actID', $this->act->id)
                    ->where('data.projectID', $project->id)
                    ->where('data.builderID', $this->act->builderID)
                    ->where('data.scID', $this->act->scID)
                    ->first();

        if ($not) {
            if ($not->count == null) {
                $not->count = 1;
                $not->save();
            } else {
                $not->count++;
                $not->save();
            }
        } else {
            return [
                'apmID'             => $this->apm->id,
                'actID'             => $this->act->id,
                'scboID'            => $this->act->scboID,
                'builderID'         => $this->act->builderID,
                'scID'              => $this->act->scID,
                'projectID'         => $project->id,
                'projectAddress'    => $project->address,
                'projectAddress2'   => $project->address2,
                /*
                'projectCity'       => $project->city,
                'projectProvince'   => $project->province,
                'scCompany'         => $sc->company,
                'builderCompany'    => $builder->company,
                'URL'               => $URL,
                'notType'           => 'BBO'*/
            ];
        }

        $notifiable->id = 0;
        return [];
    }
}
