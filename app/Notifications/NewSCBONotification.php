<?php

namespace App\Notifications;

use App\Bid;
use App\CompanyInfo;
use App\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Helpers;
use Illuminate\Support\Facades\DB;

class NewSCBONotification extends Notification  implements ShouldQueue
{
    use Queueable;

    protected $scbo;
    /**
     * Create a new notification instance.
     *
     * @return void
     */


    public function __construct($scbo)
    {
        $this->scbo = $scbo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    public function toDatabase($notifiable)
    {


        $not = $notifiable
                ->unreadNotifications
                ->where('type', 'App\Notifications\NewSCBONotification')
                ->first();

        if ($not) {
            if ($not->count == null) {
                $not->count = 1;
                $not->save();
            }
            else {
                $not->count++;
                $not->save();
            }

        } else {
            $project    = Project::find($this->scbo->projectID);
            //$sc         = CompanyInfo::find($this->scbo->scID);
            //$builder    = CompanyInfo::find($this->scbo->builderID);
            //$feURL      = Helpers\GeneralHelpers\getFrontEndURL();
            //$URL        = $feURL.'/projects/'.$this->scbo->id.'/bids/'.$this->scbo->bidID.'/builder/quote';



            return [
                'scboID'           => $this->scbo->id,
                'bidID'             => $this->scbo->bidID,
                'builderID'         => $this->scbo->builderID,
                'scID'              => $this->scbo->scID,
                'projectID'         => $project->id,
                'projectAddress'    => $project->address,
                'projectAddress2'   => $project->address2,
                /*
                'projectCity'       => $project->city,
                'projectProvince'   => $project->province,
                'scCompany'         => $sc->company,
                'builderCompany'    => $builder->company,
                'URL'               => $URL,
                'notType'           => "SCBO"*/
            ];
        }

        $notifiable->id = 0;
        return [];
    }
}
