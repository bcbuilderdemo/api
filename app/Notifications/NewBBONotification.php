<?php

namespace App\Notifications;

use App\Bid;
use App\CompanyInfo;
use App\Models\Subcontractors\Subcontractors;
use App\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Helpers;
class NewBBONotification extends Notification  implements ShouldQueue
{
    use Queueable;

    protected $bbo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($bbo)
    {
        $this->bbo = $bbo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        /*$bid        = Bid::find($this->bbo->bidID);
        $project    = Project::find($bid->projectID);
        $sc         = CompanyInfo::find($this->bbo->scID);
        $builder    = CompanyInfo::find($this->bbo->builderID);
        $feURL      = Helpers\GeneralHelpers\getFrontEndURL();
        $URL        = "$feURL/projects/$project->id/bids/$bid->id/view/$this->bbo->id";*/

        $bid        = Bid::find($this->bbo->bidID);
        $project    = Project::find($bid->projectID);

        $not = $notifiable
            ->unreadNotifications
            ->where('type', 'App\Notifications\NewBBONotification')
            ->where('data.bboID', $this->bbo->id)
            ->where('data.projectID', $project->id)
            ->first();

        if ($not) {
            if ($not->count == null) {
                $not->count = 1;
                $not->save();
            } else {
                $not->count++;
                $not->save();
            }
        } else {
            return [
                'bboID'             => $this->bbo->id,
                'bidID'             => $this->bbo->bidID,
                'builderID'         => $this->bbo->builderID,
                'scID'              => $this->bbo->scID,
                'projectID'         => $project->id,
                'projectAddress'    => $project->address,
                'projectAddress2'   => $project->address2,
                /*
                'projectCity'       => $project->city,
                'projectProvince'   => $project->province,
                'scCompany'         => $sc->company,
                'builderCompany'    => $builder->company,
                'URL'               => $URL,
                'notType'           => 'BBO'*/
            ];
        }
        $notifiable->id = 0;
        return [];

    }
}
