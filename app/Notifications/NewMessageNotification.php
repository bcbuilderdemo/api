<?php

namespace App\Notifications;

use App\CompanyInfo;
use App\Models\Chat\Chat;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewMessageNotification extends Notification  implements ShouldQueue
{
    use Queueable;

    protected $message;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {

        $nots = $notifiable->unreadNotifications->where('type', 'App\Notifications\NewMessageNotification')->all();

        if ($nots) {
            foreach ($nots as $not) {
                if ($not->data['id'] == $this->message->id) {
                    if ($not->count == null) {
                        $not->count = 1;
                        $not->save();
                    } else {
                        $not->count++;
                        $not->save();
                    }
                }
            }
        } else {
            //$chat = Chat::find($this->message->chatUUID);
            $sentBy         = CompanyInfo::find($this->message->sentByID);
            return [
                'chatUUID'      => $this->message->chatUUID,
                'sentByName'    => $sentBy->company,
                'messageID'     => $this->message->id,
            ];
        }

        $notifiable->id = 0;
        return [];

    }
}
