<?php

namespace App\Notifications;

use App\Bid;
use App\Models\Bids\BuilderBidsOut;
use App\Models\Bids\SCBidsOut;
use App\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BuilderRejectedBidNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $bid;
    protected $scbo;
    protected $bbo;



    public function __construct(Bid $bid, SCBidsOut $scbo, BuilderBidsOut $bbo)
    {
        $this->bid      = $bid;
        $this->scbo     = $scbo;
        $this->bbo      = $bbo;
    }


    public function via($notifiable)
    {
        return ['database'];
    }


    public function toDatabase($notifiable)
    {

        $project        = Project::find($this->bid->projectID);

        $not = $notifiable
            ->unreadNotifications
            ->where('type', 'App\Notifications\BuilderRejectedBidNotification')
            ->where('data.scboID', $this->scbo->id)
            ->where('data.bboID', $this->bbo->id)
            ->where('data.builderID', $this->bbo->builderID)
            ->where('data.scID', $this->bbo->scID)
            ->where('data.projectID', $project->id)
            ->first();

        if ($not) {
            if ($not->count == null) {
                $not->count = 1;
                $not->save();
            } else if ($not->count > 0) {
                $not->count++;
                $not->save();
            }


        } else {
            return [
                'scboID'            => $this->scbo->id,
                'bboID'             => $this->bbo->id,
                'bidID'             => $this->bbo->bidID,
                'builderID'         => $this->bbo->builderID,
                'scID'              => $this->bbo->scID,
                'projectID'         => $project->id,
                'projectAddress'    => $project->address,
                'projectAddress2'   => $project->address2,
                /*
                'projectCity'       => $project->city,
                'projectProvince'   => $project->province,
                'scCompany'         => $sc->company,
                'builderCompany'    => $builder->company,
                'URL'               => $URL,
                'notType'           => 'BBO'*/
            ];
        }
        $notifiable->id = 0;
        return [];
    }
}
