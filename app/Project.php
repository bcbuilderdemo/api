<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subproject;
use App\User;
use App\Bid;

class Project extends Model
{

    protected $table = 'projects';

    protected $fillable = [
        'firstName',
        'lastName',
        'address',
        'address2',
        'city',
        'postalCode',
        'province',
        'country',
        'description',
        'landType',
        'projectType',
        'lotSize',
        'buildingSize',
        'zoning',
        'buildValue',
        'details',
        'numUnits',
        'buildingType',
        'dirID',
        'latitude',
        'longitude'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function subprojects()
    {
        return $this->hasMany(Subproject::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function builder()
    {
        return $this->BelongsTo(User::class, 'userID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function bid()
    {
        return $this->hasMany(Bid::class, 'projectID');
    }
}
