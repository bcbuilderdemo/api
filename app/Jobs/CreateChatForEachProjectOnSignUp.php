<?php

namespace App\Jobs;

use App\Models\Chat\Chat;
use App\Project;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Helpers;

class CreateChatForEachProjectOnSignUp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    //public $timeout = 120;

    protected $user;

    //public $maxTries    = 10;
    //public $timeout     = 30;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $projects = Project::all();

        foreach($projects as $project) {
            $chat               = new Chat();
            $chat->uuid         = Helpers\GeneralHelpers\generateUniqueString($chat->getTable(), 'uuid', 12);
            $chat->scID         = $this->user->id;
            $chat->builderID    = $project->userID;
            $chat->projectID    = $project->id;

            try {
                $chat->save();
                Helpers\AuthHelpers\giveChatOwnershipToBuilderIDAndSCIDByChat($chat, $project->userID, $this->user->id);

            }
            catch (\Exception $ex) {

            }
        }
    }
}
