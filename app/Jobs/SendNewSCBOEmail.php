<?php

namespace App\Jobs;

use App\Mail\SenSCBOEmailMailable;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;


class SendNewSCBOEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $scbo;

    public function __construct($scbo)
    {
        $this->scbo = $scbo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email  = new SenSCBOEmailMailable($this->scbo);
        $user   = User::find($this->scbo->builderID);

        Mail
            ::to($user->email)
            ->send($email);
    }
}
