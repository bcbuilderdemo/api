<?php

namespace App\Jobs;

use App\Mail\SCHasChangedPaymentStatus;
use App\Models\Accounting\Accounting;
use App\Models\Accounting\Accounting_Payments_Made;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SCHasChangedPaymentStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $apm;
    protected $act;


    public function __construct(Accounting $act, Accounting_Payments_Made $apm)
    {
        $this->apm = $apm;
        $this->act = $act;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email      = new SCHasChangedPaymentStatus($this->act, $this->apm);
        $user       = User::find($this->act->scID);
        Mail
            ::to($user->email)
            ->send($email);
    }
}
