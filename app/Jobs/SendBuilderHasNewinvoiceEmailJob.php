<?php

namespace App\Jobs;

use App\Bid;
use App\CompanyInfo;
use App\Mail\BuilderHasNewInvoiceMailable;
use App\Models\Accounting\Accounting;
use App\Models\Accounting\Accounting_Payments_Made;
use App\Models\Bids\BuilderBidsOut;
use App\Models\Bids\SCBidsOut;
use App\Project;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendBuilderHasNewinvoiceEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $bid;
    protected $act;
    protected $bbo;

    protected $bci;
    protected $sci;
    protected $scbo;
    protected $user;


    public function __construct(Bid $bid, BuilderBidsOut $bbo, SCBidsOut $scbo, Accounting $act)
    {
        $this->bid    = $bid;
        $this->act    = $act;
        $this->bbo    = $bbo;
        $this->scbo   = $scbo;

        $this->bci     = CompanyInfo::find($scbo->builderID);
        $this->sci     = CompanyInfo::find($scbo->scID);
        $this->project = Project::find($scbo->projectID);

        $this->user     = User::find($scbo->builderID);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new BuilderHasNewInvoiceMailable($this->bid, $this->bbo, $this->scbo, $this->act);
        Mail
            ::to($this->user->email)
            ->send($email);
    }
}
