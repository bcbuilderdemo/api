<?php

namespace App\Jobs;

use App\Mail\SendBBOEmailMailable;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendNewBBOEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $bbo;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bbo)
    {
        $this->bbo = $bbo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new SendBBOEmailMailable($this->bbo);
        $user   = User::find($this->bbo->scID);
        Mail
            ::to($user->email)
            ->send($email);
    }
}
