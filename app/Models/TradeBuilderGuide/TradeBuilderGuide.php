<?php

namespace App\Models\TradeBuilderGuide;

use Illuminate\Database\Eloquent\Model;
use App\Models\HomeBuildersGuide\HomeBuildersGuide;
use App\Models\Subcontractors\SubcontractorTrades;
use App\Models\Subcontractors\Subcontractors;

class TradeBuilderGuide extends Model
{
    protected $table = "trade_builder_guide";
    public $fillable = [
        'hbg_id',
        'st_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subcontractor_trades()
    {
        return $this->BelongsTo(SubcontractorTrades::class, 'st_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subcontractor()
    {
        return $this->BelongsTo(Subcontractors::class, 'st_id', 'scTradeID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function home_builders_guide()
    {
        return $this->BelongsTo(HomeBuildersGuide::class, 'hbg_id', 'id');
    }
}
