<?php

namespace App\Models\Uploads;

use Illuminate\Database\Eloquent\Model;

class FileUploads extends Model
{

    protected $fillable = [
        'fileName',
        'filePath',
        'mimeType',
        'fileType',
        'fileType',
        'deleted',
    ];
}
