<?php

namespace App\Models\Uploads;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{

    protected $fillable = [
        'bidID',
        'userID',
        'projectID',
    ];
}
