<?php

namespace App\Models\Uploads;

use Illuminate\Database\Eloquent\Model;

class ProjectFile extends Model
{
    protected $table = 'project_files';

    protected $fillable = [
        'projectID',
        'bidID',
        'fileID',
        'fileName',
        'fileType',
        'filePath',
        'fileSize',
        'iconURL',
        'mimeType',
        'deleted',
        'subprojectID'
    ];
}
