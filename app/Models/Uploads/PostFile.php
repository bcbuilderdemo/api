<?php

namespace App\Models\Uploads;

use Illuminate\Database\Eloquent\Model;

class PostFile extends Model
{
    protected $table = 'post_files';

    protected $fillable = [
        'postID',
        'fileID',
        'fileName',
        'fileType',
        'filePath',
        'fileSize',
        'iconURL',
        'mimeType',
        'deleted'
    ];
}
