<?php

namespace App\Models\Subcontractors;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\TradeBuilderGuide\TradeBuilderGuide;
use App\Models\Subcontractors\SubcontractorTrades;

class Subcontractors extends Model
{
    protected $primaryKey = 'userID';

    protected $fillable = [
        'userID',
        'scTradeID',
        'min_employee',
        'max_employee',
        'max_project_size',
        'rating'
    ];

    /**
     * Relation with User.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->BelongsTo(User::class, 'userID');
    }

    /**
     * Relation with User.
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function trade_builder_guide()
    {
        return $this->hasMany(TradeBuilderGuide::class, 'st_id', 'scTradeID');
    }

    /**
     * Relation with User.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subcontractor_trades()
    {
        return $this->BelongsTo(SubcontractorTrades::class, 'scTradeID');
    }
}
