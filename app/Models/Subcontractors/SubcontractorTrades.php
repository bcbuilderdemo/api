<?php

namespace App\Models\Subcontractors;

use Illuminate\Database\Eloquent\Model;

class SubcontractorTrades extends Model
{
    protected $table = 'subcontractor_trades';

    protected $fillable = [
        'tradeName',
        'description',
        'image',
    ];
    
    public function subcontractor_count()
    {
        return $this->hasMany(Subcontractors::class);
    } 
}
