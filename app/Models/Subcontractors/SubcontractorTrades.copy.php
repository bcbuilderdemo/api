<?php

namespace App\Models\SubcontractorTrades;

use Illuminate\Database\Eloquent\Model;

class SubcontractorTrades extends Model
{

    protected $table = 'subcontractor_trades';

    protected $fillable = [
        'tradeName',
        'description',
        'image',
    ];

}
