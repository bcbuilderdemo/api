<?php

namespace App\Models\Chat;
use Kyslik\ColumnSortable\Sortable;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use Sortable;

    public $sortable = [
        'id',
        'updated_at']
    ;
}
