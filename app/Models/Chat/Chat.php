<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chat';

    public function comments()
    {
        return $this->hasMany('App\Messages', 'chatUUID', 'uuid');
    }
}
