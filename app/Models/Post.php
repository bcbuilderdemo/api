<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Uploads\PostFile;

class Post extends Model
{
    protected $table = "posts";
    public $fillable = [
        'userID',
        'post'
    ];

    /**
     * Relation with User.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->BelongsTo(User::class, 'userID');
    }

    /*
     * Return PostFiles Object related to post
     */
    public function post_files()
    {
        return $this->hasMany(PostFile::class, 'postID');
    }
}
