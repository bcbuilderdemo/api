<?php

namespace App\Models\CompanyInfo;

use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{
    protected $table            = 'company_info';
    protected $primaryKey       = 'userID';

}
