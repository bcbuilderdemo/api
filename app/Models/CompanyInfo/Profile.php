<?php

namespace App\Models\CompanyInfo;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table            = 'profile';
    protected $primaryKey       = 'userID';

    protected $fillable = [
        'aboutUs',
        'twitterURL',
        'facebookURL',
        'instagramURL',
        'pinterestURL',
        'youtubeURL',
        'googlePlusURL',
        'website'
    ];
}
