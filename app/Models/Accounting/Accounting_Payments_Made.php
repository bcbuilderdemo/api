<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class Accounting_Payments_Made extends Model
{
    protected $table = 'accounting_payments_made';
}
