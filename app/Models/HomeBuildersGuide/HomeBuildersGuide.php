<?php

namespace App\models\HomeBuildersGuide;

use Illuminate\Database\Eloquent\Model;
use App\Models\Subcontractors\SubcontractorTrades;
use App\Models\TradeBuilderGuide\TradeBuilderGuide;
use App\Models\Subcontractors\Subcontractors;

class HomeBuildersGuide extends Model
{
    protected $table = "home_builders_guide";

    /**
     * @var array
     */
    public $fillable = [
        'SctID',
        'stepName',
        'stepNum',
        'stepDescription',
        'is_trade'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function subcontractor_trades()
    {
        return $this->hasOne(SubcontractorTrades::class, 'id', 'sctID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function trade_builder_guide()
    {
        return $this->hasOne(TradeBuilderGuide::class, 'hbg_id', 'id');
    }
    
     public function subcontractor_count()
    {
        return $this->hasMany(Subcontractors::class,'scTradeID','sctID');
    }   
}
