<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subproject;
use App\User;
use App\Models\Bids\BidsLineItem;
use App\Models\CompanyInfo\CompanyInfo;
use App\Models\Uploads\ProjectFile;

class Bid extends Model
{

    protected $table = 'bids';

    protected $fillable = [
        'projectID',
        'builderID',
        'scTradeID',
        'additionalDetails',
        'subprojectID',
        'comments',
        'amount',
        'status',
        'subcontractorID',
        'is_bid_out'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subproject()
    {
        return $this->BelongsTo(Subproject::class, 'subprojectID', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company_info()
    {
        return $this->BelongsTo(CompanyInfo::class, 'builderID', 'userID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subcontractor()
    {
        return $this->BelongsTo(User::class, 'subcontractorID', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function bidsLineItems()
    {
        return $this->hasMany(BidsLineItem::class, 'bids_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function project_files()
    {
        return $this->hasMany(ProjectFile::class, 'bidID', 'id');
    }
}
