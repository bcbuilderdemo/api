<?php

namespace App\Models\Bids;
use Bouncer;
use Illuminate\Database\Eloquent\Model;

class BuilderBidsOut extends Model
{


    protected $table = 'builder_bids_out';

    protected $fillable = [
        'bidID',
        'builderID',
        'scID'
    ];
}
