<?php

namespace App\Models\Bids;

use Illuminate\Database\Eloquent\Model;
use App\Bid;

class BidsLineItem extends Model
{
    protected $table = 'bids_line_items';

    protected $fillable = [
        'bids_id',
        'details',
        'quantity',
        'price'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bid()
    {
        return $this->BelongsTo(Bid::class, 'bids_id', 'id');
    }
}
