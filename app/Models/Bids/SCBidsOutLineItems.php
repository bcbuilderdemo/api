<?php

namespace App\Models\Bids;

use Illuminate\Database\Eloquent\Model;

class SCBidsOutLineItems extends Model
{
    protected $table = 'sc_bids_out_line_items';
    protected $fillable = [
        'name',
        'deleted',
        'description',
        'quantity',
        'uom',
        'price',
        'lineTaxTotal',
        'lineSubTotal',
        'lineTotal'
    ];
}
