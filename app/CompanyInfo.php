<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Subcontractors\Subcontractors;

class CompanyInfo extends Model
{

    protected $primaryKey = 'userID';
    protected $table      = 'company_info';

    protected $fillable = [
        'company',
        'companySlug',
        'contactFirstName',
        'contactLastName',
        'address',
        'address2',
        'phoneNumber',
        'phoneNumber2',
        'city',
        'postalCode',
        'province',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->BelongsTo(User::class, 'userID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subcontractor()
    {
        return $this->BelongsTo(Subcontractors::class, 'userID', 'userID');
    }
}
