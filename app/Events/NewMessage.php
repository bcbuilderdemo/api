<?php

namespace App\Events;

use App\CompanyInfo;
use App\Models\Chat\Message;
use App\Models\Uploads\ChatUploads;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Helpers;
class NewMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected     $message;
    protected     $user;

    public $queue = 'chat';


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message, $user)
    {

        $this->message = $message;
        $this->user = $user;

    }

    protected function prepareData()
    {

        $bci        = CompanyInfo::find($this->message->builderID);
        $sci        = CompanyInfo::find($this->message->scID);

        $user       = $this->user;

        if ($user->isA('builder')) {
            $this->message->currentUserLogo    = $bci->logoURL;
            $this->message->otherUserLogo      = $sci->logoURL;
        }
        else {
            $this->message->currentUserLogo    = $sci->logoURL;
            $this->message->otherUserLogo      = $bci->logoURL;
        }

        /*if ($this->message->fileUploaded == 1) {
            $upload   = ChatUploads::find($this->message->chatUploadID);
        }*/

        if ($user->isA('builder')) {


            return [
                'currentUserID'         => $user->id,
                'chatID'                => $this->message->chatID,
                'builderID'             => $this->message->builderID,
                'scID'                  => $this->message->scID,
                'sentByID'              => $this->message->sentByID,
                'message'               => $this->message->message,
                'created_at'            => $this->message->created_at,
                'currentUserLogoURL'    => $bci->logoURL,
                'otherUserLogoURL'      => $sci->logoURL,
                'bci_company'           => $bci->company,
                'sci_company'           => $sci->company,
                //'fileUUID'              => $upload->fileUUID ?? ''
            ];
    }
    else {
            return [
                'currentUserID'         => $user->id,
                'chatID'                => $this->message->chatID,
                'builderID'             => $this->message->builderID,
                'scID'                  => $this->message->scID,
                'sentByID'              => $this->message->sentByID,
                'message'               => $this->message->message,
                'created_at'            => $this->message->created_at,
                'otherUserLogoURL'      => $sci->logoURL,
                'currentUserLogoURL'     => $bci->logoURL,
                'bci_company'           => $bci->company,
                'sci_company'           => $sci->company,
                //'fileUUID'              => $upload->fileUUID ?? ''
            ];
        }
    }

    public function broadcastWith()
    {
        return [
            'message' => $this->prepareData(),
        ];
    }


    public function broadcastAs()
    {
        return 'new.message';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat-'.$this->message->chatID);
        //return new PrivateChannel('chat');
    }


}
