<?php

namespace App\Http\Requests\Subproject;

use App\Http\Requests\BaseRequest;

class SubprojectUpdate extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'            => 'sometimes|required|exists:subprojects,id',
            'project_id'    => 'sometimes|required|exists:projects,id',
            'hbg_id'        => 'sometimes|required|exists:home_builders_guide,id',
            'is_filled'     => 'sometimes|required|in:0,1',
            'is_available'  => 'sometimes|required|in:0,1',
            'description'   => 'sometimes|nullable|string',
            'comments'      => 'sometimes|nullable|string',
            'start_subproject_date' => 'sometimes|nullable|date_format:Y-m-d',
            'bid_close_date' => 'sometimes|nullable|date_format:Y-m-d'
        ];
    }
}
