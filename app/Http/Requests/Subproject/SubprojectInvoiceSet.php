<?php

namespace App\Http\Requests\Subproject;

use App\Http\Requests\BaseRequest;

class SubprojectInvoiceSet extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bid.id'        => 'nullable|exists:bids,id',
            'bid.amount'    => 'nullable|numeric',
            'bid.comments'  => 'nullable|string'
        ];
    }
}
