<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class UserCreate extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8',
            'isSubContractor' => 'nullable|integer',
            'isBuilder' => 'nullable|integer',
            'userType' => 'required|string',
            'verified' => 'required|in:0,1',
            'references' => 'required|string',
            'associations' => 'required|string',
            'certifications' => 'required|string',
            'is_terms_accepted' => 'required|in:0,1',
            'is_tutorial_viewed' => 'required|in:0,1',

            'company_info.company' => 'required|string',
            'company_info.contactFirstName' => 'required|string',
            'company_info.contactLastName' => 'required|string',
            'company_info.address' => 'required|string',
            'company_info.address2' => 'nullable|string',
            'company_info.phoneNumber' => 'required|string',
            'company_info.phoneNumber2' => 'required|string',
            'company_info.city' => 'required|string',
            'company_info.postalCode' => 'required|string',
            'company_info.province' => 'required|string',

            'trade_info.scTradeID' => 'sometimes|required|integer',
            'trade_info.max_project_size' => 'sometimes|required|integer|min:1',
            'trade_info.min_employee' => 'sometimes|required|integer|min:1',
            'trade_info.max_employee' => 'sometimes|required|integer|min:1',
            'trade_info.rating' => 'sometimes|required|integer|min:0|max:100',
        ];

        return $rules;
    }
}
