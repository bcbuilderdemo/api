<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class UserUpdate extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id' => 'required|exists:users,id',
            'email' => 'required|string',
            'isSubContractor' => 'nullable|integer',
            'isBuilder' => 'nullable|integer',
            'userType' => 'required|string',
            'verified' => 'required|in:0,1',
            'references' => 'required|string',
            'associations' => 'required|string',
            'certifications' => 'required|string',
            'is_terms_accepted' => 'required|in:0,1',
            'is_tutorial_viewed' => 'required|in:0,1',
            'trade_info.max_project_size' => 'sometimes|required|integer|min:1',
            'trade_info.min_employee' => 'sometimes|required|integer|min:1',
            'trade_info.max_employee' => 'sometimes|required|integer|min:1',
            'trade_info.rating' => 'sometimes|required|integer|min:0|max:100',
        ];

        return $rules;
    }
}
