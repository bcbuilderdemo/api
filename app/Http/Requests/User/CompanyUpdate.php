<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class CompanyUpdate extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'userID' => 'required|exists:users,id',
            'company' => 'required|string',
            'contactFirstName' => 'required|string',
            'contactLastName' => 'required|string',
            'address' => 'required|string',
            'address2' => 'nullable|string',
            'phoneNumber' => 'required|string',
            'phoneNumber2' => 'required|string',
            'city' => 'required|string',
            'postalCode' => 'required|string',
            'province' => 'required|string',
        ];

        return $rules;
    }
}
