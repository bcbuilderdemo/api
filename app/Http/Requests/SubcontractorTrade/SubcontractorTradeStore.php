<?php

namespace App\Http\Requests\SubcontractorTrade;

use App\Http\Requests\BaseRequest;
use Illuminate\Contracts\Validation\Validator;
use Prologue\Alerts\Facades\Alert;
use Illuminate\Http\Exceptions\HttpResponseException;

class SubcontractorTradeStore extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tradeName' => 'required|string',
            'description' => 'required|string',
        ];
    }

    /**
     *
     */
    protected function failedValidation(Validator $validator)
    {
        Alert::error('Please fill out all fields')->flash();

        throw new HttpResponseException(redirect()->route('sct.index')->with('alerts', Alert::all()));
    }
}
