<?php

namespace App\Http\Requests\HBG;

use App\Http\Requests\BaseRequest;
use Illuminate\Contracts\Validation\Validator;
use Prologue\Alerts\Facades\Alert;
use Illuminate\Http\Exceptions\HttpResponseException;

class HbgStore extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stepName' => 'required|string',
            'stepDescription' => 'required|string',
            'sctID' => 'required|exists:subcontractor_trades,id',
        ];
    }

    /**
     *
     */
    protected function failedValidation(Validator $validator)
    {
        Alert::error('Please fill out all fields')->flash();

        throw new HttpResponseException(redirect()->route('hbg.index')->with('alerts', Alert::all()));
    }
}
