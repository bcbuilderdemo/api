<?php

namespace App\Http\Controllers;

use App\Models\Subcontractors\Subcontractors;
use App\Models\SubcontractorTrades\SubcontractorTrades;
use App\models\HomeBuildersGuide\HomeBuildersGuide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Project;
use Tymon\JWTAuth\Facades\JWTAuth;
use Bouncer;
use App\Helpers\DBHelpers;
use App\Helpers\AuthHelpers;
use App\Bid;
use Illuminate\Support\Facades\File;

class SubcontractorsController extends Controller
{
    private function getAuthorizedUser() {
        return Auth::user();
    }

    private function getAuthorizedUserID() {
        return Auth::user()->id;
    }

    public function createValidator(array $data)
    {

        /*
         * Validate the HTTP request.
         * Only jpeg, pmg, jpg, gif, and svg formats are allowed.
         * Max size 16mb.
         */
        $rules = [
            'tradeName'       =>             'required|string|min:2|max:250',
            'description'     =>             'required|string|min:2|max:1000',
            'image'          =>              'required|image|mimes:jpeg,png,jpg,svg|max:10240',
        ];

        $messages = [
            'tradeName.required'            =>  'The Trade Name cannot be empty',
            'description.required'          =>  'The Description cannot be empty',
            'tradeName.string'              =>  'The trade name must be a string',
            'description.string'            =>  'The description must be a string',
            'tradeName.min'                 =>  'The trade name must be a minimum of 2 characters',
            'description.min'               =>  'The description must be a minimum of 2 characters',
            'tradeName.max'                 =>  'The trade name cannot exceed 100 characters',
            'description.max'               =>  'The description cannot exceed 250 characters',
            'image.required'                => 'You must choose an Image',
            'image.image'                   => 'You can only upload images',
            'image.mimes'                   => 'The Image format must be one of: jpeg/jpg, png, or svg',
            'image.max'                     => 'The Image cannot be larger than 10mb',
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function create(Request $request)
    {
        $user = AuthHelpers\getAuthorizedUser();
        // TODO: TURN ON AUTHENTICATION BEFORE DEPLOYING TO PRODUCTION SERVER
        /*if ($user->isNotAn('admin')) {
            return AuthHelpers\buildUnAuthorizedResponseAndLog("User tried to create SC Trade", $request);
        }*/

        $tradeName  = $request->get('tradeName');
        $scT        = SubcontractorTrades::where('tradeName', $tradeName)->first();
        if ( ! $scT) {
            $scT                    = new SubcontractorTrades();
            $scT->sctNameSlug       = str_slug(str_replace('&', 'and', $request->get('tradeName')));

        }

        $scColumns  = $scT->getFillable();
        $scInput    = $request->only($scColumns);
        $validator  = $this->createValidator($scInput);

        if ($validator->fails()) { return DBHelpers\returnValidationError($validator); }

        $scT->tradeName      = $request->get('tradeName');
        $scT->description    = $request->get('description');

        $file                = $request->file('image');
        $ext                 = $file->getClientOriginalExtension();
        $fileNameAndType     = "$scT->sctNameSlug.$ext";

        $storagePath    = storage_path('app/public');
        $assetsPath     = ("$storagePath/assets");
        $uploadDir      = ("$assetsPath/sc_icons");

        // Make new folder for uploads
        if ( ! File::exists($storagePath)) {
            File::makeDirectory($storagePath, 0775);
        }
        if ( ! File::exists($assetsPath)) {
            File::makeDirectory($assetsPath, 0775);
        }
        if ( ! File::exists($uploadDir)) {
            File::makeDirectory($uploadDir, 0775);
        }

        $fp     = $uploadDir . '/' . $fileNameAndType;

        if ($scT->imageLocation != null) {
            if (File::exists($fp)) {
                unlink($fp);
            }
        }

        $scT->imageLocation = "app/public/assets/sc_icons/$fileNameAndType";
        $scT->imageURL      = "assets/sc_icons/$fileNameAndType";
        $scT->imageName     = $fileNameAndType;

        try
        {
            $file->move($uploadDir, $fileNameAndType);
            $scT->save();
        }
        catch (Exception $ex)
        {
            return DBHelpers\returnDBError($ex);
        }
        return response()->json(['message' => $scT->tradeName . ' has been saved'], 200);
    }
//-----------jonas---------------start//
    public function getAllSubcontractors()
    {
        $scs =  Subcontractors::with(['user'])
                ->paginate(1000);
        return response($scs, 200);
    }

    public function getTradeBySubContractor($scTradeID)
    {
        $sct = SubcontractorTrades::findOrFail($scTradeID);
        return response($sct, 200);
    }

    public function getHbgBySctID($scTradeID)
    {
        $hbgs = HomeBuildersGuide::with(['trade_builder_guide', 'trade_builder_guide.subcontractor_trades'])
            ->where('sctID', $scTradeID)
            ->paginate(100);
        return response($hbgs);
    }

    public function getScTradeID(Request $request)
    {
        $user = AuthHelpers\getAuthorizedUser();
        if ($user->isBuilder == '1') {
            $message = "Builder tried getting Subcontractor tradeID";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }
        $subcontractor = Subcontractors::where('userID', $user->id)->first();
        return response()->json(['scTradeID' => $subcontractor->scTradeID]);
    }
//------------jonas---------------end//

    public function getAll()
    {
        /*$scs =  SubcontractorTrades
                ::orderByRaw('tradeName', 'ASC')
                ->paginate(1000);*/

  $scs = SubcontractorTrades::leftJoin('subcontractors as sc', 'subcontractor_trades.id', '=', 'sc.scTradeID' )
  ->selectRaw('subcontractor_trades.*, count(sc.scTradeID) as SubcontractorCount')
  ->groupBy('sc.scTradeID')
  ->orderByRaw('subcontractor_trades.tradeName', 'ASC')
  ->paginate(1000);

        return response($scs, 200);
    }

    // for register select
    public function getTrades()
    {
        $scs =  SubcontractorTrades
                ::orderByRaw('tradeName', 'ASC')
                ->select('id', 'tradeName')
                ->get();
        return response()->json(['data' => $scs], 200);
    }

    public function getSCTs()
    {
        $scs =  SubcontractorTrades
                ::orderByRaw('tradeName', 'ASC')
                ->paginate(10);
        return response()->json(['data' => $scs], 200);
    }

    public function getSCByTrade($scTradeID)
    {

        $scs =  SubcontractorTrades
                ::orderByRaw('tradeName', 'ASC')
                ->select(
                    'subcontractor_trades.*',
                             'sc.userID as scID',
                             'ci.userID as scUserID', 'ci.company', 'ci.phoneNumber',
                             'ci.address', 'ci.address2', 'ci.city', 'ci.province',
                             'ci.postalCode', 'ci.companySlug', 'ci.logoURL'
                        )
                ->join('subcontractors as sc', 'subcontractor_trades.id', '=', 'sc.scTradeID')
                ->join('company_info as ci', 'sc.userID', '=', 'ci.userID')
                ->where('sc.scTradeID', $scTradeID)
                ->paginate(1000);


//        foreach ($scs as $sc) {
//            $sc->checked = false;
//        }

        return response($scs, 200);
    }

    public function getSCSAndBBO($projectID, $scTID, Request $request)
    {


        // if (AuthHelpers\checkIfBuilderCanManageProjectByProjectID($projectID) == false) {
        //     $message = "User tried viewing subcontractors and BBO for project {$projectID} and sctid: {$scTID}";
        //     return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        // }


        $scs =  SubcontractorTrades
                ::orderByRaw('tradeName', 'ASC')
                //->join('subcontractors as sc', 'subcontractor_trades.id', '=', 'sc.scTradeID')
               //->join('company_info as ci',    'sc.userID', '=', 'ci.userID')
                ->join('subcontractors as sc',  function($join) {
                    $join->on('subcontractor_trades.id', '=', 'sc.scTradeID');
                    //->where('subcontractor_trades.id', '=', 'sc.scTradeID');
                })
                ->join('company_info as ci', function($join) {
                    $join->on('sc.userID', '=', 'ci.userID');
                    //->where('sc.userID', '=', 'ci.userID');
                })
                ->join('profile as p', function($join) {
                    $join->on('sc.userID', '=', 'p.userID');
                    //->where('sc.userID', '=', 'ci.userID');
                })
                // ->join('chat as c', function($join) use($projectID) {
                //     $join->on('sc.userID', '=', 'c.scID');
                //          //->where('c.projectID', '=', $projectID);
                // })
                ->where('sc.scTradeID', $scTID)
                //->where('c.projectID', $projectID)
                ->select(
                    'subcontractor_trades.*',
                    'sc.userID as scID',
                    'ci.userID as scUserID', 'ci.company', 'ci.phoneNumber',
                    'ci.userID as scUserID', 'ci.company',
                    'ci.address', 'ci.address2', 'ci.city', 'ci.province',
                    'ci.postalCode', 'ci.companySlug', 'ci.logoURL',
                    'sc.max_project_size', 'sc.min_employee', 'sc.max_employee', 'sc.rating',
                    // 'c.uuid as chatUUID',
                    'p.website', 'p.aboutUs'
                )
                ->groupBy('sc.userID')
                ->paginate(10000);

        if ($scs->count() < 1) {
            // Todo: Lazy!!! Need to change this once I have time if no SCs are signed up for trade
           $noscs =  SubcontractorTrades::find($scTID);
           return response($noscs, 200);
        }




        /*$bbo =      Bid
                    ::leftJoin('builder_bids_out as bbo', 'bids.id', '=', 'bbo.bidID')
                    ->select('bbo.scID', 'bbo.created_at as bbo_created_at', 'bbo.scResponded', 'bbo.scResponded')
                    ->where('bids.scTradeID', $scTID)
                    ->where('bids.projectID', $projectID)
                    ->get();

        $items =  $scs->items();
        foreach ($items as $sc) {
            foreach ($bbo as $bo) {
                if ($bo->scID == $sc->scUserID) {
                    $sc->sentToSC = 1;
                    $sc->bbo_created_at = $bo->bbo_created_at;
                }
            }
        }*/

            /*foreach ($bbo as $bo) {
                if ($bo->scID == $sc->scUserID) {
                    $sc->sentToSC = 1;
                }
            }*/


//        foreach ($scs as $sc) {
//            $sc->checked = false;
//        }

        return response($scs, 200);
    }

    public function getSCTsForProject($projectID, Request $request)
    {

        if (AuthHelpers\checkProjectOwnershipByProjectID($projectID) == false) {
            $message = "User tried to retrieve SCTs for Project $projectID";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        $user       = AuthHelpers\getAuthorizedUser();

        $scts           = Project
                            ::join('bids as b', function($join) use ($user) {
                                $join->on('projects.id', '=', 'b.projectID')
                                    ->where('b.builderID', '=', $user->id);
                            })
                            ->join('subcontractor_trades as sct', 'b.scTradeID', '=', 'sct.id')
                            ->where('projects.id', $projectID)
                            ->Where('projects.userID', $user->id)
                            ->orderByRaw('tradeName', 'ASC')
                            ->paginate(10);

        return response()->json(['data' => $scts], 200);

    }

    public function getSCTsForAllBids()
    {

        $user       = AuthHelpers\getAuthorizedUser();

        $scts           = Bid
                            ::join('subcontractor_trades as sct', function($join) use ($user) {
                                $join->on('bids.scTradeID', '=', 'sct.id')
                                    ->where('bids.builderID', '=', $user->id);
                            })
                            ->orderByRaw('tradeName', 'ASC')
                            ->paginate(10);

        return response()->json(['data' => $scts], 200);

    }
}
