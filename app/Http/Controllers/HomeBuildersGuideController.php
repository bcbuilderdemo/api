<?php

namespace App\Http\Controllers;

use App\Models\HomeBuildersGuide\HomeBuildersGuide;
use Illuminate\Http\Request;

class HomeBuildersGuideController extends Controller
{
    public function get()
    {

        $hbg = HomeBuildersGuide
                ::select('stepName', 'sctID', 'stepNum', 'stepDescription')
                ->orderByRaw('stepNum ASC')
                ->get();
        return response()->json(['hbg' => $hbg], 200);
    }
}
