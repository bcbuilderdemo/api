<?php

namespace App\Http\Controllers;

use App\Models\Bids\BuilderBidsOut;
use App\Models\Uploads\FileUploads;
use App\Models\Uploads\Upload;
use Illuminate\Http\Request;

use App\Helpers\DBHelpers;
use App\Helpers\AuthHelpers;
use App\Helpers\GeneralHelpers;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;

class ManageUploadsController extends Controller
{


    public function deleteFile($projectID, $bidID, $scTradeID, $fileID) {
        $userID             =  AuthHelpers\getAuthorizedUserID();
        $file               =  FileUploads::find($fileID);
        if (AuthHelpers\checkFileUploadOwnershipByFile($file) === false) { return AuthHelpers\returnUnAuthorizedResponse(); }

        //$fp = storage_path('app/uploads/' .$userID .'/' .$projectID  .'/' .$bidID .'/' .$scTradeID .'/' .$file->fileName);
        $fp = $file->filePath;

        try
        {
            if (File::exists($fp))
            {
                unlink($fp);
                $file->deleted = 1;
            }
            else
            {
                return response()->json(['error' => 'File does not exist'], 404);

            }
        }
        catch (Exception $ex)
        {
            return DBHelpers\returnDBError($ex);
        }

        $file->save();

        return response()->json(['message' => $file->fileName .' has been deleted'], 200);

    }

    public function getFilesForProjectOrBid($projectID, Request $request)
    {
        $user   = AuthHelpers\getAuthorizedUser();
        $bidID  = $request->bidID;

        $query =    Upload
                    ::where('projectID', $projectID)
                    ->join('file_uploads as fu', 'uploads.id', '=', 'fu.uploadsID')
                    ->select('fu.*');

        

        if ($user->isA('subcontractor'))
        {
            $files = $query
                        ->where('uploads.bidID', $bidID)
                        ->get();
        }
        else if ($user->isA('builder'))
        {
           $files =  $query->get();
        }
        else
        {
            $message = 'User tried to access files for Project/Bid ID'. $projectID.'/'.$bidID;
            return AuthHelpers\returnUnAuthorizedResponseAndLog( $message, $request->route()->getActionMethod(), $request->path(), $request->ip());
        }

        $sp = storage_path('public/assets/file_icons/');
        foreach ($files as $file)
        {
            $fp = $sp . $file->fileType . '_icon.png';
            if (File::exists($fp)) {
                $file->iconURL = Storage::url('assets/file_icons/' . $file->fileType . '_icon.png');
            } else {
                $file->iconURL = Storage::url('assets/file_icons/default_icon.png');
            }
        }
        return response()->json(['data' => $files], 200);
    }

    public function getFile($fileID, Request $request)
    {
        $user           = AuthHelpers\getAuthorizedUser();

        if ($user->isA('subcontractor') == true) {

            $file     = FileUploads::where('fileID', $fileID)->first();
            $bbo      = BuilderBidsOut::where('bidID', $file->bidID)->where('scID', $user->id)->first();
            // TODO: NOT TESTED. CHANGED FROM ONLY CHECKING BID ID TO BID & PROJECT ID
            if (AuthHelpers\checkIfSCCanViewBuilderBidOutByBidIDAndProjectID($file->bidID, $bbo->projectID) == false) {
                $message = 'SC tried to view file '.$file->id;
                return AuthHelpers\returnUnAuthorizedResponseAndLog($message, $request->route()->getActionMethod(), $request->path(), $request->ip());
            }
            return response()->file($file->filePath, [
                'Content-Type'          => $file->mimeType,
                "Content-Disposition'   => 'attachment; filename='$file->fileName''",
            ]);
        }
        else if ($user->isA('builder') == true) {
            $file     = FileUploads::where('fileID', $fileID)->first();
            if (AuthHelpers\checkFileUploadOwnershipByFile($file) == false)  {
                $message = 'SC tried to view file '.$file->id;
                return AuthHelpers\returnUnAuthorizedResponseAndLog($message, $request->route()->getActionMethod(), $request->path(), $request->ip());
            }
            return response()->file($file->filePath, [
                'Content-Type'          => $file->mimeType,
                "Content-Disposition'   => 'attachment; filename='$file->fileName''",
            ]);
            /*return response($file->filePath, 200, [
                'Content-Type' => $file->mimeType,
                "Content-Disposition'   => 'attachment; filename='$file->fileName''",
            ]);*/
        }
    }
}
