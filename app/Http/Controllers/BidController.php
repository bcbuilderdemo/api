<?php

namespace App\Http\Controllers;
use App;
use App\Bid;
use App\Models\Bids\BidsLineItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;
use App\Models\Bids\BuilderBidsOut;
USE App\Models\Bids\SCBidsOut;
USE App\Models\Bids\SCBidsOutLineItems;
use Bouncer;
use Exception;
use App\Helpers\DBHelpers;
use App\Helpers\AuthHelpers;
use App\Helpers\GeneralHelpers;
use Illuminate\Support\Facades\File;
use App\Models\Uploads\Upload;
use App\Models\Uploads\FileUploads;
use App\Project;
use App\Models\Subcontractors\SubcontractorTrades;
use App\Models\Accounting\Accounting;
use App\Models\Accounting\Accounting_Payments_Made;
use Webpatser\Uuid\Uuid;

class BidController extends Controller
{

    public function __construct()
    {

        //TODO:: CHANGE TO scID
        //Bouncer::ownedVia('userID');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }


    function checkProjectOwnershipByProjectID($id)
    {
        $user = AuthHelpers\getAuthorizedUser();
        $project = Project::find($id);

        if ($user->cant('manage', $project)) {
            return response()->json(['error' => 'You are not authorized to submit a bid for this project'], 401);
        } else {
            return true;
        }
    }

//--------------------------jonas start
    public function getBidsLineItemsByBidID($bidID)
    {
        $blis = BidsLineItem::with(['bid'])
            ->where('bids_id', $bidID)->get();
        return response()->json($blis);
    }

    public function updateBid(Request $request)
    {
        $id = $request->get('id');
        $status = $request->get('status');
        Bid::findOrFail($id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['message' => 'Successfully Accepted']);
    }

    
    public function getAllBidsForProject($projectID)
    {
        $bids = Bid::with(['bidsLineItems', 'subcontractor', 'subproject'])
            ->where('projectID', $projectID)->get();
        return response()->json(['bids' => $bids]);
    }

    public function getAllBidsByProjectID($projectID)
    {
        $bids = Bid::with(['subproject', 'subproject.hbg', 'subcontractor', 'subcontractor.company_info', 'subcontractor.profile'])
            ->where('projectID', $projectID)            
            ->get()
            ->groupBy('subprojectID');
        return response()->json(['bids' => $bids]);
    }
//--------------------------jonas end
    public function createBidValidator(array $data)
    {

        /*
         * Validate the HTTP request.
         * Only jpeg, pmg, jpg, gif, and svg formats are allowed.
         * Max size 16mb.
         */
        $rules = [
            'additionalDetails' => 'nullable|min:1|max:250',
            'files.*'            => 'nullable',
        ];

        $messages = [
            'additionalDetails.string' => 'Additional Details can only be a string',
            'additionalDetails.min' => 'Additional Details cannot be less than 1 character',
            'additionalDetails.max' => 'Additional Details cannot exceed 250 characters',
        ];

        return Validator::make($data, $rules, $messages);
    }


    public function getDirectory($projectID, $bidID, $scTradeID)
    {
        $storagePath    = storage_path('app/uploads');
        $userPath       = ($storagePath.'/'.AuthHelpers\getAuthorizedUserID());
        $projectPath    = ($userPath . '/' . $projectID);
        $bidPath        = ($projectPath . '/' . $bidID);
        $uploadDir      = ($bidPath . '/' . $scTradeID);

        // Make new folder for uploads
        if ( ! File::exists($storagePath)) {
            File::makeDirectory($storagePath, 0775);
        }
        if ( ! File::exists($userPath)) {
            File::makeDirectory($userPath, 0775);
        }
        if ( ! File::exists($projectPath)) {
            File::makeDirectory($projectPath, 0775);
        }
        if ( ! File::exists($bidPath)) {
            File::makeDirectory($bidPath, 0775);
        }
        if ( ! File::exists($uploadDir)) {
            File::makeDirectory($uploadDir, 0775);
        }
        return $uploadDir;
    }

    public function deleteDirectory($uploadDir)
    {
        File::cleanDirectory($uploadDir);
    }


    public function findOrCreateBid($projectID, $scTradeID)
    {
        $bid    =   Bid
                    ::where('projectID', $projectID)
                    ->where('scTradeID', $scTradeID)
                    ->first();

        if ( ! $bid) {
            $bid                = new Bid();
            $bid->projectID     = $projectID;
            $bid->scTradeID     = $scTradeID;
            $bid->builderID     = AuthHelpers\getAuthorizedUserID();

            try
            {
                $bid->save();
            }
            catch (Exception $ex)
            {
                return DBHelpers\returnDBError($ex);
            }

        } else {
            if (AuthHelpers\checkBidOwnershipByBidID($bid->id) === false) { return AuthHelpers\returnUnAuthorizedResponse(); }
        }
        // TODO: CHECK FOR PERMISSIONS
        return $bid;
    }

    public function findOrCreateUpload($bidID, $projectID)
    {
        $upload    =   Upload::find($bidID);

        if ( ! $upload) {
            $upload             = new Upload();
            $upload->bidID      = $bidID;
            $upload->projectID  = $projectID;
            $upload->userID     = AuthHelpers\getAuthorizedUserID();

            try
            {
                $upload->save();
                AuthHelpers\giveUploadPermissionToUser($upload);
            }
            catch (Exception $ex)
            {
                return DBHelpers\returnDBError($ex);
            }
        } else {
            if (AuthHelpers\checkBidOwnershipByBidID($bidID) === false) { return AuthHelpers\returnUnAuthorizedResponse(); }
        }
        // TODO: CHECK FOR PERMISSIONS
        return $upload;
    }

    public function createOrUpdate($projectID, $scTradeID, Request $request)
    {

        //$bid         = $this->findOrCreateBid($projectID, $scTradeID);
        $bid    =   Bid
                    ::where('projectID', $projectID)
                    ->where('scTradeID', $scTradeID)
                    ->first();

        if ( ! $bid) {
            $bid                = new Bid();
            $bid->projectID     = $projectID;
            $bid->scTradeID     = $scTradeID;
            $bid->builderID     = AuthHelpers\getAuthorizedUserID();

            try
            {
                $bid->save();
            }
            catch (Exception $ex)
            {
                return DBHelpers\returnDBError($ex);
            }

        }
        else
        {
            $logMessage = "User tried to edit existing bid with id {$bid->id}, Project: {$projectID}, Trade: {$scTradeID}";
            $resMessage = "You cannot update this bid";
            return AuthHelpers\buildResponseWithLogMessageAndResponseMessage($resMessage, $logMessage, $request);
            //if (AuthHelpers\checkBidOwnershipByBidID($bid->id) === false) { return AuthHelpers\returnUnAuthorizedResponse(); }
        }
        $hasFiles    = $request->hasFile('files');

        if (AuthHelpers\checkProjectOwnershipByProjectID($projectID) === false) {
            return AuthHelpers\returnUnAuthorizedResponse();
        }

        $validate = $this->createBidValidator($request->only(['files.*', 'additionalDetails']));

        if ($validate->fails()) {
            return DBHelpers\returnValidationError($validate);
        }

        $bid->additionalDetails = $request->additionalDetails ?? 'None Specified';

        try {
            $bid->save();
            AuthHelpers\giveBidPermissionToBuilderByBid($bid);
        } catch (Exception $ex) {
            return DBHelpers\returnDBError($ex);
        }
        $bidID       = $bid->id;

        $uploadDir = $this->getDirectory($projectID, $bidID, $scTradeID);
        if ($hasFiles) {
            try
            {
                $upload             = $this->findOrCreateUpload($bidID, $projectID);
                $upload->updated_at = GeneralHelpers\getDateTime();
                try
                {
                        $upload->save();

                }
                catch (Exception $ex)
                {
                    return DBHelpers\returnDBError($ex);
                }
                foreach ($request->files as $arr) {
                    foreach($arr as $file) {
                        $name       = $file->getClientOriginalName();
                        $fName      = pathinfo($name)['filename'];
                        $ext        = $file->getClientOriginalExtension();
                        $uniqueName = $fName.' '.Carbon::now('America/Vancouver')->toDateTimeString().'.'.$ext;
                        $path       = $file->move($uploadDir, $uniqueName);

                        $fileUpload                     = new FileUploads();
                        $fileUpload->fileName           = $name;
                        $fileUpload->uniqueFileName     = $uniqueName;
                        $fileUpload->fileID             = Uuid::generate();
                        $fileUpload->fileType           = $ext;
                        $fileUpload->mimeType           = $file->getClientMimeType();
                        $fileUpload->filePath           = $path;
                        $fileUpload->bidID              = $bidID;
                        $fileUpload->uploadsID          = $upload->id;
                        $fileUpload->fileSize           = GeneralHelpers\getFileSize($file->getClientSize());
                        $fileUpload->save();

                        AuthHelpers\giveFileUploadsPermissionToUser($fileUpload);
                    }
                }


            }
            catch (Exception $ex)
            {
                // DONT DELETE IN CASE OF UPDATE YOU DONT WANT IT TO DELETE
                //if ($uploadDir) { $this->deleteDirectory($uploadDir); }
                //if ($upload)    { $upload->delete(); }
                //if ($bid)       { $bid->delete(); }
                return DBHelpers\returnDBError($ex);
            }
        }
        return response()->json(['message' => 'Your Bid form has been saved'], 200);
    }

    public function createOrUpdateFromSct($projectID, $hbgID, Request $request)
    {        
        return response()->json(['message' => 'Your Bid form has been saved'], 200);
    }

    /*public function sendBidToSC($bidID, $scID)
    {
        $bidOut             = new BuilderBidsOut();
        $bidOut->scID       = $scID;
        $bidOut->bidID      = $bidID;
        $bidOut->builderID  = Auth::user()->id;

        try {
            $bidOut->save();
            $this->giveBuilderBidOutPermissions($bidOut);
        } catch (Exception $ex) {
            $this->returnDBError($ex);
        }
        return response()->json(['message' => 'Your Bid has been sent out'], 200);
    }*/


    // Builder bids sent out to SC
    public function getBuilderBidsOut($projectID)
    {

    }

    public function getBuilderBidOutFormByTrade($projectID, $scTradeID)
    {
        $user           = AuthHelpers\getAuthorizedUser();
        $bid            = Bid
                            ::where('projectID', $projectID)
                            ->where('scTradeID', $scTradeID)
                            ->where('builderID', $user->id)
                            ->first();

        if($bid)
        {
            if ($user->cant('manage', $bid)) { return AuthHelpers\returnUnAuthorizedResponse(); }
            $files =  Upload
                        ::join('file_uploads as fu', 'uploads.bidID', '=', 'fu.bidID')
                        ->where('uploads.bidID', $bid->id)
                        ->where('fu.deleted', 0)
                        ->select('fu.*')
                        ->get();
            return response()->json(['bidForm' => $bid, 'files' => $files], 200);
        }
        else
        {
            return response()->json(['data' => $bid], 200);
        }

    }

    public function getSCForProject($projectID, $scTID) {
        /*$bidType = "";
        if ($bidType === 'out' || $bidType === 'in') {
            if      ($bidType === 'in') { $bidType = 'bidsIn'; }
            else if ($bidType === 'out') { $bidType = 'bidsOut'; }
        } else {
            return DBHelpers\returnInvalidInput($bidType);
        }*/
        $user   = AuthHelpers\getAuthorizedUser();

        $bidsOut   = Bid
                    ::where('projectID', $projectID)
                    ::where('scTradeID', $scTID)
                    ::where('userID', $user->id)
                    ->join('builder_bids_out as bbo', 'bids.id', '=', 'bbo.bidID')
                    ->get();

        $bidsIn   = Bid
                    ::where('projectID', $projectID)
                    ::where('scTradeID', $scTID)
                    ::where('userID', $user->id)
                    ->join('sc_bids_out as scbo', 'bids.id', '=', 'scbo.bidID')
                    ->get();

        if ($user->can('manage', $bidsIn) && $user->can('manage', $bidsOut))
        {
            return response()->json([
                                        'bidsOut' => $bidsOut,
                                        'bidsIn'  => $bidsIn,
                                     ], 200);
        } else
        {
            return AuthHelpers\returnUnAuthorizedResponse();
        }

    }

    public function sendBidToSCSValidator(array $data)
    {

        /*
         * Validate the HTTP request.
         * Only jpeg, pmg, jpg, gif, and svg formats are allowed.
         * Max size 16mb.
         */
        $rules = [
            '*.scID' =>                 'required|exists:subcontractors,userID',
            //'*.scID' =>               'required|exists:subcontractors,userID',
        ];

        $messages = [
            '*.scID' =>                 'Required fields are missing',
            '*.exists' =>               'You have entered invalid input',
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function findOrCreateBBO($bidID, $builderID, $scID)
    {
        $bbo =          BuilderBidsOut
                        ::where('bidID', $bidID)
                        ->where('builderID', $builderID)
                        ->where('scID', $scID)
                        ->first();

        if ( ! $bbo) {
            $bbo                    = new BuilderBidsOut();
            $bbo->bidID             = $bidID;
            $bbo->scID              = $scID;
            $bbo->builderID         = $builderID;
        }
        else
        {
            $bbo->updated           = 1;
            $bbo->scViewed          = 0;
            $bbo->scResponded       = 0;
            $bbo->updated_at        = GeneralHelpers\getDateTime();
        }
        try
        {
            $bbo->save();
            AuthHelpers\giveBuilderAndSCBidOutPermissions($bbo);
        }
        catch (Exception $ex)
        {
            return DBHelpers\returnDBError($ex);
        }
        return $bbo;
    }

    //send bbo to sc
    public function sendBidToSelectedSCS($projectID, $scTradeID, $bidID, Request $request)
    {

        $data = $request->input('data');

        $validate = $this->sendBidToSCSValidator($data);

        $bbo = null;

        if (AuthHelpers\checkBidOwnershipByBidID($bidID) === false) { return AuthHelpers\returnUnAuthorizedResponse(); }
        $userID = AuthHelpers\getAuthorizedUserID();

        // TODO: HAVE TO CHANGE id to USERID in validator
        if ($validate->fails()) { return DBHelpers\returnValidationError($validate); }
        foreach ($data as $key => $value) {
            try
            {
               //$bbo                 = $this->findOrCreateBBO($bidID, $userID, $value['scID']);
                $bbo =          BuilderBidsOut
                                ::where('bidID', $bidID)
                                ->where('builderID', $userID)
                                ->where('scID', $value['scID'])
                                ->first();

                if ( ! $bbo) {
                    $bbo                    = new BuilderBidsOut();
                    $bbo->bidID             = $bidID;
                    $bbo->scID              = $value['scID'];
                    $bbo->builderID         = $userID;
                    $bbo->projectID         = $projectID;
                }
                else
                {
                    $bbo->updated           = 1;
                    $bbo->scViewed          = 0;
                    $bbo->scResponded       = 0;
                    $bbo->updated_at        = GeneralHelpers\getDateTime();
                }
                try
                {
                    $bbo->save();
                    AuthHelpers\giveBuilderAndSCBidOutPermissions($bbo);
                    $when = Carbon::now()->addSeconds(20);


                    dispatch(((new App\Jobs\SendNewBBOEmail($bbo))
                            ->onQueue('emails')
                            ->delay($when)));

                    $sc = User::find($bbo->scID);
                    $sc->notify((new App\Notifications\NewBBONotification($bbo))->delay($when));
                }
                catch (Exception $ex)
                {
                    return DBHelpers\returnDBError($ex);
                }
               $bbo->updated_at     = GeneralHelpers\getDateTime();
               $bbo->save();
                AuthHelpers\giveSCProjectViewPermissionByProjectID($projectID, $value['scID']);

            }
            catch (Exception $ex)
            {
                //if ($bbo) { $bbo->delete(); }
                return DBHelpers\returnDBError($ex);
            }
        }
        return GeneralHelpers\returnResponse('message', 'Your bids have been been sent!', 200);

        /*foreach ($data as $key => $value) {
            if (array_key_exists('scID', $value)) {
                print_r($value['scID']);
            } else {
                return DBHelpers\returnInvalidInput('scID');
            }
        }*/
    }

    public function getAllBidsByTrade($projectID, $scTradeID , $bidType, Request $request) {


        $acceptableParams   = ['in', 'default', 'all', 'project', 'project-all'];

        $user               = AuthHelpers\getAuthorizedUser();
        $userType           = $user->userType;

        if (! in_array($bidType, $acceptableParams)) { return DBHelpers\returnInvalidInput($bidType); }

        if ($bidType !== 'all') {
            $message    = 'User tried viewing Project '.$projectID;
            $method     = $request->route()->getActionMethod();
            $route      =  $request->path();
            $ip         = $request->ip();
            if ($user->isA('builder')) {
                if (AuthHelpers\checkProjectOwnershipByProjectID($projectID) === false) {
                    return AuthHelpers\returnUnAuthorizedResponseAndLog($message, $method, $route, $ip);
                }
            }
            else if ($user->isA('subcontractor')) {
                // if (AuthHelpers\checkIfSCCanViewProjectByProjectID($projectID) == false) {
                //     return AuthHelpers\returnUnAuthorizedResponseAndLog($message, $method, $route, $ip);
                // }
            }
            else
            {
                return AuthHelpers\returnUnAuthorizedResponseAndLog($message, $method, $route, $ip);
            }
        }


        switch($userType) {
            case 'subcontractor':
                return $this->getAllBidsByTradeForSubcontractor($projectID, $scTradeID, $bidType, $user->id);
                break;
            case 'builder':
                return $this->getAllBidsByTradeForBuilder($projectID, $scTradeID, $bidType, $user->id);
                break;
            case 'admin':
                break;
            default:
                return AuthHelpers\returnUnAuthorizedResponse();
                break;
        }
    }

    public function v2_getAllBidsForSubcontractor($filterCondition) 
    {
        $user = AuthHelpers\getAuthorizedUser();

        switch ($filterCondition) {
            case 'accepted':
                $q = Bid::with(['company_info'])
                    ->where('subcontractorID', $user->id)
                    ->where('status', 'accepted')
                    ->get();
                break;
            case 'renegotiated':
                $q = Bid::with(['company_info'])
                    ->where('subcontractorID', $user->id)
                    ->where('status', 'renegotiated')
                    ->get();
                break;
            case 'all-in':
                $q = Bid::with(['company_info'])
                    ->where('subcontractorID', $user->id)
                    ->where('is_bid_out', 1)
                    ->get();
                break;
            case 'all-out':
                $q = Bid::with(['company_info'])
                    ->where('subcontractorID', $user->id)
                    ->where('is_bid_out', 0)
                    ->get();
                break;
            default:
                break;

        }

        return response()->json($q);
    }


    public function getAllBidsByTradeForBuilder($projectID, $scTradeID, $bidType, $userID)
    {


        $queryIn = Project
                    ::join('bids as b', 'projects.id', '=', 'b.projectID')
                    ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                    ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                    ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                    ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
                         ->select(
                             'b.*',
                             'bbo.bidID as bidID', 'bbo.id as bboID', 'bbo.bbo_accepted', 'bbo.acceptedSCBOID as bidAcceptedSCBOID',
                             'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at', 'bbo.scResponded',
                             'scbo.id as scboID', 'scbo.builderViewed as bidViewedByBuilder',
                             'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                             'scbo.builderAccepted as builderAcceptedSCBO', 'scbo.builder_accepted_at as builderAcceptedAtSCBO',
                             'scbo.builder_rejected_at as builderRejectedAtSCBO',
                             'scbo.accountingID', 'scbo.builderAccepted as scbo_builderAccepted',
                             'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                             'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city',
                             'ci.companySlug', 'ci.uuid', 'ci.logoURL',
                             'c.uuid as chatUUID'
                    )
                    ->join('chat as c', function($join) use ($userID) {
                        $join->on('projects.id', '=', 'c.projectID')
                            ->where('c.builderID', $userID);
                    })
                    ->where('scbo.builderID', $userID)
                    ->groupBy('b.id');

        $queryOut = Project
                    ::join('bids as b', 'projects.id', '=', 'b.projectID')
                    ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                    ->leftJoin('sc_bids_out as scbo', 'projects.id', '=', 'scbo.projectID')
                    ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                    ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
                    ->join('chat as c', function($join) use ($userID) {
                        $join->on('projects.id', '=', 'c.projectID')
                            ->where('c.builderID', $userID);
                    })
                    ->select(
                        'b.*',
                        'bbo.bidID as bidID', 'bbo.id as bboID', 'bbo.bbo_accepted', 'bbo.acceptedSCBOID as bidAcceptedSCBOID',
                        'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at', 'bbo.scResponded',
                        'scbo.id as scboID', 'scbo.builderViewed as bidViewedByBuilder',
                        'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                        'scbo.builderAccepted as builderAcceptedSCBO', 'scbo.builder_accepted_at as builderAcceptedAtSCBO',
                        'scbo.builder_rejected_at as builderRejectedAtSCBO',
                        'scbo.accountingID', 'scbo.builderAccepted as scbo_builderAccepted',
                        'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                        'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city',
                        'ci.companySlug', 'ci.uuid', 'ci.logoURL',
                        'c.uuid as chatUUID'
                    )
                    ->where('bbo.builderID', $userID)
                    ->groupBy('b.id');

        $queryOut2 = Project
                    ::join('bids as b', 'projects.id', '=', 'b.projectID')
                    ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                    ->leftJoin('sc_bids_out as scbo', 'projects.id', '=', 'scbo.projectID')
                    ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                    ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
                    ->join('chat as c', function($join) use ($userID) {
                        $join->on('projects.id', '=', 'c.projectID')
                            ->where('c.builderID', $userID);
                    })
                    ->select(
                        'b.*',
                        'bbo.bidID as bidID', 'bbo.id as bboID', 'bbo.bbo_accepted', 'bbo.acceptedSCBOID as bidAcceptedSCBOID',
                        'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at', 'bbo.scResponded',
                        'scbo.id as scboID', 'scbo.builderViewed as bidViewedByBuilder',
                        'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                        'scbo.builderAccepted as builderAcceptedSCBO', 'scbo.builder_accepted_at as builderAcceptedAtSCBO',
                        'scbo.builder_rejected_at as builderRejectedAtSCBO',
                        'scbo.accountingID', 'scbo.builderAccepted as scbo_builderAccepted',
                        'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                        'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city',
                        'ci.companySlug', 'ci.uuid', 'ci.logoURL',
                        'c.uuid as chatUUID'
                    )
                    ->where('bbo.builderID', $userID)
                    ->groupBy('b.id');

        if ($bidType == 'default' && $scTradeID == 0) {
            // TODO: FIND OUT WHY REUSING $QUERY->OUT CHANGES TO ACCEPTED QUERY
            /*$sct        = SubcontractorTrades::orderBy('tradeName', 'asc')->first();
            $bidsOut    = $queryOut2->where('b.scTradeID', '=' , $sct->id)->where('projects.id', '=' , $projectID)->get();
            $accepted   = $queryOut->where('b.scTradeID', '=', $sct->id)->where('projects.id', '=',$projectID)->where('bbo.bbo_accepted', '=', '1')->get();
            $bidsIn     = $queryIn->where('b.scTradeID','=' , $sct->id)->where('projects.id', '=' , $projectID)->get();*/

            $accepted   = $queryOut->where('projects.id', '=',$projectID)->where('projects.id', '=',$projectID)->where('bbo.bbo_accepted', '=', '1')->get();
            $bidsIn     = $queryIn->where('projects.id', $projectID)->get();
            $bidsOut    = $queryOut2->where('projects.id', $projectID)->get();
        }
        else if ($bidType == 'default') {
            $sct        = SubcontractorTrades::orderBy('tradeName', 'asc')->first();
            $accepted   = $queryOut->where('b.scTradeID', '=', $sct->id)->where('projects.id', '=',$projectID)->where('bbo.bbo_accepted', '=', '1')->get();
            $bidsIn     = $queryIn->where('b.scTradeID','=' , $sct->id)->where('projects.id', '=' , $projectID)->get();
            $bidsOut    = $queryOut2->where('b.scTradeID', '=' , $sct->id)->where('projects.id', '=' , $projectID)->get();
        }
        elseif ($bidType == 'all') {
            $sct        = SubcontractorTrades::orderBy('tradeName', 'asc')->first();
            $scTradeID  = ($scTradeID == 0) ? $sct->id : $scTradeID;
            $bidsIn     = $queryIn->where('b.scTradeID', '=' , $scTradeID)->where('projects.userID','=' , $userID)->get();
            $bidsOut    = $queryOut2->where('b.scTradeID', '=' , $scTradeID)->where('projects.userID',  $userID)->get();
            $accepted   = $queryOut->where('b.scTradeID', '=', $scTradeID)->where('projects.userID', '=',$userID)->where('bbo.bbo_accepted', 1)->get();

        }
        elseif ($bidType == 'project-all' && $scTradeID == 0) {
            $accepted   = $queryOut->where('projects.id', '=',$projectID)->where('bbo.bbo_accepted', '=', '1')->get();
            $bidsIn     = $queryIn->where('projects.id', $projectID)->get();
            $bidsOut    = $queryOut2->where('projects.id', $projectID)->get();
        }
        else
        {
            $accepted   = $queryOut->where('b.scTradeID', '=', $scTradeID)->where('projects.id', '=',$projectID)->where('bbo.bbo_accepted', '=', '1')->get();
            $bidsIn     = $queryIn->where('b.scTradeID', $scTradeID)->where('projects.id', $projectID)->get();
            $bidsOut    = $queryOut2->where('b.scTradeID', $scTradeID)->where('projects.id', $projectID)->get();
        }

        return response()->json(['bidsIn' => $bidsIn, 'bidsOut' => $bidsOut, 'acceptedBids' => $accepted], 200);

    }



    public function getBidByTypeForSC($type, Request $request)
    {

        $user = AuthHelpers\getAuthorizedUser();
        $projectID = $request->projectID ?? '';

        $bidsIn = Project
                    ::join('bids as b', 'projects.id', '=', 'b.projectID')
                    ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                    ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                    ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                    ->join('company_info as ci', 'ci.userID', '=', 'b.builderID')
                    ->join('company_info as sci', 'bbo.scID', '=', 'sci.userID')
                    ->join('chat as c', function($join) use ($user) {
                        $join->on('bbo.scID', '=', 'c.scID')
                            ->where('c.scID', $user->id);
                    })
                    ->select(
                        'b.*',
                        'bbo.bidID as bidID', 'bbo.id as bboID',
                        'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at', 'bbo_accepted',
                        'bbo.created_at as bbo_createdAt',
                        'bbo.acceptedSCBOID as bidAcceptedSCBOID', 'bbo.scViewed as bbo_scViewed',
                        'bbo.scViewedAt as bbo_scViewedAt',
                        'scbo.id as scboID',
                        'scbo.accountingID', 'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                        'scbo.builderAccepted', 'scbo.builder_accepted_at', 'scbo.builder_rejected_at', 'scbo.updated as scbo_updated',
                        'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                        'ci.company', 'ci.phoneNumber', 'ci.companySlug', 'ci.logoURL as bci_logoURL',
                        'projects.address', 'projects.address2', 'projects.city', 'projects.province',
                        'c.uuid as chatUUID',
                        'sci.logoURL as sci_logoURL'
                    )
                    ->groupBy('b.id');


        $bidsOut = Project
                    ::join('bids as b', 'projects.id', '=', 'b.projectID')
                    ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                    ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                    ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                    ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
                    ->join('company_info as bci', 'bbo.builderID', '=', 'bci.userID')
                    ->select(
                        'b.*',
                        'bbo.bidID as bidID', 'bbo.id as bboID', 'bbo.scViewed as bboSCViewed',
                        'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at', 'bbo_accepted',
                        'bbo.acceptedSCBOID as bidAcceptedSCBOID', 'bbo.scViewed as bboSCViewed',
                        'scbo.id as scboID',
                        'scbo.accountingID', 'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                        'scbo.builderAccepted', 'scbo.builder_accepted_at', 'scbo.builder_rejected_at', 'scbo.updated as scbo_updated',
                        'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                        'ci.company', 'ci.phoneNumber',
                        'projects.address', 'projects.address2', 'projects.city', 'projects.province',
                        'projects.country', 'projects.postalCode',
                        'bci.companySlug', 'bci.logoURL as bci_logoURL',
                        'ci.logoURL as sci_logoURL'
                    )
                    ->groupBy('b.id');

        if ($projectID != '') {
            // if (AuthHelpers\checkIfSCCanViewProjectByProjectID($projectID) == false) {
            //     $message = "User tried to view Project: {$projectID} as SC";
            //     return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            // }
            $bidsIn     = $bidsIn->where('b.projectID', $projectID);
            $bidsOut    = $bidsOut->where('b.projectID', $projectID);
            if ($type == 'all' || $type == 'new') {
                $date = Carbon::today('America/Vancouver')->subWeek(2);
                $bids = $bidsIn->where('bbo.scID', $user->id)->where('bbo.created_at', '>=', $date)->paginate(50);
                //$bidsOut    = $bidsOut->where('b.scTradeID',$user->id)->where('bbo.created_at', '>=', $date)->get();
            } 
            else if ($type == 'accepted') {
                $bids = $bidsIn->where('bbo.scID', $user->id)->where('b.accepted', 1)->paginate(50);
            }
            else if ($type == 'rejected') {
                $bids = $bidsOut->where('bbo.scID', $user->id)->where('scbo.status', '=', 'rejected')->paginate(50);
            } 
            else if ($type == 'waiting') {
                $bids = $bidsOut->where('bbo.scID', $user->id)->where('scbo.status', '=', 'waiting')->paginate(50);
            } 
            else if ($type == 'all-in') {
                $bids = $bidsIn->where('bbo.scID', $user->id)->paginate(50);
            } 
            else if ($type == 'all-out') {
                $bids = $bidsOut->where('scbo.scID', $user->id)->paginate(50);
            } 
            else {
                $message = "User manually tried accessing function with incorrect Bid Type: {$type} for Project: {$projectID}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }
        } 
        else if ($projectID == '') 
        {
           if ($type == 'all' || $type == 'new') {
            $date = Carbon::today('America/Vancouver')->subWeek(2);
            $bids = $bidsIn->where('bbo.scID', $user->id)->where('bbo.created_at', '>=', $date)->paginate(50);
            //$bidsOut    = $bidsOut->where('b.scTradeID',$user->id)->where('bbo.created_at', '>=', $date)->get();
            }
            else if ($type == 'accepted') {
                $bids = $bidsIn->where('bbo.scID', $user->id)->where('bbo.accepted', 1)->paginate(50);
            } 
            else if ($type == 'rejected') {
                $bids = $bidsOut->where('bbo.scID', $user->id)->where('scbo.status', '=', 'rejected')->paginate(50);
            } 
            else if ($type == 'waiting') {
                $bids = $bidsOut->where('bbo.scID', $user->id)->where('scbo.status', '=', 'waiting')->paginate(50);
            } 
            else if ($type == 'all-in') {
                $bids = $bidsIn->where('bbo.scID', $user->id)->paginate(50);
            } 
            else if ($type == 'all-out') {
                $bids = $bidsOut->where('scbo.scID', $user->id)->paginate(50);
            } 
            else {
                $message = "User manually tried accessing function with incorrect Bid Type: {$type}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }
        }
        else {
            $message = "User manually tried accessing function with incorrect parameters";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        return response()->json(['data' => $bids], 200);

    }


    public function getAllBidsByTradeForSubcontractor($projectID, $scTradeID , $bidType, $userID) {


        $bidsIn   = Project
                    ::join('bids as b', 'projects.id', '=', 'b.projectID')
                    ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                    ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                    ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                    ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
                    ->join('company_info as bci', 'ci.userID', '=', 'bbo.builderID')
                    ->join('chat as c', function($join) use ($userID) {
                            $join->on('bbo.scID', '=', 'c.scID')
                                  ->where('c.scID', $userID);
                    })
                    ->select(
                        'b.*',
                        'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                        'bbo.scViewed as bboSCViewed',
                        'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                        'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                        'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city',
                        'ci.companySlug',
                        'c.uuid as chatUUID',
                        'ci.logoURL as sci_logoURL',
                        'bci.logoURL as bci_logoURL'
                    );


        $bidsOut   = Project
                    ::join('bids as b', 'projects.id', '=', 'b.projectID')
                    ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                    ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                    ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                    ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
                    ->join('company_info as bci', 'ci.userID', '=', 'bbo.builderID')
                    ->join('chat as c', function($join) use ($userID) {
                        $join->on('bbo.scID', '=', 'c.scID')
                            ->where('c.scID', $userID);
                    })
                    ->select(
                        'b.*',
                        'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                        'bbo.scViewed as bboSCViewed',
                        'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                        'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                        'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city',
                        'ci.companySlug',
                        'c.uuid as chatUUID',
                        'ci.logoURL as sci_logoURL',
                        'bci.logoURL as bci_logoURL'
                    );

        if ($bidType == 'default') {
            $sct        = SubcontractorTrades::orderBy('tradeName', 'asc')->first();
            $bidsIn     = $bidsIn->where('b.scTradeID', $sct->id)->where('projects.id', $projectID)->get();
            $bidsOut    = $bidsOut->where('b.scTradeID', $sct->id)->where('projects.id', $projectID)->get();
        } elseif ($bidType == 'all') {
            $sct        = SubcontractorTrades::orderBy('tradeName', 'asc')->first();
            $scTradeID  = ($scTradeID == 0) ? $sct->id : $scTradeID;
            $bidsIn  = $bidsIn->where('b.scTradeID', $scTradeID)->where('bbo.scID', $userID)->get();
            $bidsOut = $bidsOut->where('b.scTradeID', $scTradeID)->where('scbo.scID', $userID)->get();
        }

        else
        {
            $bidsIn     = $bidsIn->where('b.scTradeID', $scTradeID)->where('projects.id', $projectID)->get();
            $bidsOut    = $bidsOut->where('b.scTradeID', $scTradeID)->where('projects.id', $projectID)->get();
        }
        return response()->json(['bidsIn' => $bidsIn, 'bidsOut' => $bidsOut], 200);

    }


    public function getFilesAndProjectDataForBid($bidID, $projectID)
    {
        if (AuthHelpers\checkIfBuilderCanManageOrSCCanViewBuilderBidOutByBidID($bidID) === false)
        {
            return AuthHelpers\returnUnAuthorizedResponse();
        }

        $files          =  Upload
                           ::join('file_uploads as fu', 'uploads.bidID', '=', 'fu.bidID')
                           ->where('uploads.bidID', $bidID)
                           ->where('fu.deleted', 0)
                           ->select('fu.*')
                           ->get();

        $projectInfo    = Project::find($projectID);

        return response()->json(['files' => $files, 'projectInfo' => $projectInfo], 200);
    }

    public function getProjectDataAndBuilderInfo($bidID, $projectID, Request $request)
    {
        $user = AuthHelpers\getAuthorizedUser();

        if ($user->isA('builder')) {
            if (AuthHelpers\checkIfBuilderCanManageProjectByProjectID($projectID) == false) {
                return AuthHelpers\buildUnAuthorizedResponseAndLog("User tried viewing Bid $bidID Project: $projectID", $request);
            }
        }
        else if ($user->isA('subcontractor')) {
            if (AuthHelpers\checkIfBuilderCanManageOrSCCanViewBuilderBidOutByBidID($bidID) == false) {
                return AuthHelpers\buildUnAuthorizedResponseAndLog("User tried viewing Bid $bidID Project: $projectID", $request);
            }
        }


        /*$files   =  Upload
                    ::join('file_uploads as fu', 'uploads.bidID', '=', 'fu.bidID')
                    ->where('uploads.bidID', $bidID)
                    ->where('fu.deleted', 0)
                    ->select('fu.*')
                    ->get();*/

        $projectInfo    = Project
                            ::join('bids as b', 'projects.id', '=', 'b.projectID')
                            ->leftJoin('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                            //->where('b.id', $bidID)
                            //->where('bbo.bidID', $bidID)
                            ->where('projects.id', $projectID)
                            ->select(
                                'projects.*',
                                        'b.additionalDetails as bidAdditionalDetails', 'b.id as bidID',
                                        'bbo.id as bboID'
                                )
                            ->first();

        $builderInfo    = App\CompanyInfo::where('userID', $projectInfo->userID)->first();


        return response()->json(['projectInfo' => $projectInfo, 'builderInfo' => $builderInfo], 200);
    }

    public function getEveryBidByTradeForAll($user, $userType)
    {

        $userID    = $user->id;
        /*$bids    =  Project
                    ::where('projects.id', $projectID)
                    ->join('bids as b', 'projects.id', '=', 'b.projectID')
                    ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                    ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                    ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                    ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
                    ->select(
                        'b.*',
                        'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                        'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                        'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                        'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'
                    );*/


        $bidsIn = Project
                    ::join('bids as b', 'projects.id', '=', 'b.projectID')
                    ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                    ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                    ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                    ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
                    ->select(
                        'b.*'
                    //'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                    // 'scbo.cr     eated_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                    //'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                    //'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'
                    );


        $bidsOut = Project
                    ::join('bids as b', 'projects.id', '=', 'b.projectID')
                    ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                    ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                    ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                    ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
                    ->select(
                        'b.*',
                        'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                        'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                        'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                        'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'
                    );

        $sct = SubcontractorTrades::orderBy('tradeName', 'asc')->first();

        if ($userType == 'subcontractor') {
            $bidsIn = $bidsIn->where('b.scTradeID', $sct->id)->where('projects.userID', $userID)->get();
            $bidsOut = $bidsOut->where('b.scTradeID', $sct->id)->where('projects.userID', $userID)->get();
        }

        return response()->json(['bidsIn' => $bidsIn, 'bidsOut' => $bidsOut], 200);
    }


    public function SCBidOutValidator(array $data)
    {


        $rules = [
            'lineData.*.price'              => 'required|numeric',
            'lineData.*.quantity'           => 'required|numeric',
            'lineData.*.description'        => 'nullable|string|max:255',
            'lineData.*.name'               => 'required|max:255',
            'lineData.*.uom'                => 'nullable|max:255',
            'lineData.*.lineTaxTotal'       => 'required|max:255',
            'lineData.*.lineSubTotal'       => 'required|max:255',
            'lineData.*.lineTotal'          => 'required|max:255',
            'lineData.*.taxRate'            => 'nullable|max:255',
            'grandTotal'                    => 'required|max:255',
            'taxTotal'                      => 'required|max:255',
            'subTotal'                      => 'required|max:255',
            'comments'                      => 'nullable|max:500',
        ];

        $messages = [
            'lineData.*.price.required' =>        'The price must be filled out for each item',
            'lineData.*.price.numeric' =>         'The price must be numeric',
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function findOrCreateSCBidOut($bidID, $subTotal, $taxTotal, $grandTotal, $comments)
    {
        $userID                 = AuthHelpers\getAuthorizedUserID();
        $bidOut                 = SCBidsOut::where('bidID', $bidID)->first();
        $bidOut->subTotal       = $subTotal;
        $bidOut->taxTotal       = $taxTotal;
        $bidOut->grandTotal     = $grandTotal;
        $bidOut->comments       = $comments;
        $bid                    = BuilderBidsOut::where('bidID',$bidID)->where('scID', $userID)->first();
        $bid->scResponded       = 1;
        $bid->scViewed          = 1;
        $bid->scViewedAt        = GeneralHelpers\getDateTime();
        if ( ! $bidOut) {
            $bidOut             = new SCBidsOut();
            $bidOut->builderID  = $bid->builderID;
            $bidOut->bidID      = $bidID;
            $bidOut->scID       = $userID;
            /*$bidOut->subTotal   = $subTotal;
            $bidOut->taxTotal   = $taxTotal;
            $bidOut->grandTotal = $grandTotal;
            $bidOut->comments   = $comments;*/

            //$bidOut->save();
        }
        else
        {

            /*$bidOut->subTotal           = $subTotal;
            $bidOut->taxTotal           = $taxTotal;
            $bidOut->grandTotal         = $grandTotal;
            $bidOut->comments           = $comments;*/

            $bidOut->updated            = 1;
            $bidOut->builderViewed      = 0;
            $bidOut->updated_at         = GeneralHelpers\getDateTime();
            SCBidsOutLineItems::where('scBidOutID', $bidOut->id)->delete();
            //$bidOut->save();
        }
        try
        {
            $bid->save();
            $bidOut->save();
            AuthHelpers\giveSCBidOutPermissionsToBuilderAndSC($bidOut);

        }
        catch (Exception $ex)
        {
            return DBHelpers\returnDBError($ex);

        }
        return $bidOut;
    }

    // send scbo to builder
    public function SCBidOut($projectID, $bidID, Request $request)
    {


        $data           = $request->input('data');
       // $lineData   = $request->input('data.lineData.*');

        $validate   = $this->SCBidOutValidator($data);

        if ($validate->fails()) { return DBHelpers\returnValidationError($validate); }
        //$bid                = Bid::find($bidID);
        //$bidOut             = $this->findOrCreateSCBidOut($bidID, $data['subTotal'], $data['taxTotal'], $data['grandTotal'], $data['comments']);
        $userID                 = AuthHelpers\getAuthorizedUserID();
        $bidOut                 = SCBidsOut::where('bidID', $bidID)->first();
        $newSCBO                = null;
        
        //TODO: DO BID CALCULATION VERIFICATION ON BACK END

        $bid                    = BuilderBidsOut::where('bidID',$bidID)->where('scID', $userID)->first();
        $bid->scResponded       = 1;
        $bid->scViewed          = 1;
        //$bid->scViewedAt        = GeneralHelpers\getDateTime();
        if ( ! $bidOut) {
            $bidOut                 = new SCBidsOut;
            $bidOut->builderID      = $bid->builderID;
            $bidOut->bidID          = $bidID;
            $bidOut->scID           = $userID;
            $bidOut->subTotal       = $data['subTotal'];
            $bidOut->taxTotal       = $data['taxTotal'];
            $bidOut->grandTotal     = $data['grandTotal'];
            $bidOut->comments       = $data['comments'] ?? '';
            $bidOut->projectID      = $projectID;
            $newSCBO                = true;
            //$bidOut->save();
        }
        else
        {
            $bbo                    = BuilderBidsOut::where('acceptedSCBOID', $bidOut->id)->where('scID', $userID)->first();

            if ($bbo) {
                $bbo->acceptedSCBOID        = null;
                $bbo->bbo_accepted          = null;
                $bbo->accepted              = null;
                $newSCBO                    = false;
                try
                {
                    $bbo->save();
                }
                catch (Exception $ex)
                {
                    return DBHelpers\returnDBError($ex);
                }
            }

            $bidOut->subTotal               = $data['subTotal'];
            $bidOut->taxTotal               = $data['taxTotal'];
            $bidOut->grandTotal             = $data['grandTotal'];
            $bidOut->comments               = $data['comments'] ?? '';

            $bidOut->updated                = 1;
            $bidOut->builderViewed          = 0;
            $bidOut->builderViewedAt        = null;
            $bidOut->updated_at             = GeneralHelpers\getDateTime();
            $bidOut->builder_accepted_at    = null;
            $bidOut->builder_rejected_at    = null;
            $bidOut->builderAccepted        = null;
            $bidOut->status                 = 'waiting';
            SCBidsOutLineItems::where('scBidOutID', $bidOut->id)->delete();
            //$bidOut->save();
        }


        try
        {
            $bid->save();
            $bidOut->save();
            if ($newSCBO == false) {
                // TODO: CHANGE TO UPDATED SCBO INSTEAD OF NEW
                $when = Carbon::now()->addSeconds(20);
                $builder = User::find($bidOut->builderID);
                dispatch(((new App\Jobs\SendNewSCBOEmail($bidOut))
                    ->onQueue('emails')
                    ->delay($when->addSeconds(5))));
                $builder->notify((new App\Notifications\NewSCBONotification($bidOut)));
            }
            else if ($newSCBO == true) {
                $when = Carbon::now()->addSeconds(20);
                $builder = User::find($bidOut->builderID);
                dispatch(((new App\Jobs\SendNewSCBOEmail($bidOut))
                            ->onQueue('queue2')
                            ->delay($when)));
                $builder->notify((new App\Notifications\NewSCBONotification($bidOut))->delay($when->addSeconds(5)));

            }
            AuthHelpers\giveSCBidOutPermissionsToBuilderAndSC($bidOut);
        }
        catch (Exception $ex)
        {
            return DBHelpers\returnDBError($ex);

        }
        $boLIT              = new SCBidsOutLineItems();
        $lineItemColumns    = $boLIT->getFillable();

        /*
        $bidOut->bidID      = $bidID;
        $bidOut->scID       = AuthHelpers\getAuthorizedUserID();
        $bidOut->subTotal   = $data['subTotal'];
        $bidOut->taxTotal   = $data['taxTotal'];
        $bidOut->grandTotal = $data['grandTotal'];
        try
        {
            $bidOut->save();
        }
        catch (Exception $ex)
        {
            return DBHelpers\returnDBError($ex);
        }
        */

        foreach ($data['lineData'] as $arr) {
            $boLI = new SCBidsOutLineItems();

            foreach ($lineItemColumns as $column) {
                if (array_key_exists($column, $arr)
                    && $column !== 'token') {
                    $boLI->$column      = $arr[$column];
                    /*try
                    {
                        $boLI->bidID = $bidID;
                        $boLI->scBidOutID = $bidOut->id;
                        $boLI->save();
                        return response(['message' => 'Your bid has been sent out'], 200);
                    }
                    catch (Exception $ex)
                    {
                        return DBHelpers\returnDBError($ex);
                    }*/
                }
            }
            $boLI->bidID        = $bidID;
            $boLI->scBidOutID   = $bidOut->id;
            try
            {
                $boLI->save();
            }
            catch (Exception $ex)
            {
                return DBHelpers\returnDBError($ex);
            }
        }
        return response(['message' => 'Your bid has been sent out'], 200);
    }

    public function getSCBidOut($projectID, $bidID, Request $request)
    {

        $data           = BuilderBidsOut
                            ::join('bids as b', 'builder_bids_out.bidID', '=', 'b.id')
                            ->join('projects as p', 'b.projectID', '=', 'p.id')
                            ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                            ->leftJoin('sc_bids_out_line_items as scboli', 'b.id', '=', 'scboli.bidID')
                            //->leftJoin('accounting as a', 'scbo.accountingID', '=', 'a.id')
                            ->join('company_info as ci', 'builder_bids_out.scID', '=', 'ci.userID')
                            ->where('builder_bids_out.bidID', $bidID)
                            ->select(
                                'scboli.*',
                                'scbo.id as scboID', 'scbo.bidID as scbo_bidID',
                                'scbo.created_at as scbo_created_at',
                                'scbo.updated_at as scbo_updated_at',
                                'scbo.builderViewed as builder_viewed_scbo',
                                'scbo.builderViewedAt as builder_viewed_at_scbo',
                                'scbo.grandTotal', 'scbo.subTotal',
                                'scbo.taxTotal', 'scbo.comments',
                                'scbo.accountingID as accountingID',
                                'scbo.builderAccepted', 'scbo.builder_accepted_at', 'scbo.builder_rejected_at',
                                'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city',
                                'b.additionalDetails as bidAdditionalDetails',
                                'builder_bids_out.scResponded as sc_responded_bbo',
                                'builder_bids_out.scViewed as sc_viewed_bbo',
                                'builder_bids_out.scViewedAt as sc_viewed_bbo_at',
                                'p.id as projectID',
                                'b.id as bidID',
                                'builder_bids_out.id as bboID'
                            )
                            ->get();


        /*return $data;


        $data           = SCBidsOut
                          ::where('bbo.bidID', $bidID)
                          ->rightJoin('bids as b', 'sc_bids_out.bidID', '=', 'b.id')
                          ->rightJoin('builder_bids_out as bbo', 'b.id', '=', 'sc_bids_out.bidID' )
                          ->join('projects as p', 'b.projectID', '=', 'p.id')
                          ->leftJoin('sc_bids_out_line_items as scboli', 'sc_bids_out.id', '=', 'scboli.scBidOutID')
                          ->join('company_info as ci', 'sc_bids_out.scID', '=', 'ci.userID')
                          ->select(
                              'scboli.*',
                              'sc_bids_out.id as scboID', 'sc_bids_out.bidID as scbo_bidID',
                              'sc_bids_out.created_at as scbo_created_at',
                              'sc_bids_out.updated_at as scbo_updated_at',
                              'sc_bids_out.grandTotal', 'sc_bids_out.subTotal',
                              'sc_bids_out.taxTotal', 'sc_bids_out.comments',
                              'sc_bids_out.builderAccepted', 'sc_bids_out.builder_accepted_at', 'sc_bids_out.builder_rejected_at',
                              'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'
                          )
                          ->get();*/



        $user = AuthHelpers\getAuthorizedUser();

        if ($user->isA('builder')) {
            if (AuthHelpers\checkIfBuilderCanManageProjectByProjectID($projectID) == true) {
                return response()->json([
                    'data' => $data,
                    'viewing' => true
                ], 200);
            } else {
                $message = "Builder tried to view SCBO with BBOID: $bidID";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }
        }
        else if ($user->isA('subcontractor')) {
           if (AuthHelpers\checkIfSCCanViewBuilderBidOutByBidID($bidID) == true) {
            return response()->json([
                'data' => $data,
                'editing' => ($data->count()) ? true : false,
            ], 200);
            }
            else {
               $message = "SC tried to view SCBO with BBOID: $bidID";
               return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }
        }
        else
        {
            return AuthHelpers\returnUnAuthorizedResponseWithDebug('getSCBidOut(). SC/Builder doesnt have permission to manage/view bid');
        }

    }

    public function acceptBid($bidID, Request $request)
    {
        if (AuthHelpers\checkBidOwnershipByBidID($bidID) == false) {
            $message = "User tried to accept bid {$bidID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        $bid        = Bid::find($bidID);
        $scbo       = SCBidsOut::where('bidID', $bidID)->first();
        $bbo        = BuilderBidsOut::where('bidID', $bidID)->where('scID', $scbo->scID)->first();

        if ($bbo->accepted != null ) {
            $status    = $bbo->accepted == 1 ? 'accepted' : 'rejected';
            $message = "ERROR: The bid has already been {$status}. You cannot change the status of the bid";
            return AuthHelpers\buildResponseWithMessageAndLog($message, $request);
        }

        $now        = GeneralHelpers\getDateTime();

        $bbo->accepted              = 1;
        $bbo->scID                  = $scbo->scID;
        $bbo->acceptedSCBOID        = $scbo->id;
        $scbo->status               = 'accepted';
        $scbo->sc_read              = 0;
        $scbo->builderAccepted      = 1;
        $scbo->builder_accepted_at  = $now;
        $scbo->builderID            = $bid->builderID;
        $scbo->projectID            = $bid->projectID;
        $scbo->builder_rejected_at  = $now;
        $bbo->bbo_accepted          = 1;
        $accounting                 =  Accounting::where('projectID', $bid->projectID)->where('scboID', $scbo->id)->first();
        if ( ! $accounting) {
            $accounting                     = new Accounting();
            $accounting->deleted            = 0;
            $accounting->builderID          = $bid->builderID;
            $accounting->scID               = $scbo->scID;
            $accounting->projectID          = $bid->projectID;
            $accounting->bidID              = $bid->id;
            $accounting->scboID             = $scbo->id;
            $accounting->totalAmount        = $scbo->grandTotal;
            $accounting->amountRemaining    = $scbo->grandTotal;
            $accounting->amountPaidSoFar    = 0.00;
            $accounting->scTradeID          = $bid->scTradeID;
            $accounting->bboID              = $bbo->id;
        }
        else {
            $accounting->deleted = 0;
        }

        try
        {
            $bid->save();
            $bbo->save();
            $accounting->save();
            $scbo->accountingID = $accounting->id;
            $scbo->save();
            AuthHelpers\giveSCProjectViewPermissionByProjectID($bid->projectID, $scbo->scID);
            AuthHelpers\giveAccountingOwnershipToBuilderAndSCByID($accounting->id, $accounting->builderID, $accounting->scID);

            $when   = Carbon::now()->addSeconds(15);
            $sc     = User::find($bbo->scID);


            dispatch(((new App\Jobs\SendBuilderHasNewinvoiceEmailJob($bid, $bbo, $scbo, $accounting))
                    ->onQueue('emails')
                    ->delay($when)));

            dispatch(((new App\Jobs\BuilderHasChangedSCBOStatus($bid, $bbo, $scbo, $accounting))
                    ->onQueue('queue2')
                    ->delay($when->addSeconds(5))));

            $sc
                ->notify((new App\Notifications\BuilderAcceptedBidNotification($bid, $scbo, $bbo))
                ->onQueue('queue')
                ->delay($when->addSeconds(10)));
        }

        catch (Exception $ex)
        {
            return DBHelpers\returnDBError($ex);
        }
        return response()->json(['message' => 'The bid has been accepted'], 200);

    }

    public function rejectBid($bidID, Request $request)
    {
        if (AuthHelpers\checkBidOwnershipByBidID($bidID) == false) {
            $message = "User tried to reject bid {$bidID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }
        $bid        = Bid::find($bidID);
        $scbo       = SCBidsOut::where('bidID', $bidID)->first();
        $bbo        = BuilderBidsOut::where('bidID', $bidID)->first();
        if ($bbo->accepted != null ) {
            $status    = $bbo->accepted == 1 ? 'accepted' : 'rejected';
            $message = "ERROR: The bid has already been {$status}. You cannot change the status of the bid";
            return AuthHelpers\buildResponseWithMessageAndLog($message, $request);
        }
        $now        = GeneralHelpers\getDateTime();

        $bbo->accepted              = 0;
        $bbo->scID                  = null;
        $bbo->acceptedSCBOID        = null;
        $scbo->status               = 'rejected';
        $scbo->sc_read              = 0;
        $scbo->builderAccepted      = 0;
        $scbo->builder_accepted_at  = null;
        $scbo->builder_rejected_at  = $now;
        //$scbo->builderID            = null;
        $bbo->bbo_accepted          = 0;
        $accounting                 =  Accounting::where('projectID', $bid->projectID)->where('scboID', $scbo->id)->first();
        $accounting->deleted        = 1;
        $accounting->deleted_at     = $now;
        try
        {
           // AuthHelpers\revokeSCProjectViewPermissionByProjectID($bid->projectID);

            //$bid->save();
            $scbo->save();
            $bbo->save();
            $accounting->save();

            $when = Carbon::now()->addSeconds(20);

            dispatch(((new App\Jobs\BuilderHasChangedSCBOStatus($bid, $bbo, $scbo, $accounting))
                            ->onQueue('emails')
                            ->delay($when)));


            $sc         = User::find($scbo->scID);
            $sc->notify((new App\Notifications\BuilderRejectedBidNotification($bid, $scbo, $bbo))
                        ->onQueue('queue2')
                        ->delay($when->addSeconds(5)));
        }

        catch (Exception $ex)
        {
            return DBHelpers\returnDBError($ex);
        }

        return response()->json(['message' => 'The bid has been rejected'], 200);

    }

    /*public function getBidForProjectForSC($projectID, Request $request)
    {

        if (AuthHelpers\checkIfSCCanViewProjectByProjectID($projectID) == false)
        {
            $message = 'User tried viewing project with id '.$projectID;
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        $bidsIn   = Project
            ::join('bids as b', 'projects.id', '=', 'b.projectID')
            ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
            ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
            ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
            ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
            ->select(
                'b.*',
                'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'
            );


        $bidsOut   = Project
            ::join('bids as b', 'projects.id', '=', 'b.projectID')
            ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
            ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
            ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
            ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
            ->select(
                'b.*',
                'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'
            );
    }*/

    public function getBBOAndInformation($bidID, $bboID, Request $request)
    {

        $user       = AuthHelpers\getAuthorizedUser();

        $bbo      = Project
                        ::join('bids as b', 'projects.id', '=', 'b.projectID')
                        ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                        ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                        //->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                        ->join('company_info as bci', 'bci.userID', '=', 'b.builderID')
                        ->join('company_info as sci', 'sci.userID', '=', 'bbo.scID')
                        ->where('b.id', $bidID)
                        ->where('bbo.id', $bboID)
                        ->select(
                            'b.*', 'b.additionalDetails as bid_additionalDetails',
                            'bbo.id as bboID', 'bbo_accepted',
                            'bbo.scViewed as bbo_viewed_by_sc',
                            'bbo.scViewedAt as bbo_viewed_by_sc_at',
                            'bbo.created_at as bbo_created_at', 'bbo.updated_at as bbo_updated_at',
                            'scbo.id as scboID', 'scbo.builderAccepted as scbo_builderAccepted',
                            'scbo.builder_accepted_at as scbo_builderAcceptedAt',
                            'scbo.accountingID as accountingID',
                            'scbo.builder_rejected_at as scbo_builderRejectedAt',
                            'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                            'scbo.builderViewed as builderViewedSCBO',
                            'projects.address as projectAddress', 'projects.address2 as projectAddress2',
                            'projects.city as projectCity', 'projects.postalCode as projectPostalCode',
                            'projects.province as projectProvince', 'projects.details as projectDetails',
                            'projects.landType as projectLandType', 'projects.projectType as projectProjectType',
                            'projects.numUnits as projects.projectNumUnits', 'projects.lotSize as projectLotSize',
                            'projects.buildingSize as projectBuildingSize', 'projects.zoning as projectZoning',
                            'projects.buildValue as projectBuildValue',
                            'bci.company as bci_company', 'bci.address as bci_address', 'bci.address2 as bci_address2',
                            'bci.phoneNumber as bci_phoneNumber', 'bci.postalCode as bci_postalCode', 'bci.city as bci_city',
                            'bci.logoURL as bci_logoURL',
                            'sci.company as sci_company', 'sci.address as sci_address', 'sci.address2 as sci_address2',
                            'sci.phoneNumber as sci_phoneNumber', 'sci.postalCode as sci_postalCode', 'sci.city as sci_city',
                            'sci.logoURL as sci_logoURL'
                        )
                        ->get();

        $files =    Upload
                    ::join('file_uploads as fu', 'uploads.bidID', '=', 'fu.bidID')
                    ->where('uploads.bidID', $bidID)
                    ->where('fu.deleted', 0)
                    ->select('fu.*')
                    ->get();

        if ($user->isA('builder')) {
            if (AuthHelpers\checkIfBuilderCanManageOrSCCanViewBuilderBidOutByBidID($bidID) == false) {
                $message = "Builder tried to view BBO for BidID {$bidID}  BBO ID: {$bboID}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }

        }
        elseif ($user->isA('subcontractor')) {
            if (AuthHelpers\checkIfSCCanViewBuilderBidOutByBBOID($bboID) == false) {
                $message = "SC tried to view BBO for BidID {$bidID}  BBO ID: {$bboID}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }

        }

        return response()->json(['bbo' => $bbo, 'uploads' => $files], 200);
    }

    public function checkIfBidFormExistsForTrade($projectID, $sctID, Request $request) {

        if (AuthHelpers\checkIfBuilderCanManageProjectByProjectID($projectID) == false) {
            $message = "User tried to view Bid Form for ProjectiD {$projectID}  SC Trade {$sctID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }
        $bid        = Bid::where('projectID', $projectID)->where('scTradeID', $sctID);

        $bidID      = '';
        $exists     = false;
        if ($bid->count()) {
            $exists = true;
            $bidID = $bid->first()->id;
        }
        return response()->json(['data' => $exists, 'bidID' => $bidID], 200);

    }


    public function getBBOInfo($projectID, $type, $sctID, Request $request)
    {

        if (AuthHelpers\checkIfBuilderCanManageProjectByProjectID($projectID) == false) {
            $message = "User tried to get BBO details to create new Bid for ProjectID: {$projectID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        $sctInfo    = SubcontractorTrades::find($sctID);


        if ($type == 'create') {

            $details = Project
                ::join('company_info as bci',       'bci.userID', '=', 'projects.userID')
                ->where('projects.id', $projectID)
                ->select(
                    'projects.address as projectAddress', 'projects.address2 as projectAddress2',
                    'projects.city as projectCity', 'projects.postalCode as projectPostalCode',
                    'projects.province as projectProvince', 'projects.details as projectDetails',
                    'projects.landType as projectLandType', 'projects.projectType as projectProjectType',
                    'projects.numUnits as projects.projectNumUnits', 'projects.lotSize as projectLotSize',
                    'projects.buildingSize as projectBuildingSize', 'projects.zoning as projectZoning',
                    'projects.buildValue as projectBuildValue',
                    'bci.company as bci_company', 'bci.address as bci_address', 'bci.address2 as bci_address2',
                    'bci.phoneNumber as bci_phoneNumber', 'bci.postalCode as bci_postalCode', 'bci.city as bci_city'
                )
                ->get();


             return response()->json(['projectInfo' => $details, 'sctInfo' => $sctInfo], 200);
        }
        else if ($type == 'view') {

            $details = Project
                        ::join('company_info as bci',       'bci.userID', '=', 'projects.userID')
                        ->join('bids as b', 'projects.id', '=', 'b.projectID')
                        ->leftJoin('builder_bids_out as bbo', 'b.id', '=','bbo.id' )
                        ->where('projects.id', $projectID)
                        ->where('b.scTradeID', $sctID)
                        ->select(
                            'b.*', 'b.additionalDetails as bid_additionalDetails',
                            'b.id as bidID',
                            'bbo.id as bboID', 'bbo_accepted',
                            'bbo.created_at as bbo_created_at', 'bbo.updated_at as bbo_updated_at',
                            'projects.address as projectAddress', 'projects.address2 as projectAddress2',
                            'projects.city as projectCity', 'projects.postalCode as projectPostalCode',
                            'projects.province as projectProvince', 'projects.details as projectDetails',
                            'projects.landType as projectLandType', 'projects.projectType as projectProjectType',
                            'projects.numUnits as projects.projectNumUnits', 'projects.lotSize as projectLotSize',
                            'projects.buildingSize as projectBuildingSize', 'projects.zoning as projectZoning',
                            'projects.buildValue as projectBuildValue',
                            'bci.company as bci_company', 'bci.address as bci_address', 'bci.address2 as bci_address2',
                            'bci.phoneNumber as bci_phoneNumber', 'bci.postalCode as bci_postalCode', 'bci.city as bci_city'
                        )
                       ->get();

            $files  =  Upload
                        ::join('file_uploads as fu', 'uploads.bidID', '=', 'fu.bidID')
                        ->where('uploads.bidID', $details[0]->bidID)
                        ->where('fu.deleted', 0)
                        ->select('fu.*')
                        ->get();

                return response()->json(['projectInfo' => $details, 'sctInfo' => $sctInfo, 'files' => $files], 200);

        }
        else
        {
            $message = "User tried to get BBO details to create new Bid for ProjectID: {$projectID} with illegal parameter Type: ${type}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

    }



//******* Vikash Rathore ********//    
public function v2_getAllBidsForSubcontractorWithCount() 
    {
                $user = AuthHelpers\getAuthorizedUser();       


                $q['accepted'] = Bid::where('subcontractorID', $user->id)
                                ->where('status', 'accepted')               
                                ->count(); 
            
                $q['renegotiated'] = Bid::where('subcontractorID', $user->id)
                                  ->where('status', 'renegotiated')
                                  ->count();               
           
               $q['is_bid_out'] = Bid::where('subcontractorID', $user->id)
                                  ->where('is_bid_out', 1)
                                  ->count();
            
                $q['not_bid_out'] = Bid::where('subcontractorID', $user->id)
                                  ->where('is_bid_out', 0)
                                  ->count();                        
      

        return response()->json($q);
    }


    
    

}
