<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use App\Helpers;
use Illuminate\Notifications\Notifiabe;
use Illuminate\Support\Collection;

class NotificationsController extends Controller
{

    public function getAll()
    {
        $user = Helpers\AuthHelpers\getAuthorizedUser();

        $nots = $user->unreadNotifications->all();

        $notsCollect = new Collection();
        if ($user->isA('builder')) {
            $projects = App\Project::where('userID', $user->id)->get();
            if ($projects->count() && count($nots)) {
                foreach ($projects as $proj) {
                    foreach ($nots as $not) {
                        if ($proj->address == $not->data['projectAddress']) {
                            $notsCollect->push($not);
                        }
                        else {
                            $data = array('projectAddress' => $proj->address ,'projectAddress2' => $proj->address2);
                            $values = ([
                                'type' => 'NoNotifications',
                                'data' => $data
                            ]);
                            $notsCollect->push($values);
                        }
                    }
                }
            }
            // projects exist but no nots
            else if ($projects->count()) {
                foreach ($projects as $proj) {
                    $data = array('projectAddress' => $proj->address ,'projectAddress2' => $proj->address2);
                    $values = ([
                        'type' => 'NoNotifications',
                        'data' => $data
                    ]);
                    $notsCollect->push($values);
                }
            }

            //No projects for builder
            else {
                $data = array();
                $values = ([
                    'type' => 'NoNotificationsAtAll',
                    'data' => $data
                ]);
                $notsCollect->push($values);
            }
        }

        if ($user->isA('subcontractor')) {
            $projects = App\Project
                        ::join('builder_bids_out as bbo', 'projects.id', '=', 'bbo.projectID')
                        ->where('bbo.scID', $user->id)
                        ->where('bbo.bbo_accepted', 1)
                        ->get();

            if ($projects->count() && count($nots)) {
                foreach ($projects as $proj) {
                    foreach ($nots as $not) {
                        if ($proj->address == $not->data['projectAddress']) {
                            $notsCollect->push($not);
                        }
                        else {
                            $data = array('projectAddress' => $proj->address ,'projectAddress2' => $proj->address2);
                            $values = ([
                                'type' => 'NoNotifications',
                                'data' => $data
                            ]);
                            $notsCollect->push($values);
                        }
                    }
                }
            }
            // SC has nots but no accepted scbos
            else if ($projects->count() && count($nots) <= 0) {
                foreach ($projects as $proj) {
                    $data = array('projectAddress' => $proj->address ,'projectAddress2' => $proj->address2);
                    $values = ([
                        'type' => 'NoNotifications',
                        'data' => $data
                    ]);
                    $notsCollect->push($values);
                }
            }
            // projects but no nots
            else if (count($nots) > 0 && !$projects->count()) {
                return response()->json(['nots'=> $nots = $user->unreadNotifications->groupBy('data.projectAddress')], 200);
            }
            //No projects for sc
            else {
                $values = ([
                    'type' => 'NoNotificationsAtAll',
                ]);
                $notsCollect->push($values);
            }
        }

        // Note: in the future grouping by data.projectAddress & created_at may cause problems
         return response()->json(['nots'=> $notsCollect->groupBy('data.projectAddress')], 200);

    }

    public function getAllNotificationsForProject($projectID, Request $request)
    {
        $user = Helpers\AuthHelpers\getAuthorizedUser();

        $nots = $user->unreadNotifications->where('data.projectID', $projectID)->groupBy('data.projectAddress');


        return response()->json(['nots'=> $nots], 200);

    }
}
