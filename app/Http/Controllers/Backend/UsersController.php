<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\User\UserUpdate;
use App\Http\Requests\User\UserCreate;
use App\Http\Requests\User\CompanyUpdate;

use App\User;
use App\CompanyInfo;
use App\Models\Subcontractors\SubcontractorTrades;
use App\Models\Subcontractors\Subcontractors;
use App\Models\Builder\Builders;
use App\Models\CompanyInfo\Profile;
use App\Models\CompanyInfo\ProfileUploads;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Webpatser\Uuid\Uuid;
use Bouncer;
use App\Helpers\GeneralHelpers;
use App\Helpers\DBHelpers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\Events\Registered;
use Prologue\Alerts\Facades\Alert;

class UsersController extends Controller
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var CompanyInfo
     */
    private $company_info;

    /**
     * @var SubcontractorTrades
     */
    private $subcontractorTrades;

    /**
     * @var Subcontractors
     */
    private $subcontractors;

    /**
     * @var Builders
     */
    private $builders;

    /**
     * UsersController constructor.
     *
     * @param User $user
     * @param SubcontractorTrades $subcontractorTrades
     * @param CompanyInfo $company_info
     * @param Subcontractors $subcontractors
     * @param Builders $builders
     */
    public function __construct(User $user, SubcontractorTrades $subcontractorTrades, CompanyInfo $company_info, Subcontractors $subcontractors, Builders $builders)
    {
        $this->user = $user;
        $this->subcontractorTrades = $subcontractorTrades;
        $this->company_info = $company_info;
        $this->subcontractors = $subcontractors;
        $this->builders = $builders;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = $this->user->with(['company_info', 'trade_info', 'trade_info.subcontractor_trades'])->get();
        
        return view('dashboard.users.index', [
            'users' => $users
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        return view('dashboard.users.edit', [ 
            'id' => $id
        ]);
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dashboard.users.create', [ 
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy(int $id)
    {
        $this->user->findOrFail($id)->delete();
        Alert::success('User successfully deleted')->flash();
        return redirect()->route('users.index')->with('alerts', Alert::all());
    }

// ------------------API-----------------//
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getUser(Request $request)
    {
        $id = $request->get('id');
        $user = $this->user->with(['company_info', 'trade_info'])->findOrFail($id);

        return response()->json(['user' => $user], 200);
    }

    /**
     * @param UserUpdate $request
     *
     * @return JsonResponse
     */
    public function saveUser(UserUpdate $request)
    {
        $id = $request->get('id');
        $user = $this->user->findOrFail($id);

        $user->update($request->validatedOnly());
        if ($user->userType === 'subcontractor') {
            try {
                $subcontractor = $this->subcontractors->findOrFail($id);
                $subcontractor->update($request->get('trade_info'));
            }
            catch(ModelNotFoundException $e){}
        }
        $user = $this->user->with(['company_info', 'trade_info'])->findOrFail($id);
        return response()->json(['message' => 'User successfully updated', 'user' => $user]);
    }

    /**
     * @param CompanyUpdate $request
     *
     * @return JsonResponse
     */
    public function saveCompany(CompanyUpdate $request)
    {
        $id = $request->get('userID');
        $company_info = $this->company_info->findOrFail($id);
        $validatedReq = $request->validatedOnly();
        $validatedReq['companySlug'] = str_slug(str_replace('&', 'and', $validatedReq['company']));
        $company_info->update($validatedReq);        
        return response()->json(['message' => 'Company successfully updated', 'company_info' => $company_info]);
    }

    /**
     * @param UserCreate $request
     *
     * @return JsonResponse
     */
    public function createUser(UserCreate $request)
    {
        $user = new User;
        $sc = new Subcontractors();
        $builder = new Builders();
        $company = new CompanyInfo;

        $validatedReq = $request->validatedOnly();

        $userInput = $request->only($user->getFillable());
        $userColumns = $user->getFillable();

        $companyInput = $request->get('company_info');
        $companyColumns = $company->getFillable();

        $userInput['password'] = bcrypt($userInput['password']);
        $userType = $request->input('userType');
        
        $user->is_terms_accepted  = $request->input('is_terms_accepted');
        $user->is_tutorial_viewed  = $request->input('is_tutorial_viewed');
        $user->associations = $request->input('associations');
        $user->certifications = $request->input('certifications');
        $user->references  = $request->input('references');
        


        foreach ($userColumns as $column) {
            if (array_key_exists($column, $userInput) && !empty($userInput[$column])
                && $column !== 'isSubcontractor'
                && $column !== 'isBuilder'
                && $column !== 'isAdmin'
                && $column !== 'userType'
                && $column !== 'trade'
                && $column !== 'password_confirmation'
                && $column !== 'token') {
                $user->$column = $userInput[$column];
            }
        }
        foreach ($companyColumns as $column2) {
            if (array_key_exists($column2, $companyInput)
                && !empty($companyInput[$column2])
                && $column2 !== 'isAdmin'
                && $column2 !== 'isBuilder'
                && $column2 !== 'userType'
                && $column2 !== 'trade'
                && $column2 !== 'isSubcontractor'
                && $column2 !== 'password_confirmation'
                && $column2 !== 'token') {
                $company->$column2 = $companyInput[$column2];
            }
        }

        try {
            $user->token = Password::getRepository()->createNewToken();
            $company->companySlug = str_slug(str_replace('&', 'and', $companyInput['company']));
            $company->uuid = GeneralHelpers\generateUniqueString("company_info", "uuid", 16);
            $company->logoURL = 'assets/images/no-image-found.png';
            if ($userType === 'subcontractor') {
                $trade = $validatedReq['trade_info']['scTradeID'];
                $user->isSubContractor = 1;
                $user->userType = 'subcontractor';
                $sc->scTradeID = $trade;
                $user->save();
                $sc->userID = $user->id;
                $sc->max_project_size = $validatedReq['trade_info']['max_project_size'];
                $sc->min_employee = $validatedReq['trade_info']['min_employee'];
                $sc->max_employee = $validatedReq['trade_info']['max_employee'];
                $sc->rating = $validatedReq['trade_info']['rating'];
                $sc->save();
                $user->assign('subcontractor');
            } else if ($userType === 'builder') {
                $user->isBuilder = 1;
                $user->userType = 'builder';
                $user->save();
                $user->assign('builder');
                $builder->userID = $user->id;
                $builder->save();
            } else {
                return response()->json(['error' => 'You are doing something you are unauthorized to do'], 401);
            }
        } catch (Exception $exception) {
            if ($user) {
                $user->delete();
            }
            if ($sc) {
                $sc->delete();
            }
            if ($builder) {
                $builder->delete();
            }
            return DBHelpers\returnDBError($exception);
        }
        try {
            $company->userID = $user->id;
            $count = CompanyInfo::where('companySlug', 'LIKE', '%' . $company->companySlug . '%')->count();
            if ($count > 0) {
                ++$count;
                $company->companySlug = $company->companySlug . $count;
            }
            $company->save();

            $profile = new Profile();
            $profile->userID = $user->id;
            $profile->save();

            for ($i = 0; $i < 8; $i++) {
                $profUploads = new ProfileUploads();
                $profUploads->userID = $user->id;
                $profUploads->profileID = $user->id;
                $profUploads->isLogo = 0;
                $profUploads->fileID = Uuid::generate();
                $profUploads->save();
            }
            $profUploads = new ProfileUploads();
            $profUploads->userID = $user->id;
            $profUploads->profileID = $user->id;
            $profUploads->isLogo = 1;
            $profUploads->fileURL = "assets/images/no-image-found.png";
            $profUploads->fileID = Uuid::generate();
            $profUploads->save();

            $profUploads = new ProfileUploads();
            $profUploads->userID = $user->id;
            $profUploads->profileID = $user->id;
            $profUploads->isLogo = 2;
            $profUploads->fileID = Uuid::generate();
            $profUploads->save();

            Bouncer::allow($user)->toManage($profile);

            event(new Registered($user));            
        } catch (Exception $exception) {
            if ($sc && $sc->id != 0) {
                $sc->delete();
            }
            if ($builder && $builder->id != 0) {
                $builder->delete();
            }
            if ($user && $user->id != 0) {
                $user->delete();
            }            
            return DBHelpers\returnDBError($exception);
        }
        return response()->json(['message' => 'Registration successful. Check your email to validate your email'], 200);
    }

    /**
     *
     * @return JsonResponse
     */
    public function getScts()
    {
        $scts =  $this->subcontractorTrades
            ->orderBy('tradeName', 'ASC')->get();
        return response()->json(['scts' => $scts], 200);
    }
}
