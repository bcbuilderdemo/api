<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Hbg\HbgStore;

use App\Models\HomeBuildersGuide\HomeBuildersGuide;
use App\Models\Subcontractors\SubcontractorTrades;
use App\Models\TradeBuilderGuide\TradeBuilderGuide;
use Prologue\Alerts\Facades\Alert;

class HbgController extends Controller
{
    /**
     * @var HomeBuildersGuide
     */
    private $hbg;

    /**
     * @var SubcontractorTrades
     */
    private $subcontractorTrades;

    /**
     * HbgController constructor.
     *
     * @param HomeBuildersGuide $hbg
     * @param SubcontractorTrades $subcontractorTrades
     */
    public function __construct(HomeBuildersGuide $hbg, SubcontractorTrades $subcontractorTrades)
    {
        $this->hbg = $hbg;
        $this->subcontractorTrades = $subcontractorTrades;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $scts =  $this->subcontractorTrades
            ->orderBy('tradeName', 'ASC')->get();

        return view('dashboard.hbgs.index', [
            'isHbg' => true,
            'scts' => $scts
        ]);
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(HbgStore $request)
    {
        $hbg = new HomeBuildersGuide();
        $hbg->stepName = $request->input('stepName');
        $hbg->stepDescription = $request->input('stepDescription');
        $hbg->sctID = $request->input('sctID');
        $hbg->stepNum = $this->hbg->max('stepNum') + 1;
        $hbg->save();

        Alert::success('Home Builders Guide successfully created')->flash();
        return redirect()->route('hbg.index')->with('alerts', Alert::all());
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy(int $id)
    {
        $this->hbg->findOrFail($id)->delete();
        Alert::success('Home Builders Guide successfully deleted')->flash();
        return redirect()->route('hbg.index')->with('alerts', Alert::all());
    }

// ------------------API-----------------//

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getHbgs(Request $request)
    {
        $columns = array (
            0 => "stepName",
            1 => "stepDescription",
            2 => "sctID",
            3 => "is_trade",
            4 => "actions"
        );

        $totalData = $this->hbg->count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $hbgs = $this->hbg->with(['trade_builder_guide'])
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $hbgs =  $this->hbg->with(['trade_builder_guide'])
                ->where('stepName','LIKE',"%{$search}%")
                ->orWhere('stepDescription', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = $this->hbg->where('stepName','LIKE',"%{$search}%")
                ->orWhere('stepDescription', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($hbgs))
        {
            foreach ($hbgs as $hbg)
            {
                $destroy =  route('hbg.destroy', $hbg->id);
                $nestedData['stepName'] = $hbg->stepName;
                $nestedData['stepDescription'] = $hbg->stepDescription;
                $nestedData['sctID'] = [
                    'sctID'=> $hbg->trade_builder_guide['st_id'],
                    'id' => $hbg->id
                ];
                $nestedData['is_trade'] = [
                    'is_trade' => $hbg->is_trade,
                    'id' => $hbg->id
                ];
                $nestedData['actions'] = "<a class='btn btn-xs btn-danger pull-right remove' data-href='$destroy'><i class='fa fa-trash'></i> Delete</a>";
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function changeSct(Request $request)
    {
        $id = $request->get('id');
        $sctID = $request->get('sctID');
        $hbg = $this->hbg->findOrFail($id);
        $hbg->sctID = $sctID;
        $hbg->save();
        if (TradeBuilderGuide::where('hbg_id', $id)->count() > 0) {
            $tbg = TradeBuilderGuide::where('hbg_id', $id)->first();
            $tbg->st_id = $sctID;
            $tbg->save();
        } else {
            $tbg = new TradeBuilderGuide();
            $tbg->hbg_id = $id;
            $tbg->st_id = $sctID;
            $tbg->save();
        }
        return response()->json(['message' => 'Subcontract Trade successfully changed', 'hbg' => $hbg]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function changeIsTrade(Request $request)
    {
        $id = $request->get('id');
        $is_trade = $request->get('is_trade');
        $hbg = $this->hbg->findOrFail($id);
        $hbg->is_trade = $is_trade;
        $hbg->save();
        return response()->json(['message' => 'Subcontract Trade successfully changed', 'hbg' => $hbg]);
    }
}
