<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Subcontractors\SubcontractorTrades;
use App\Http\Requests\SubcontractorTrade\SubcontractorTradeStore;

use Prologue\Alerts\Facades\Alert;

class SubcontractorTradesController extends Controller
{
    /**
     * @var SubcontractorTrades
     */
    private $subcontractorTrades;

    /**
     * SubcontractorTradesController constructor.
     *
     * @param SubcontractorTrades $subcontractorTrades
     */
    public function __construct(SubcontractorTrades $subcontractorTrades)
    {
        $this->subcontractorTrades = $subcontractorTrades;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard.scts.index', [
            'isScts' => true
        ]);
    }

    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(SubcontractorTradeStore $request)
    {
        $sct = new SubcontractorTrades();
        $sct->tradeName = $request->input('tradeName');
        $sct->description = $request->input('description');
        $sct->sctNameSlug = str_slug(str_replace('&', 'and', $sct->tradeName));
        $sct->save();

        Alert::success('Subcontractor Trade successfully created')->flash();
        return redirect()->route('sct.index')->with('alerts', Alert::all());
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy(int $id)
    {
        $this->subcontractorTrades->findOrFail($id)->delete();
        Alert::success('Subcontractor Trade successfully deleted')->flash();
        return redirect()->route('sct.index')->with('alerts', Alert::all());
    }

// ------------------API-----------------//

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getScts(Request $request)
    {
        $columns = array (
            0 => "tradeName",
            1 => "description",
            2 => "actions"
        );

        $totalData = $this->subcontractorTrades->count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $scts = $this->subcontractorTrades
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $scts =  $this->subcontractorTrades
                ->where('tradeName','LIKE',"%{$search}%")
                ->orWhere('description', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = $this->subcontractorTrades
                ->where('tradeName','LIKE',"%{$search}%")
                ->orWhere('description', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($scts))
        {
            foreach ($scts as $sct)
            {
                $destroy =  route('sct.destroy', $sct->id);
                $nestedData['tradeName'] = $sct->tradeName;
                $nestedData['description'] = $sct->description;
                $nestedData['actions'] = "<a class='btn btn-xs btn-danger pull-right remove' data-href='$destroy'><i class='fa fa-trash'></i> Delete</a>";
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data);
    }
}
