<?php

namespace App\Http\Controllers;

use App\Bid;
use App\CompanyInfo;
use App\Models\Chat\Chat;
use App\Models\Subcontractors\Subcontractors;
use App\Models\Subcontractors\SubcontractorTrades;
use App\Models\HomeBuildersGuide\HomeBuildersGuide;
use App\Models\TradeBuilderGuide\TradeBuilderGuide;
use App\User;
use App\Noticationnew;
use App\Subproject;
use App\Models\Uploads\ProjectFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Project;
use Tymon\JWTAuth\Facades\JWTAuth;
use Bouncer;
use App\Helpers\DBHelpers;
use App\Helpers\AuthHelpers;
use App\Helpers\GeneralHelpers;
use App\Http\Requests\Subproject\SubprojectUpdate;
use App\Http\Requests\Subproject\SubprojectInvoiceSet;
use App\Models\Bids\BidsLineItem;
use Illuminate\Support\Facades\File;
use Webpatser\Uuid\Uuid;
use Mail;



class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    private function getAuthorizedUser() {
        return Auth::user();
    }

    private function getAuthorizedUserID() {
        return Auth::user()->id;
    }



    public function createValidator(array $data)
    {

        /*
         * Validate the HTTP request.
         * Only jpeg, pmg, jpg, gif, and svg formats are allowed.
         * Max size 16mb.
         */
        $rules = [
            'projectType'       =>           'required|string|min:2|max:50',
            'buildingType'      =>           'required|string|min:2|max:50',
            'landType'          =>           'string|min:2|max:50',
            'buildValue'        =>           'required|string|min:0|max:20',
            'firstName'         =>           'required|string|min:2|max:100',
            'lastName'          =>           'required|string|min:2|max:100',
            'address'           =>           'required|string|min:2|max:100',
            'address2'          =>           'nullable|string|min:2|max:100',
            'description'       =>           'nullable|string|min:2|max:100',
            'details'           =>           'nullable|string|min:2|max:100',
            'city'              =>           'required|string|min:2|max:100',
            'postalCode'        =>           'required|string|min:2|max:100',
            'province'          =>           'required|string|min:2|max:100',
            'country'           =>           'required|string|min:2|max:100',
            'lotSize'           =>           'required|string|min:2|max:100',
            'zoning'            =>           'required|string|min:2|max:100',
            'numUnits'          =>           'required|min:1|max:100',            
        ];

        $messages = [
            'firstName.required'        =>  'First Name cannot be empty',
            'lastName.required'         =>  'Last Name cannot be empty',
            'address.required'          =>  'Address cannot be empty',
            'city.required'             =>  'City cannot be empty',
            'province.required'         =>  'Province cannot be empty',
            //'postalCode.required'     =>  'Postal Code cannot be empty',
            //'postalCode.regex'        => 'You must enter a Postal Code in the format A0A0A0',
        ];

        return Validator::make($data, $rules, $messages);
    }


    public function create(Request $request)
    {
        $user           = AuthHelpers\getAuthorizedUser();

        if ($user->isA('builder') == false) {
            return AuthHelpers\buildResponseWithMessageAndLog("User is not a builder and tried to create a project", $request);
        }
        $project            = new Project();
        $projectColumns     = $project->getFillable();
        $projectInput       = $request->only($projectColumns);
        $validator          = $this->createValidator($projectInput);

        if ($validator->fails()) { return DBHelpers\returnValidationError($validator); }

        foreach ($projectColumns as $column) {
            if (array_key_exists($column, $projectInput) && !empty($projectInput[$column])
                && $column !== 'token') {
                $project->$column = $projectInput[$column];
            }
        }

        try
        {
            $project->userID = $user->id;
            $project->save();
            $this->giveProjectPermissionToBuilder($user, $project);

            // TODO: TURN THIS INTO A JOB
            $scs = Subcontractors::all();

            foreach ($scs as $sc) {
                $chat               = new Chat();
                $chat->uuid         = GeneralHelpers\generateUniqueString($chat->getTable(), 'uuid', 12);
                $chat->scID         = $sc->userID;
                $chat->builderID    = $project->userID;
                $chat->projectID    = $project->id;
                $chat->save();
                AuthHelpers\giveChatOwnershipToBuilderIDAndSCIDByChat($chat, $project->userID, $sc->userID);
            }
            $hgbs = HomeBuildersGuide::all();
            foreach ($hgbs as $hbg) {
                $subproject = new Subproject();
                $subproject->project_id = $project->id;
                $subproject->hbg_id = $hbg->id;
                $subproject->is_available = 0;
                $subproject->is_trade = $hbg->is_trade;
                $subproject->save();
            }
        }
        catch (Exception $exception)
        {
            if ($project) { $project->delete(); }
            return DBHelpers\returnDBError($exception);
        }
        if($request['is_send_email']=='1'){ 
                       
            $scs = Subcontractors::with(['user'])                    
                 ->get();

        foreach ($scs as $key => $value) { 
         $emailids[] =  $value->user->email; 
         }  

         $name = $projectInput['firstName'].' '.$projectInput['lastName'];  
         $address = $projectInput['address'].', '.$projectInput['address2'].' '.$projectInput['city'].' '.$projectInput['province'].' '.$projectInput['country'].' - '.$projectInput['postalCode'];

                     Mail::send('emails',  ['name' => $name,'address' => $address], function ($message) use ( $emailids )
                     {
                         $message->from('no-reply@yourdomain.com', 'BCBuilder');
                         $message->to($emailids);
                         $message->subject("New Email From Your site");
                     });
         }else{

                 $scs = array('vikash@mailinator.com', 'vikash1@mailinator.com','vikash3@mailinator.com','gunjan.k@cisinlabs.com');

                 foreach ($scs as $key => $value) { 
                  $emailids[] =  $value; 
                  }  

                  $name = 'TEST'; /*$projectInput['firstName'].' '.$projectInput['lastName'];  */
                  $address = 'TEST Address';/* $projectInput['address'].', '.$projectInput['address2'].' '.$projectInput['city'].' '.$projectInput['province'].' '.$projectInput['country'].' - '.$projectInput['postalCode'];*/

                     Mail::send('emails',  ['name' => $name,'address' => $address], function ($message) use ( $emailids )
                     {
                         $message->from('no-reply@yourdomain.com', 'BCBuilder');
                         $message->to($emailids);
                         $message->subject("New Email From Your site");
                     });   
                     /*var_dump( Mail:: failures());
			exit;                */

         } 
         
//******** Send email to all Subcontractors*********///
        return response()->json(['message' => 'Your project has been saved', 'project' => $project], 200);
    }

    public function update(Request $request)
    {
        $user           = AuthHelpers\getAuthorizedUser();

        if ($user->isA('builder') == false) {
            return AuthHelpers\buildResponseWithMessageAndLog("User is not a builder and tried to update a project", $request);
        }

        $project            = Project::find($request->id);
        $projectColumns     = $project->getFillable();
        $projectInput       = $request->only($projectColumns);
        $validator          = $this->createValidator($projectInput);

        if ($validator->fails()) { return DBHelpers\returnValidationError($validator); }

        foreach ($projectColumns as $column) {
            if (array_key_exists($column, $projectInput) && !empty($projectInput[$column])
                && $column !== 'token') {
                $project->$column = $projectInput[$column];
            }
        }

        try
        {
            $project->update();
        }
        catch (Exception $exception)
        {
            if ($project) { $project->delete(); }
            return DBHelpers\returnDBError($exception);
        }
        return response()->json(['message' => 'Your project has been updated', 'project' => $project], 200);
    }
//----------------------jonas start
    public function getProjectWithSubProjects() {
        $subprojects = Project::with(['subprojects'])->get();
        return response()->json($subprojects);
    }

    public function getProject($id)
    {
        $user       = AuthHelpers\getAuthorizedUser();
        $project    = Project::find($id);
        if ($user->can('manage', $project)) {
            return response()->json(['data' => $project], 200);
        } else {
            $project = Project::with(['subprojects', 'subprojects.hbg', 'subprojects.project_files', 'builder', 'builder.company_info'])->find($id);
            return response()->json(['data' => $project], 200);
        }
    }

    public function getProjectByUserID(){

      $user       = AuthHelpers\getAuthorizedUser();
      $project    = Project::where('userID', $user->id);

      return response()->json($project);

    }

    public function getSubProjectsByHbg($hbgID)
    {
        $subproject = Subproject::with(['project'])
            ->where('hbg_id', $hbgID)
            ->where('is_filled', '<>', 1)
            ->paginate(100);
        return response()->json($subproject);
    }

   public function getSubProjectsByProjectID($projectID)
    {                 

        $subproject = Subproject::with(['project','project_files','hbg.subcontractor_count'])
            ->where('project_id', $projectID)
            ->paginate(100);          

        return response()->json($subproject); 

    }

    public function getSubProjectsBySct($sctID)
    {
        $tbg = TradeBuilderGuide::where('st_id', $sctID);
        $subprojects = [];
        $allProjects = Project::with(['builder', 'builder.company_info'])
            ->get();
        $projects = Subproject::with(['project', 'project.builder', 'project.builder.company_info', 'hbg', 'bid'])
            ->having('is_filled', '<>', 1)
            ->having('is_available', 1)
            ->get()
            ->groupBy('project_id');

        if ($tbg->count() > 0) {
            $tbgs = $tbg->get();
            $hbg_ids = [];
            foreach ($tbgs as $key => $tbg) {
                $hbg_ids[] = $tbg->hbg_id;
            }
            $subprojects = Subproject::with(['project', 'project.builder', 'project.builder.company_info', 'hbg', 'bid'])
                ->whereIn('hbg_id', $hbg_ids)
                ->having('is_filled', '<>', 1)
                ->having('is_available', 1)
                ->get();
            $subprojects = $subprojects->groupBy('project_id');
        }

        return response()->json([
            'subprojects' => $subprojects,
            'projects' => $projects,
            'all_projects' => $allProjects
        ]);
    }

    /**
     * @param int $id
     * @param SubprojectUpdate $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSubproject(int $id, SubprojectUpdate $request) {
        $subproject = Subproject::findOrFail($id);
        $subproject->update($request->validatedOnly());

        $user = AuthHelpers\getAuthorizedUser();
        $hbgs = HomeBuildersGuide::join('subcontractors as s', 's.scTradeID', '=', 'home_builders_guide.sctID')
             ->where('home_builders_guide.id', $request->hbg_id)
             ->select('*')                         
            ->get(); 

            foreach ($hbgs as $key => $value) { 

                $noticationnew = new Noticationnew();

                $noticationnew->receiverID = $value->userID;
                $noticationnew->senderID = 1;
                $noticationnew->projectID = $request->project_id; 

                $projectinfo = Project::find($request->project_id);         
               $info = $projectinfo->getAttributes();
               $noticationnew->ProjectAddress = $info['address'].', '.$info['address2'].' '.$info['city'].' '.$info['province'].' '.$info['country'].' '.$info['postalCode'];
                $noticationnew->bid = 0; 
                $noticationnew->scTradeID = $value->scTradeID;
                $noticationnew->subprojectID = $request->id;
                $noticationnew->message = 'Bids and Bid line Items are successfuly created';
                $noticationnew->read_at = 0;                
                $noticationnew->save();

            }

        return response()->json([
            'message' => 'Subproject successfully updated',
            'subproject' => $subproject
        ]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBidsBySubprojectID(int $subproject_id)
    {
        $bids = Bid::with(['subproject', 'subproject.project', 'subproject.hbg', 'subcontractor', 'subcontractor.company_info', 'subcontractor.profile', 'project_files'])->where('subprojectID', $subproject_id)->get();
        return response()->json($bids);
    }

    /**
     * @param int $subproject_id
     * @param int $subcontractor_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBidsForSubprojectFromSubcontractor(int $subproject_id, int $subcontractor_id)
    {
        $bidOut = Bid::with(['subproject', 'subproject.project', 'subproject.hbg', 'subcontractor', 'subcontractor.company_info', 'subcontractor.profile', 'project_files'])
            ->where('subprojectID', $subproject_id)
            ->where('subcontractorID', $subcontractor_id)
            ->where('is_bid_out', 1)
            ->first();
        return response()->json(['bidOut' => $bidOut]);
    }
    
    /**
     * @param SubprojectInvoiceSet $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function invoiceSet(SubprojectInvoiceSet $request)
    {
        $bid = $request->input('bid');
        $bidsLineItems = $request->input('bids_line_items');
        $project = $request->input('project');
        $subprojectID = $request->input('subproject_id');
        $is_bid_out = $request->input('is_bid_out');
        $subcontractorID = $request->input('subcontractorID');
         
        $user = AuthHelpers\getAuthorizedUser();
        $subcontractor = User::with(['trade_info'])->find($user->id);

        if (!$bid['id']) {
            $bid = Bid::create([
                'projectID'     => $project['id'],
                'builderID'     => $project['userID'],
                'scTradeID'     => $subcontractor['trade_info']['scTradeID'],
                'subprojectID'  => $subprojectID,
                'comments'      => $bid['comments'],
                'amount'        => $bid['amount'],
                'is_bid_out'    => $is_bid_out,
                'subcontractorID' => $subcontractorID
            ]);

            $noticationnew = new Noticationnew();

            if ($user->isA('subcontractor') === true) {

             $noticationnew->receiverID = $project['userID'];
             $noticationnew->senderID = $subcontractorID;

            }else{

                 $noticationnew->receiverID = $subcontractorID;
                 $noticationnew->senderID = $project['userID'];
            }                 
                $noticationnew->bid = $bid['id'];               
                $noticationnew->scTradeID = $subcontractor['trade_info']['scTradeID'];
                $noticationnew->subprojectID = $subprojectID;
                $noticationnew->message = 'Bids and Bid line Items are successfuly created';
                $noticationnew->read_at = 0;                
                $noticationnew->save();


            $retBidsLineItems = [];
            foreach ($bidsLineItems as $key => $bidsLineItem) {
                $retBidsLineItem = BidsLineItem::create([
                    'bids_id'   => $bid['id'],
                    'details'   => $bidsLineItem['details'],
                    'quantity'  => $bidsLineItem['quantity'],
                    'price'     => $bidsLineItem['price']
                ]);
                array_push($retBidsLineItems, $retBidsLineItem);
            }
            return response()->json([
                'message'           => 'Bids and Bid line Items are successfuly created',
                'bid'               => $bid,
                'bids_line_items'   => $retBidsLineItems
            ]);
        } else {
            Bid::find($bid['id'])->update([
                'comments'      => $bid['comments'],
                'amount'        => $bid['amount'],
                'is_bid_out'    => $is_bid_out
            ]);
            $retBidsLineItems = [];
            foreach ($bidsLineItems as $key => $bidsLineItem) {
                if ($bidsLineItem['id']) {
                    BidsLineItem::find($bidsLineItem['id'])->update([
                        'details'   => $bidsLineItem['details'],
                        'quantity'  => $bidsLineItem['quantity'],
                        'price'     => $bidsLineItem['price']
                    ]);
                    array_push($retBidsLineItems, $bidsLineItem);
                } else {
                    $retBidsLineItem = BidsLineItem::create([
                        'bids_id'   => $bid['id'],
                        'details'   => $bidsLineItem['details'],
                        'quantity'  => $bidsLineItem['quantity'],
                        'price'     => $bidsLineItem['price']
                    ]);
                    array_push($retBidsLineItems, $retBidsLineItem);
                }
            }

            $noticationnew = new Noticationnew();

            if ($user->isA('subcontractor') === true) {

             $noticationnew->receiverID = $project['userID'];
             $noticationnew->senderID = $subcontractorID;

            }else{

                 $noticationnew->receiverID = $subcontractorID;
                 $noticationnew->senderID = $project['userID'];
            }  

                $projectinfo = Project::find($project['id']);         
                $info = $projectinfo->getAttributes();
                $noticationnew->ProjectAddress = $info['address'].', '.$info['address2'].' '.$info['city'].' '.$info['province'].' '.$info['country'].' '.$info['postalCode'];

                $noticationnew->bid = $bid['id'];  
                $noticationnew->projectID = $project['id'];               
                $noticationnew->scTradeID = $subcontractor['trade_info']['scTradeID'];
                $noticationnew->subprojectID = $subprojectID;
                $noticationnew->message = 'Bids and Bid line Items are successfuly updated';
                $noticationnew->read_at = 0;                
                $noticationnew->save();

            return response()->json([
                'message'           => 'Bids and Bid line Items are successfuly updated',
                'bid'               => $bid,
                'bids_line_items'   => $retBidsLineItems
            ]);
        }
    }


    /**
     * @param SubprojectInvoiceSet $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addBid(SubprojectInvoiceSet $request)
    {
        $bid = $request->input('bid');
        $project = $request->input('project');
        $user = AuthHelpers\getAuthorizedUser();

        if (!$bid['id']) {
            $isExist = Bid::where('subprojectID', $bid['subprojectID'])
                ->where('is_bid_out', $bid['is_bid_out'])
                ->where('builderID', $project['userID'])
                ->where('subcontractorID', $bid['subcontractorID'])
                ->count() > 0 ? true: false;
            if ($isExist) {
                return response()->json([
                    'message'           => 'Bid already exists',
                    'bid'               => $bid,
                ]);
            }

            $bid = Bid::create([
                'projectID'       => $project['id'],
                'builderID'       => $project['userID'],
                'scTradeID'       => $bid['scTradeID'],
                'subprojectID'    => $bid['subprojectID'],
                'comments'        => $bid['comments'],
                'amount'          => $bid['amount'],
                'is_bid_out'      => $bid['is_bid_out'],
                'subcontractorID' => $bid['subcontractorID']
            ]);

            
            $noticationnew = new Noticationnew();
            $bitinfo = $bid->getAttributes();

            if ($user->isA('subcontractor') === true) {

             $noticationnew->receiverID = $project['userID'];
             $noticationnew->senderID = $bid['subcontractorID'];

            }else{

                 $noticationnew->receiverID = $bid['subcontractorID'];
                 $noticationnew->senderID = $project['userID'];
            }  

                $projectinfo = Project::find($project['id']);         
                $info = $projectinfo->getAttributes();
                $noticationnew->ProjectAddress = $info['address'].', '.$info['address2'].' '.$info['city'].' '.$info['province'].' '.$info['country'].' '.$info['postalCode'];

                $noticationnew->bid = $bitinfo['id'];  
                $noticationnew->projectID = $project['id'];              
                $noticationnew->scTradeID = $bid['scTradeID'];
                $noticationnew->subprojectID = $bid['subprojectID'];
                $noticationnew->message = 'Bids and Bid line Items are successfuly created';
                $noticationnew->read_at = 0;                
                $noticationnew->save(); 

            return response()->json([
                'message'           => 'Bids and Bid line Items are successfuly created',
                'bid'               => $bid,
            ]);
        }
    }



    public function getAllProjects() {
        $user     = AuthHelpers\getAuthorizedUser();
        $id       =  $user->id;
        if ($user->isA('subcontractor') === true) {
             $projects = Project
                        ::join('users as u', 'projects.userID', '=', 'u.id')
                        ->leftJoin('bids as b', 'projects.id', '=', 'b.projectID')
                        ->leftJoin('builder_bids_out as bbo', 'b.id', '=','bbo.bidID')
                        ->leftJoin('sc_bids_out as scbo', 'b.id', '=','scbo.bidID')
                        ->select('u.email',
                                          'projects.id as id',
                                          'projects.*',
                                          'b.id as bidID'
                        )
                        ->paginate(10);

            return response(['projectInfo' => $projects], 200);
        } elseif ($user->isA('builder') === true) {
            $projects = Project
                        ::join('users as u', 'projects.userID', '=', 'u.id')
                        ->where('projects.userID', $id)
                        ->select('u.*', 'projects.id as id', 'projects.*')
                        ->paginate(200);

            return response(['projectInfo' => $projects], 200);
        } else {
            return AuthHelpers\returnUnAuthorizedResponse();
        }
    }


/**
     * @param SubprojectInvoiceSet $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addBidforSubprojectInvoice(SubprojectInvoiceSet $request)
    {
        $bid = $request->input('bid');
        $project = $request->input('project');
        if (!$bid['id']) {
            $bid = Bid::create([
                'projectID'       => $project['id'],
                'builderID'       => $project['userID'],
                'scTradeID'       => $bid['scTradeID'],
                'subprojectID'    => $bid['subprojectID'],
                'comments'        => $bid['comments'],
                'amount'          => $bid['amount'],
                'subcontractorID' => $bid['subcontractorID']
            ]);
            return response()->json([
                'message'           => 'Bids and Bid line Items are successfuly created',
                'bid'               => $bid['id']
            ]);
        }
    }


    public function getSideBarProjects()
    {
        $user     = AuthHelpers\getAuthorizedUser();
        $id       =  $user->id;
        if ($user->isA('subcontractor') === true) {
            $projects = Project::with(['builder', 'bid'])
                        ->whereHas('bid', function ($query) use ($id) {
                            $query->where('subcontractorID', $id);
                        })->paginate(10);
            $acceptedProjects = Project::with(['builder', 'bid'])
                        ->whereHas('bid', function ($query) {
                            $query->where('status', 'accepted');
                        })->paginate(10);
            $renegotiatedProjects = Project::with(['builder', 'bid'])
                        ->whereHas('bid', function ($query) {
                            $query->where('status', 'renegotiate');
                        })->paginate(10);

            return response([
                'projects' => $projects,
                'acceptedProjects' => $acceptedProjects,
                'renegotiatedProjects' => $renegotiatedProjects
            ], 200);

        } elseif ($user->isA('builder') === true) {
            $projects = Project
                        ::join('users as u', 'projects.userID', '=', 'u.id')
                        ->where('projects.userID', $id)
                        ->select('u.*', 'projects.id as id', 'projects.*')
                        ->paginate(10);

            return response(['projectInfo' => $projects], 200);
        } else {
            return AuthHelpers\returnUnAuthorizedResponse();
        }
    }

    public function uploadFiles(Request $request)
    {
        $projectID = $request->input('projectID');
        $bidID = $request->input('bidID');
        $subprojectID = $request->input('subprojectID');
        $files = $request->file('projectFiles');
        $project = Project::find($projectID);
        if (!$project->dirID) {
            $project->dirID = Uuid::generate();
            $project->save();
        }

        $storagePath = storage_path('app/public/project');
        $projectPath = ($storagePath . '/' . $project->dirID);

        if (!File::exists($storagePath)) {
            File::makeDirectory($storagePath, 0775);
        }
        if (!File::exists($projectPath)) {
            File::makeDirectory($projectPath, 0775);
        }
        $projectFiles = [];
        if ($request->hasFile('projectFiles')) {
            foreach ($files as $file) {
                $fileID = Uuid::generate();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "jpeg", "png", "svg", "bmp"))) {
                    $iconURL = 'project/'. $project->dirID. '/'. $fileID;
                } else {
                    if ($file->getClientOriginalExtension() == 'pdf') {
                        $iconURL = 'project/'. 'defaultIcon/pdf.jpg';
                    } else if (in_array($file->getClientOriginalExtension(), array("doc", "docx"))) {
                        $iconURL = 'project/'. 'defaultIcon/doc.jpg';
                    } else {
                        $iconURL = 'project/'. 'defaultIcon/other.jpg';
                    }
                }
                $projectFile = ProjectFile::create([
                    'projectID' => $projectID,
                    'bidID' => $bidID,
                    'subprojectID' => $subprojectID,
                    'fileID' => $fileID,
                    'fileName' => $file->getClientOriginalName(),
                    'fileType' => $file->getClientOriginalExtension(),
                    'fileSize' => $file->getClientSize(),
                    'filePath' => 'project/'. $project->dirID. '/'. $fileID,
                    'iconURL'  => $iconURL,
                    'mimeType' => $file->getClientMimeType()
                ]);
                array_push($projectFiles, ProjectFile::where('fileID', $fileID)->first());
                $path = $file->move($projectPath, $fileID);
            }
        }
        return response()->json([
            'message' => "All Project files have been uploaded",
            'projectFiles' => $projectFiles
        ],  200);
    }

    public function getFilesForProjectOrBid($projectID, Request $request)
    {
        $projectFiles = ProjectFile::where('projectID', $projectID)->get();
        return response()->json(['projectFiles' => $projectFiles],  200);
    }

    public function getFile($fileID)
    {
        $user = AuthHelpers\getAuthorizedUser();

        if ($user->isA('subcontractor') == true) {

            $file = ProjectFile::where('fileID', $fileID)->first();

            return response()->file(storage_path('app/public'). '/'. $file->filePath, [
                'Content-Type'          => $file->mimeType,
                "Content-Disposition'   => 'attachment; filename='$file->fileName''",
            ]);
        }
        else if ($user->isA('builder') == true) {
            $file = ProjectFile::where('fileID', $fileID)->first();
            return response()->file(storage_path('app/public'). '/'. $file->filePath, [
                'Content-Type'          => $file->mimeType,
                "Content-Disposition'   => 'attachment; filename='$file->fileName''",
            ]);
        }
    }

    public function deleteFile($fileID)
    {
        $user = AuthHelpers\getAuthorizedUser();

        if ($user->isA('subcontractor') == true) {
        }
        else if ($user->isA('builder') == true) {
            $file = ProjectFile::where('fileID', $fileID)->first();
            $file->delete();
            return response()->json(['message' => 'Successfully Deleted'],  200);
        }
    }

//------------------------jonas end
    public function getAll()
    {
        $user     = AuthHelpers\getAuthorizedUser();
        $id       =  $user->id;
        if ($user->isA('subcontractor') === true) {
            $projects = Project
                        ::join('users as u', 'projects.userID', '=', 'u.id')
                        ->join('bids as b', 'projects.id', '=', 'b.projectID')
                        ->leftJoin('builder_bids_out as bbo', 'b.id', '=','bbo.bidID')
                        ->leftJoin('sc_bids_out as scbo', 'b.id', '=','scbo.bidID')
                        ->where('bbo.scID', $id)
                        ->where('scbo.builderAccepted', 1)
                        ->select('u.email',
                                          'projects.id as id',
                                          'projects.*',
                                          'b.id as bidID'
                        )
                        ->paginate(2000);

            //return response(['projectInfo' => $projects, 'userInfo' => $ci], 200);
            return response(['projectInfo' => $projects], 200);

        } elseif ($user->isA('builder') === true) {
            $projects = Project::with(['builder', 'builder.company_info'])
                        ->join('users as u', 'projects.userID', '=', 'u.id')
                        ->where('projects.userID', $id)
                        ->select('u.*', 'projects.id as id', 'projects.*')
                        ->paginate(2000);
            $othersProjects = Project::with(['builder', 'builder.company_info'])
                        ->join('users as u', 'projects.userID', '=', 'u.id')
                        ->where('projects.userID', '<>', $id)
                        ->select('u.*', 'projects.id as id', 'projects.*')
                        ->paginate(2000);

            $mySubprojects = Subproject::with(['project', 'project.builder', 'project.builder.company_info', 'hbg', 'bid'])
                        ->whereHas('project', function ($query) use ($id) {
                            $query->where('userID', $id);
                        })
                        ->having('is_filled', '<>', 1)
                        ->having('is_available', 1)
                        ->get()
                        ->groupBy('project_id');
            $othersSubprojects = Subproject::with(['project', 'project.builder', 'project.builder.company_info', 'hbg', 'bid'])
                        ->whereHas('project', function ($query) use ($id) {
                            $query->where('userID', '<>', $id);
                        })
                        ->having('is_filled', '<>', 1)
                        ->having('is_available', 1)
                        ->get()
                        ->groupBy('project_id');
            //return response(['projectInfo' => $projects, 'userInfo' => $ci], 200);
            return response([
                'projectInfo' => $projects,
                'othersProjectInfo' => $othersProjects,
                'mySubprojects' => $mySubprojects,
                'othersSubprojects' => $othersSubprojects
            ], 200);
        } else {
            return AuthHelpers\returnUnAuthorizedResponse();
        }
    }


    public function giveProjectPermissionToBuilder($builder, $project)
    {

        try {
            Bouncer::allow($builder)->toManage($project);
        } catch (Exception $exception) {
            if ($project) { $project->delete(); }
            return DBHelpers\returnDBError($exception);
        }
    }



    public function getProjectInfoForBBO($projectID, $type, $scTradeID, Request $request)
    {
        if (AuthHelpers\checkIfBuilderCanManageProjectByProjectID($projectID) == false) {
            $message = "User tried to get BBO details to create new Bid for ProjectID: {$projectID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }


        if ($type == 'create') {

            $details = Project
                        ::join('company_info as bci',       'bci.userID', '=', 'p.userID')
                        ->where('p.id', $projectID)
                        ->select(
                            'projects.address as projectAddress', 'projects.address2 as projectAddress2',
                            'projects.city as projectCity', 'projects.postalCode as projectPostalCode',
                            'projects.province as projectProvince', 'projects.details as projectDetails',
                            'projects.landType as projectLandType', 'projects.projectType as projectProjectType',
                            'projects.numUnits as projects.projectNumUnits', 'projects.lotSize as projectLotSize',
                            'projects.buildingSize as projectBuildingSize', 'projects.zoning as projectZoning',
                            'projects.buildValue as projectBuildValue',
                            'bci.company as bci_company', 'bci.address as bci_address', 'bci.address2 as bci_address2',
                            'bci.phoneNumber as bci_phoneNumber', 'bci.postalCode as bci_postalCode', 'bci.city as bci_city'
                        )
                        ->get();
        }
        else
        {
            $message = "User tried to get BBO details to create new Bid for ProjectID: {$projectID} with illegal parameter Type: ${type}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        $sctInfo    = SubcontractorTrades::find($scTradeID);

        return response()->json(['projectInfo' => $details, 'sctInfo' => $sctInfo], 200);
    }

    public function checkProjectOwnership(Request $request)
    {

        $user           = AuthHelpers\getAuthorizedUser();
        $permission     = "NAL";
        $project        = Project::find($request->id);
        if ($user->isA('subcontractor')) {
            $permission = ($user->can('view', $project)) ? "AL" : "NAL";
            $permission = "AL";
        }
        else if ($user->isA('builder')) {
            $permission = ($user->can('manage', $project)) ? "AL" : "NAL";
        }
        // TODO:: LOG USER TIRED VIEWING UNAUTHORIZED PROJECT
        return response()->json(['type' => $permission], 200);
    }

     /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroyProject(int $id)
    {
       Project::findOrFail($id)->delete();

        return response()->json([
            'message' => 'Project successfully deleted'
        ], 200);
    }


public function notificationsnewread(Request $request)
    {
       
       $user = AuthHelpers\getAuthorizedUser();
     
       Noticationnew::find($request->id)->update([
                'read_at'      => 1                
            ]);
       
      return response()->json(['message' => 'Notication has been saved', 'id' => $request->id], 200);

    }


public function notificationsnewload(Request $request)

    {       
       $user = AuthHelpers\getAuthorizedUser();
       $scs =  Noticationnew
                ::orderByRaw('id', 'DESC')
                ->select(
                    'noticationnew.*'                             
                        )  
                ->where('noticationnew.receiverID', $user->id) 
                ->where('noticationnew.read_at', 0)             
                ->paginate(1000);
        return response()->json(['noticationnew' => $scs], 200);   


    }

}
