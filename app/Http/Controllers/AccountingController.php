<?php

namespace App\Http\Controllers;

use App;
use App\Models\Accounting\Accounting;
use App\Models\Accounting\Accounting_Payments_Made;
use App\Models\Bids\SCBidsOut;
use App\Models\Subcontractors\SubcontractorTrades;
use Illuminate\Http\Request;
use App\Project;
use App\Helpers\AuthHelpers;
use App\Helpers\GeneralHelpers;
use App\Helpers\DBHelpers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Support\Facades\Validator;

class AccountingController extends Controller
{
    public function getAccountingForProject($projectID, $type, Request $request)
    {

        $user           = AuthHelpers\getAuthorizedUser();
        $queryIn          = Project
                            ::join('bids as b', 'projects.id', '=', 'b.projectID')
                            ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                            ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                            ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                            ->leftJoin('accounting as a', 'projects.id', '=', 'a.projectID')
                            //->leftJoin('accounting_payments_made as apm', 'a.id', '=', 'apm.accountingID')
                            ->join('company_info as bci', 'bci.userID', '=', 'bbo.builderID')
                            ->join('company_info as sci', 'sci.userID', '=', 'bbo.scID')
                            ->where('projects.id', $projectID)
                            ->select(
                            'b.*',
                                     'a.id as accountingID', 'a.deleted as accountingDeleted', 'a.totalAmount',
                                     'a.amountPaidSoFar', 'a.amountRemaining',
                                     //'apm.id as apmID', 'apm.amountPaid as apmAmountPaid', 'apm.paymentMadeOn as apmPaymentMadeOn',
                                    //'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                                    // 'scbo.cr     eated_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                                    //'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                                    //'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'
                                    'projects.address as projectAddress', 'projects.address2 as projectAddress2',
                                    'projects.city as projectCity', 'projects.postalCode as projectPostalCode',
                                    'projects.province',
                                    'bci.company as bci_company', 'bci.address as bci_address', 'bci.address2 as bci_address2',
                                    'bci.phoneNumber as bci_phoneNumber', 'bci.postalCode as bci_postalCode', 'bci.city as bci_city',
                                    'bci.companySlug as bci_companySlug',
                                    'sci.company as sci_company', 'sci.address as sci_address', 'sci.address2 as sci_address2',
                                    'sci.phoneNumber as sci_phoneNumber', 'sci.postalCode as sci_postalCode', 'sci.city as sci_city',
                                    'sci.companySlug as sci_companySlug',
                                    'c.uuid as chatUUID'

                            )
                            ->groupBy('projects.id');

        $queryOut               = Project
                                ::join('bids as b', 'projects.id', '=', 'b.projectID')
                                ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                                ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                                ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                                ->leftJoin('accounting as a', 'projects.id', '=', 'a.projectID')
                                //->leftJoin('accounting_payments_made as apm', 'a.id', '=', 'apm.accountingID')
                                ->join('company_info as bci', 'bci.userID', '=', 'bbo.builderID')
                                ->join('company_info as sci', 'sci.userID', '=', 'bbo.scID')
                                ->where('projects.id', $projectID)
                                ->where('bbo.accepted', 1)
                                ->select(
                                    'b.*',
                                            'a.id as accountingID', 'a.deleted as accountingDeleted', 'a.totalAmount',
                                            'a.amountPaidSoFar', 'a.amountRemaining',
                                            //'apm.id as apmID', 'apm.amountPaid as apmAmountPaid', 'apm.paymentMadeOn as apmPaymentMadeOn',
                                            //'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                                            // 'scbo.cr     eated_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                                            //'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                                            'projects.address as projectAddress', 'projects.address2 as projectAddress2',
                                            'projects.city as projectCity', 'projects.postalCode as projectPostalCode',
                                            'projects.province as projectProvince',
                                            'bci.company as bci_company', 'bci.address as bci_address', 'bci.address2 as bci_address2',
                                            'bci.phoneNumber as bci_phoneNumber', 'bci.postalCode as bci_postalCode', 'bci.city as bci_city',
                                            'bci.province as bci_province', 'bci.companySlug as bci_companySlug',
                                            'sci.company as sci_company', 'sci.address as sci_address', 'sci.address2 as sci_address2',
                                            'sci.phoneNumber as sci_phoneNumber', 'sci.postalCode as sci_postalCode', 'sci.city as sci_city',
                                            'sci.province as sci_province',
                                            'sci.companySlug as sci_companySlug',
                                            'c.uuid as chatUUID'
                                )
                                ->groupBy('projects.id');


        $queryOut2        = Project
                            ::join('bids as b', 'projects.id', '=', 'b.projectID')
                            ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                            ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                            ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                            ->leftJoin('accounting as a', 'projects.id', '=', 'a.projectID')
                            //->leftJoin('accounting_payments_made as apm', 'a.id', '=', 'apm.accountingID')
                            ->join('company_info as bci', 'bci.userID', '=', 'bbo.builderID')
                            ->join('company_info as sci', 'sci.userID', '=', 'bbo.scID')
                            ->where('projects.id', $projectID)
                            ->where('bbo.accepted', 1)
                            ->select(
                                'b.*',
                                'a.id as accountingID', 'a.deleted as accountingDeleted', 'a.totalAmount',
                                'a.amountPaidSoFar', 'a.amountRemaining',
                                //'apm.id as apmID', 'apm.amountPaid as apmAmountPaid', 'apm.paymentMadeOn as apmPaymentMadeOn',
                                //'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                                // 'scbo.cr     eated_at as sc_created_at', 'scbo.updated_at as sc_created_at',
                                //'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                                'projects.address as projectAddress', 'projects.address2 as projectAddress2',
                                'projects.city as projectCity', 'projects.postalCode as projectPostalCode',
                                'projects.province as projectProvince',
                                'bci.company as bci_company', 'bci.address as bci_address', 'bci.address2 as bci_address2',
                                'bci.phoneNumber as bci_phoneNumber', 'bci.postalCode as bci_postalCode', 'bci.city as bci_city',
                                'bci.province as bci_province', 'bci.companySlug as bci_companySlug',
                                'sci.company as sci_company', 'sci.address as sci_address', 'sci.address2 as sci_address2',
                                'sci.phoneNumber as sci_phoneNumber', 'sci.postalCode as sci_postalCode', 'sci.city as sci_city',
                                'sci.province as sci_province',
                                'sci.companySlug as sci_companySlug',
                                'c.uuid as chatUUID'
                            )
                            ->groupBy('projects.id');

        if ($user->isA('builder'))
        {
            if (AuthHelpers\checkIfBuilderCanManageProjectByProjectID($projectID) == false) {
                $message = "Builder tried to view accounting for Project {$projectID}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }

            if ($type == 'default' || $type == 'ar')
            {
                $accounting      =  $queryIn
                                        ->join('chat as c', function($join) use ($user) {
                                            $join->on('projects.id', '=', 'c.projectID')
                                                ->where('c.builderID', $user->id);
                                        })
                                        ->where('b.builderID', $user->id)
                                        ->paginate(20);
            }
            else if ($type == 'ap')
            {
                        $accounting      =  $queryOut
                                            ->join('chat as c', function($join) use ($user) {
                                                $join->on('projects.id', '=', 'c.projectID')
                                                    ->where('c.builderID', $user->id);
                                            })
                                            ->where('b.builderID', $user->id)
                                            ->where('a.amountRemaining', '>', 0)
                                            ->paginate(20);
            }

            else if ($type == 'all')
            {
                            $accounting  =
                                            $queryOut2
                                            ->join('chat as c', function($join) use ($user) {
                                                $join->on('projects.id', '=', 'c.projectID')
                                                    ->where('c.builderID', $user->id);
                                            })
                                            ->where('b.builderID', $user->id)
                                            ->paginate(20);
            }

            else
            {
                $message = "Builder tried to view accounting for Project {$projectID} with illegal parameter {$type}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }
        }
        else if ($user->IsA('subcontractor'))
        {
            // if (AuthHelpers\checkIfBuilderOrSCCanViewAccountingByProjectIDAndscID($projectID, $user->id) == false) {
            //     $message = "SC tried to view accounting for Project {$projectID}";
            //     return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            // }

            if ($type == 'default' || $type == 'ar')
            {
                $accounting    =  $queryOut
                                    ->join('chat as c', function($join) use ($user) {
                                        $join->on('projects.id', '=', 'c.projectID')
                                            ->where('c.scID', $user->id);
                                    })
                                    ->where('bbo.scID', $user->id)
                                    ->where('a.amountRemaining', '>', '0')
                                    ->paginate(20);
            }
            else if ($type == 're')
            {
                $accounting    =  $queryIn
                                    ->join('chat as c', function($join) use ($user) {
                                        $join->on('projects.id', '=', 'c.projectID')
                                            ->where('c.builderID', $user->id);
                                    })
                                    ->where('bbo.scID', $user->id)
                                    ->where('a.amountRemaining', '>', '0')
                                    ->paginate(20);
            }
            else if ($type == 'all')
            {
                            $accounting =  $queryIn
                                            ->join('chat as c', function($join) use ($user) {
                                                $join->on('projects.id', '=', 'c.projectID')
                                                    ->where('c.builderID', $user->id);
                                            })
                                            ->where('bbo.scID', $user->id)
                                            ->paginate(20);
            }
            else
            {
                $message = "SC tried to view accounting for Project {$projectID} with illegal parameter {$type}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }
        }

        return response()->json(['data' => $accounting], 200);

    }

    public function getSCAccountingForProject($projectID, $type, Request $request)
    {
        $user             = AuthHelpers\getAuthorizedUser();
        $queryIn          = Project
            ::join('bids as b', 'projects.id', '=', 'b.projectID')
            ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
            ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
            ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
            ->leftJoin('accounting as a', 'projects.id', '=', 'a.projectID')
            //->leftJoin('accounting_payments_made as apm', 'a.id', '=', 'apm.accountingID')
            ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
            ->where('projects.id', $projectID)
            ->where('bbo.accepted', 1)
            ->select(
                'b.*',
                'a.id as accountingID', 'a.deleted as accountingDeleted', 'a.totalAmount',
                'a.amountPaidSoFar', 'a.amountRemaining',
                'ci.companySlug'
            //'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
            // 'scbo.cr     eated_at as sc_created_at', 'scbo.updated_at as sc_created_at',
            //'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
            //'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'

            )
            ->groupBy('projects.id');

        $queryOut               = Project
            ::join('bids as b', 'projects.id', '=', 'b.projectID')
            ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
            ->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
            ->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
            ->leftJoin('accounting as a', 'projects.id', '=', 'a.projectID')
            //->leftJoin('accounting_payments_made as apm', 'a.id', '=', 'apm.accountingID')
            ->join('company_info as ci', 'ci.userID', '=', 'bbo.scID')
            ->where('projects.id', $projectID)
            ->where('bbo.accepted', 1)
            ->select(
                'b.*',
                'a.id as accountingID', 'a.deleted as accountingDeleted', 'a.totalAmount',
                'a.amountPaidSoFar', 'a.amountRemaining',
                'ci.companySlug'
            //'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
            // 'scbo.cr     eated_at as sc_created_at', 'scbo.updated_at as sc_created_at',
            //'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
            //'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'
            )
            ->groupBy('projects.id');


        /*if (AuthHelpers\checkIfBuilderOrSCCanViewAccountingByProjectIDAndscID($projectID, $user->id) == false) {
            $message = "SC tried to view accounting for Project {$projectID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }*/


        if ($type == 'default')
        {
            $date = Carbon::today('America/Vancouver')->subWeek(2);
            $accountingIn      =  $queryIn->where('bbb.scID', $user->id)->where('a.created_at', '>=', $date)->paginate(1);
            $accountingOut     =  $queryOut->where('bob.scID', $user->id)->where('a.created_at', '>=', $date)->paginate(1);
        }
        else
        {
            $message = "SC tried to access API with illegal parameter for type: {$type}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        return response()->json(['accountingIn' => $accountingIn, 'accountingOut' => $accountingOut], 200);

    }

    public function getAccountingForAllProjects($type, Request $request)
    {
        $user           = AuthHelpers\getAuthorizedUser();

        $query          =       Project
                                ::join('bids as b', 'projects.id', '=', 'b.projectID')
                                ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                                //->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                                //->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                                ->leftJoin('accounting as a', 'projects.id', '=', 'a.projectID')
                                //->leftJoin('accounting_payments_made as apm', 'a.id', '=', 'apm.accountingID')
                                ->join('company_info as bci', 'bci.userID', '=', 'bbo.builderID')
                                ->join('company_info as sci', 'sci.userID', '=', 'bbo.scID')
                                ->select(
                                    'b.*',
                                    'a.id as accountingID', 'a.deleted as accountingDeleted', 'a.totalAmount',
                                    'a.amountPaidSoFar', 'a.amountRemaining', 'a.created_at as accounting_created_at',
                                    'a.paidInFull',
                                    //'apm.id as apmID', 'apm.amountPaid as apmAmountPaid', 'apm.paymentMadeOn as apmPaymentMadeOn',
                                    //'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                                    ///'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at', 'scbo.comments as scbo_comments'.
                                    //'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                                    //'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'
                                    'projects.address as projectAddress', 'projects.address2 as projectAddress2',
                                    'projects.city as projectCity', 'projects.postalCode as projectPostalCode',
                                    'projects.province as projectProvince', 'projects.details as projectDetails',
                                    'projects.landType as projectLandType', 'projects.projectType as projectProjectType',
                                    'projects.numUnits as projects.projectNumUnits', 'projects.lotSize as projectLotSize',
                                    'projects.buildingSize as projectBuildingSize', 'projects.zoning as projectZoning',
                                    'projects.buildValue as projectBuildValue',
                                    'bci.company as bci_company', 'bci.address as bci_address', 'bci.address2 as bci_address2',
                                    'bci.phoneNumber as bci_phoneNumber', 'bci.postalCode as bci_postalCode', 'bci.city as bci_city',
                                    'bci.companySlug as bci_companySlug',
                                    'sci.company as sci_company', 'sci.address as sci_address', 'sci.address2 as sci_address2',
                                    'sci.phoneNumber as sci_phoneNumber', 'sci.postalCode as sci_postalCode', 'sci.city as sci_city',
                                    'sci.companySlug as sci_companySlug',
                                    'c.uuid as chatUUID'
                                )
                                ->groupBy('projects.id')
                                ;

        if ($user->isA('builder')) {
            if ($type == 'ap' || $type == 'default') {
                $accounting = $query
                                ->join('chat as c', function($join) use ($user) {
                                    $join->on('projects.id', '=', 'c.projectID')
                                        ->where('c.builderID', $user->id);
                                })
                                ->where('a.builderID', $user->id)
                                ->where('a.amountRemaining', '>', 0)
                                ->paginate(25);
            }
            else if ($type == 'all') {
                $accounting = $query
                                ->join('chat as c', function($join) use ($user) {
                                    $join->on('projects.id', '=', 'c.projectID')
                                        ->where('c.builderID', $user->id);
                                })
                                ->where('a.builderID', $user->id)
                                ->paginate(25);
            }
            else {
                $message = "Builder tried to access query all project accounting with illegal parameter: {$type}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }
        }

        elseif ($user->isA('subcontractor')) {
            if ($type == 'ar' || $type == 'default') {
                $accounting = $query
                                ->join('chat as c', function($join) use ($user) {
                                    $join->on('projects.id', '=', 'c.projectID')
                                        ->where('c.scID', $user->id);
                                })
                                ->where('a.scID', $user->id)
                                ->where('a.amountRemaining', '>', 0)
                                ->paginate(25);
            }
            else  if ($type == 'all') {
                $accounting = $query
                                ->join('chat as c', function($join) use ($user) {
                                    $join->on('projects.id', '=', 'c.projectID')
                                        ->where('c.scID', $user->id);
                                })
                                ->where('a.scID', $user->id)
                                ->paginate(25);
            }
            else {
                $message = "Builder tried to access query all project accounting with illegal parameter: {$type}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }
        }

        return response()->json(['data' => $accounting], 200);
    }

    public function getAccountingAndInvoiceData($projectID, $accountingID, Request $request)
    {
        $user           = AuthHelpers\getAuthorizedUser();

        $accounting          = Project
                                ::join('bids as b', 'projects.id', '=', 'b.projectID')
                                ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                                //->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                                //->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                                ->leftJoin('accounting as a', 'projects.id', '=', 'a.projectID')
                                //->leftJoin('accounting_payments_made as apm', 'a.id', '=', 'apm.accountingID')
                                ->join('company_info as bci', 'bci.userID', '=', 'bbo.builderID')
                                ->join('company_info as sci', 'sci.userID', '=', 'bbo.scID')
                                ->join('chat as c', function($join) use($projectID) {
                                    $join->on('projects.id', '=', 'c.projectID')
                                         ->where('c.projectID', $projectID);
                                })
                                ->where('projects.id', $projectID)
                                ->select(
                                    'b.*',
                                    'a.id as accountingID', 'a.deleted as accountingDeleted', 'a.totalAmount',
                                    'a.amountPaidSoFar', 'a.amountRemaining', 'a.created_at as accounting_created_at',
                                    //'apm.id as apmID', 'apm.amountPaid as apmAmountPaid', 'apm.paymentMadeOn as apmPaymentMadeOn',
                                    //'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                                    ///'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at', 'scbo.comments as scbo_comments'.
                                    //'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                                    //'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'
                                    'projects.address as projectAddress', 'projects.address2 as projectAddress2',
                                    'projects.city as projectCity', 'projects.postalCode as projectPostalCode',
                                    'projects.province as projectProvince', 'projects.details as projectDetails',
                                    'projects.landType as projectLandType', 'projects.projectType as projectProjectType',
                                    'projects.numUnits as projects.projectNumUnits', 'projects.lotSize as projectLotSize',
                                    'projects.buildingSize as projectBuildingSize', 'projects.zoning as projectZoning',
                                    'projects.buildValue as projectBuildValue',
                                    'bci.company as bci_company', 'bci.address as bci_address', 'bci.address2 as bci_address2',
                                    'bci.phoneNumber as bci_phoneNumber', 'bci.postalCode as bci_postalCode', 'bci.city as bci_city',
                                    'bci.companySlug as bci_companySlug',
                                    'sci.company as sci_company', 'sci.address as sci_address', 'sci.address2 as sci_address2',
                                    'sci.phoneNumber as sci_phoneNumber', 'sci.postalCode as sci_postalCode', 'sci.city as sci_city',
                                    'sci.companySlug as sci_companySlug',
                                    'c.uuid as chatUUID'
                                )
                                ->groupBy('projects.id')
                                ->get();

     $apm   = Accounting_Payments_Made
                ::where('accountingID', $accountingID)
                ->orderByRaw('created_at DESC')
                ->get();

    $scboli = SCBidsOut
                ::join('sc_bids_out_line_items as scboli', 'sc_bids_out.id', '=', 'scboli.scBidOutID')
                ->where('sc_bids_out.accountingID', $accountingID)
                ->where('scboli.deleted', 0)
                ->select(
                            'sc_bids_out.subTotal as scbo_subTotal', 'sc_bids_out.taxTotal as scbo_taxTotal',
                                    'sc_bids_out.grandTotal as scbo_grandTotal',
                                    'scboli.name as scboli_name', 'scboli.description as scboli_description',
                                    'scboli.quantity as scboli_quantity', 'scboli.uom as scboli_uom', 'scboli.price as scboli_price',
                                    'scboli.lineSubTotal as scboli_lineSubTotal', 'scboli.lineTaxTotal as scboli_lineTaxTotal',
                                    'scboli.lineTotal as scboli_lineTotal'
                        )
                ->get();

        if ($user->isA('builder'))
        {

            if (AuthHelpers\checkIfUserCanManageAccountingByAccountingIDAndUserObject($accountingID, $user) == false)
            {
                $message = "Builder tried to view accounting for Project {$projectID}  Accounting: {$accountingID}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }
            /*
            else
            {
                $message = "Builder tried to view accounting for Project {$projectID} Accounting ID {$accountingID}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }*/
        }
        else if ($user->IsA('subcontractor'))
        {
            if (AuthHelpers\checkIfUserCanManageAccountingByAccountingIDAndUserObject($accountingID, $user) == false) {
                $message = "SC tried to view accounting for Project {$projectID} Accounting ID {$accountingID}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }
            /*else
            {
                $message = "SC tried to view accounting for Project {$projectID} Accounting ID {$accountingID}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }*/
        }

        return response()->json(['accounting' => $accounting, 'apm' => $apm, 'scbo' => $scboli], 200);
    }

    public function makePaymentValidator(array $data)
    {

        /*
         * Validate the HTTP request.
         * Only jpeg, pmg, jpg, gif, and svg formats are allowed.
         * Max size 16mb.
         */
        $rules = [
            'paymentAmount'      =>  'required|min:1|numeric',
        ];

        $messages = [
            'paymentAmount.required'    => 'You must specify an Amount',
            'paymentAmount.min'         => 'Cannot be less than $1',
            'paymentAmount.numeric'     => 'Payment Amount must be numeric',
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function makePayment($projectID, $accountingID, Request $request)
    {

        $validate           = $this->makePaymentValidator($request->all());

        if ($validate->fails()) { return DBHelpers\returnValidationError($validate); }


        if (AuthHelpers\checkIfBuilderCanManageProjectByProjectID($projectID) == false) {
            $message = "User tried to make payment for Project: {$projectID}, Amount {$request->paymentAmount}, 
            AccountingId: {$accountingID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }
        if (AuthHelpers\checkIfBuilderOrSCCanViewAccountingByID($accountingID) == false) {
            $message = "User tried to make payment for Project: {$projectID}, Amount {$request->paymentAmount}, 
            AccountingId: {$accountingID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }


        $accounting         = Accounting::find($accountingID);
        $apm                = new Accounting_Payments_Made();
        $now                = GeneralHelpers\getDateTime();
        $paymentAmount      = $request->paymentAmount;

        //$amtRemaining       = $accounting->amountRemaining;

       /* $newRemaining                   = abs(($amtRemaining - $paymentAmount));
        $accounting->amountRemaining    =  $newRemaining;
        $accounting->lastPaymentMadeOn  = $now;
        $accounting->amountPaidSoFar    = $accounting->amountPaidSoFar + abs($paymentAmount);

        if ($newRemaining >= $amtRemaining) {
            $accounting->paidInFull         = 1;
            $accounting->paidInFullOn       = $now;
            $accounting->amountRemaining    = 0.00;
        }*/

        $apm->accountingID                  = $accountingID;
        $apm->amountPaid                    = $paymentAmount;
        $apm->paymentMadeOn                 = $now;
        $apm->status                        = 'waiting';

        try
        {
            $apm->save();

            $when = GeneralHelpers\getDateTime();

            dispatch(((new App\Jobs\NewPaymentMadeEmailJob($accounting, $apm))
                            ->onQueue('emails')
                            ->delay($when)));

            $sc = App\User::find($accounting->scID);

            $sc
                ->notify((new App\Notifications\BuilderHasSentPaymentNotification($apm, $accounting))
                    ->onQueue('queue')
                    ->delay($when->addSeconds(10)));
        }
        catch(Exception $ex)
        {
            return DBHelpers\returnDBError($ex);
        }
        try
        {
            $accounting->save();

            dispatch(new App\Jobs\NewPaymentMadeEmailJob($accounting, $apm));
        }
        catch(Exception $ex)
        {
            return DBHelpers\returnDBError($ex);
        }

        return response()->json(['message' => "Your Payment of $${paymentAmount} has been sent successfully!"], 200);
    }

    public function getAccountingReportForAllProjects()
    {
        $user       = AuthHelpers\getAuthorizedUser();


        $query = Project
                    ::join('bids as b', 'projects.id', '=', 'b.projectID')
                    ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
                    //->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
                    //->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
                    ->leftJoin('accounting as a', 'projects.id', '=', 'a.projectID')
                    //->leftJoin('accounting_payments_made as apm', 'a.id', '=', 'apm.accountingID')
                    ->join('company_info as bci', 'bci.userID', '=', 'bbo.builderID')
                    ->join('company_info as sci', 'sci.userID', '=', 'bbo.scID')
                    ->join('chat as c', function($join) {
                        $join->on('projects.id', '=', 'c.projectID');
                    })
                    ->select(
                        'b.*',
                        'a.id as accountingID', 'a.deleted as accountingDeleted', 'a.totalAmount',
                        'a.amountPaidSoFar', 'a.amountRemaining', 'a.created_at as accounting_created_at',
                        'a.paidInFull',
                        //'apm.id as apmID', 'apm.amountPaid as apmAmountPaid', 'apm.paymentMadeOn as apmPaymentMadeOn',
                        //'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                        ///'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at', 'scbo.comments as scbo_comments'.
                        //'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                        //'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'
                        'projects.address as projectAddress', 'projects.address2 as projectAddress2',
                        'projects.city as projectCity', 'projects.postalCode as projectPostalCode',
                        'projects.province as projectProvince', 'projects.details as projectDetails',
                        'projects.landType as projectLandType', 'projects.projectType as projectProjectType',
                        'projects.numUnits as projects.projectNumUnits', 'projects.lotSize as projectLotSize',
                        'projects.buildingSize as projectBuildingSize', 'projects.zoning as projectZoning',
                        'projects.buildValue as projectBuildValue',
                        'bci.company as bci_company', 'bci.address as bci_address', 'bci.address2 as bci_address2',
                        'bci.companySlug as bci_companySlug',
                        'bci.phoneNumber as bci_phoneNumber', 'bci.postalCode as bci_postalCode', 'bci.city as bci_city',
                        'sci.company as sci_company', 'sci.address as sci_address', 'sci.address2 as sci_address2',
                        'sci.phoneNumber as sci_phoneNumber', 'sci.postalCode as sci_postalCode', 'sci.city as sci_city',
                        'sci.companySlug as sci_companySlug',
                        'c.uuid as chatUUID'
                    )
                    ->groupBy('projects.id')
        ;

        if ($user->isA('subcontractor')) {

            $accounting = $query->where('a.scID', $user->id)->paginate(50);

            $totalBalanceRemainingForAllProjects       = 0.00;
            $totalBalanceOwedForAllProjects            = 0.00;
            $totalBalancePaidForAllProjects            = 0.00;
            $totalProjectsBalanceIsOutstanding         = 0;
            $totalProjectsBalanceIsPaidInFull          = 0;

            foreach ($accounting->items() as $ac) {
                $ac->totalBalanceOwedForProject                += $ac->totalAmount;
                $ac->totalBalancePaidForProject                += $ac->amountPaidSoFar;
                $ac->totalBalanceRemainingForProject           += $ac->amountRemaining;
                $totalBalanceOwedForAllProjects                += $ac->totalAmount;
                $totalBalancePaidForAllProjects                += $ac->amountPaidSoFar;
                $totalBalanceRemainingForAllProjects           += $ac->amountRemaining;
                if($ac->paidInFull != 1) {
                    $totalProjectsBalanceIsOutstanding++;
                    $ac->projectBalanceIsOutstanding    = 1;
                }
                else {
                    $totalProjectsBalanceIsPaidInFull++;
                    $ac->projectBalanceIsOutstanding    = 0;
                }
            }
            $acctData = collect([
                'totalBalanceRemainingForAllProjects'  => $totalBalanceRemainingForAllProjects,
                'totalBalanceOwedForAllProjects'       => $totalBalanceOwedForAllProjects,
                'totalBalancePaidForAllProjects'       => $totalBalancePaidForAllProjects,
                'totalProjectsBalanceIsOutstanding'    => $totalProjectsBalanceIsOutstanding,
                'totalProjectsBalanceIsPaidInFull'     => $totalProjectsBalanceIsPaidInFull
            ]);
        }
        else if ($user->isA('builder')) {

            /*$accounting     =   Accounting
                                ::join('projects as p', 'p.id', '=', 'accounting.projectID')
                                ->leftJoin('accounting_payments_made as apm', 'accounting.id', '=', 'apm.accountingID')
                                ->where('accounting.scID', $user->id)
                                ->orderByRaw('accounting.id DESC')
                                ->select(
                                    'accounting.id as accountingID', 'accounting.builderID as builderID',
                                            'accounting.scID as scID', 'accounting.projectID as projectID',
                                            'accounting.bidID as bidID', 'accounting.bboID as bboID',
                                            'accounting.scTradeID as scTradeID',
                                            'accounting.scboID as scboID', 'accounting.deleted as acct_deleted',
                                            'accounting.totalAmount as acct_totalAmount', 'accounting.amountRemaining as acct_amountRemaining',
                                            'accounting.amountPaidSoFar as acct_amountPaidSoFar', 'accounting.lastPaymentMadeOn as acct_lastPaymentMadeOn',
                                            'accounting.paidInFull as acct_paidInFull', 'accounting.paidInFullOn as acct_paidInFullOn'
                                        )
                                    ->paginate(50);*/

            $accounting = $query->where('a.builderID', $user->id)->paginate(50);

            $totalBalanceRemainingForAllProjects       = 0.00;
            $totalBalanceOwedForAllProjects            = 0.00;
            $totalBalancePaidForAllProjects            = 0.00;
            $totalProjectsBalanceIsOutstanding         = 0;
            $totalProjectsBalanceIsPaidInFull          = 0;

            foreach ($accounting->items() as $ac) {
                $ac->totalBalanceOwedForProject                += $ac->totalAmount;
                $ac->totalBalancePaidForProject                += $ac->amountPaidSoFar;
                $ac->totalBalanceRemainingForProject           += $ac->amountRemaining;
                $totalBalanceOwedForAllProjects                += $ac->totalAmount;
                $totalBalancePaidForAllProjects                += $ac->amountPaidSoFar;
                $totalBalanceRemainingForAllProjects           += $ac->amountRemaining;
                if($ac->paidInFull != 1) {
                    $totalProjectsBalanceIsOutstanding++;
                    $ac->projectBalanceIsOutstanding    = 1;
                }
                else {
                    $totalProjectsBalanceIsPaidInFull++;
                    $ac->projectBalanceIsOutstanding    = 0;
                }
            }
            $acctData = collect([
                'totalBalanceRemainingForAllProjects'  => $totalBalanceRemainingForAllProjects,
                'totalBalanceOwedForAllProjects'       => $totalBalanceOwedForAllProjects,
                'totalBalancePaidForAllProjects'       => $totalBalancePaidForAllProjects,
                'totalProjectsBalanceIsOutstanding'    => $totalProjectsBalanceIsOutstanding,
                'totalProjectsBalanceIsPaidInFull'     => $totalProjectsBalanceIsPaidInFull
            ]);
        }


        $data = $acctData->merge($accounting);
        return response()->json(['data' => $data], 200);
    }

    public function getAccountingReportForProject($projectID, Request $request)
    {
        $user       = AuthHelpers\getAuthorizedUser();

        $query = Project
            ::join('bids as b', 'projects.id', '=', 'b.projectID')
            ->join('builder_bids_out as bbo', 'b.id', '=', 'bbo.bidID')
            //->leftJoin('sc_bids_out as scbo', 'b.id', '=', 'scbo.bidID')
            //->leftJoin('sc_bids_out_line_items as scboli', 'scbo.id', '=', 'scboli.scBidOutID')
            ->leftJoin('accounting as a', 'projects.id', '=', 'a.projectID')
            //->leftJoin('accounting_payments_made as apm', 'a.id', '=', 'apm.accountingID')
            ->join('company_info as bci', 'bci.userID', '=', 'bbo.builderID')
            ->join('company_info as sci', 'sci.userID', '=', 'bbo.scID')
            ->join('chat as c', function($join) use($projectID) {
                $join->on('projects.id', '=', 'c.projectID')
                    ->where('c.projectID', $projectID);
            })
            ->where('projects.id', $projectID)
            ->select(
                'b.*',
                'a.id as accountingID', 'a.deleted as accountingDeleted', 'a.totalAmount',
                'a.amountPaidSoFar', 'a.amountRemaining', 'a.created_at as accounting_created_at',
                'a.paidInFull',
                //'apm.id as apmID', 'apm.amountPaid as apmAmountPaid', 'apm.paymentMadeOn as apmPaymentMadeOn',
                //'bbo.created_at as b_created_at', 'bbo.updated_at as b_updated_at',
                ///'scbo.created_at as sc_created_at', 'scbo.updated_at as sc_created_at', 'scbo.comments as scbo_comments'.
                //'scboli.created_at as scboli_created_at', 'scboli.updated_at as scboli_updated_at',
                //'ci.company', 'ci.address', 'ci.address2', 'ci.phoneNumber', 'ci.postalCode', 'ci.city'
                'projects.address as projectAddress', 'projects.address2 as projectAddress2',
                'projects.city as projectCity', 'projects.postalCode as projectPostalCode',
                'projects.province as projectProvince', 'projects.details as projectDetails',
                'projects.landType as projectLandType', 'projects.projectType as projectProjectType',
                'projects.numUnits as projects.projectNumUnits', 'projects.lotSize as projectLotSize',
                'projects.buildingSize as projectBuildingSize', 'projects.zoning as projectZoning',
                'projects.buildValue as projectBuildValue',
                'bci.company as bci_company', 'bci.address as bci_address', 'bci.address2 as bci_address2',
                'bci.phoneNumber as bci_phoneNumber', 'bci.postalCode as bci_postalCode', 'bci.city as bci_city',
                'bci.companySlug as bci_companySlug',
                'sci.company as sci_company', 'sci.address as sci_address', 'sci.address2 as sci_address2',
                'sci.phoneNumber as sci_phoneNumber', 'sci.postalCode as sci_postalCode', 'sci.city as sci_city',
                'sci.companySlug as sci_companySlug',
                'c.uuid as chatUUID'
            )
            ->groupBy('projects.id')
        ;

        if ($user->isA('subcontractor')) {

            if (AuthHelpers\checkIfSCCanViewProjectByProjectID($projectID) == false) {
                $message = "SC tried to view accounting for Project {$projectID}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }

            $accounting = $query->where('a.scID', $user->id)->paginate(50);

            $totalBalanceRemainingForAllProjects       = 0.00;
            $totalBalanceOwedForAllProjects            = 0.00;
            $totalBalancePaidForAllProjects            = 0.00;
            $totalProjectsBalanceIsOutstanding         = 0;
            $totalProjectsBalanceIsPaidInFull          = 0;

            foreach ($accounting->items() as $ac) {
                $ac->totalBalanceOwedForProject                += $ac->totalAmount;
                $ac->totalBalancePaidForProject                += $ac->amountPaidSoFar;
                $ac->totalBalanceRemainingForProject           += $ac->amountRemaining;
                $totalBalanceOwedForAllProjects                += $ac->totalAmount;
                $totalBalancePaidForAllProjects                += $ac->amountPaidSoFar;
                $totalBalanceRemainingForAllProjects           += $ac->amountRemaining;
                if($ac->paidInFull != 1) {
                    $totalProjectsBalanceIsOutstanding++;
                    $ac->projectBalanceIsOutstanding    = 1;
                }
                else {
                    $totalProjectsBalanceIsPaidInFull++;
                    $ac->projectBalanceIsOutstanding    = 0;
                }
            }
            $acctData = collect([
                'totalBalanceRemainingForAllProjects'  => $totalBalanceRemainingForAllProjects,
                'totalBalanceOwedForAllProjects'       => $totalBalanceOwedForAllProjects,
                'totalBalancePaidForAllProjects'       => $totalBalancePaidForAllProjects,
                'totalProjectsBalanceIsOutstanding'    => $totalProjectsBalanceIsOutstanding,
                'totalProjectsBalanceIsPaidInFull'     => $totalProjectsBalanceIsPaidInFull
            ]);
        }
        else if ($user->isA('builder')) {

            if (AuthHelpers\checkIfBuilderCanManageProjectByProjectID($projectID) == false) {
                $message = "Builder tried to view accounting report for Project {$projectID}";
                return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
            }

            $accounting = $query->where('a.builderID', $user->id)->paginate(50);

            $totalBalanceRemainingForAllProjects       = 0.00;
            $totalBalanceOwedForAllProjects            = 0.00;
            $totalBalancePaidForAllProjects            = 0.00;
            $totalProjectsBalanceIsOutstanding         = 0;
            $totalProjectsBalanceIsPaidInFull          = 0;

            foreach ($accounting->items() as $ac) {
                $ac->totalBalanceOwedForProject                += $ac->totalAmount;
                $ac->totalBalancePaidForProject                += $ac->amountPaidSoFar;
                $ac->totalBalanceRemainingForProject           += $ac->amountRemaining;
                $totalBalanceOwedForAllProjects                += $ac->totalAmount;
                $totalBalancePaidForAllProjects                += $ac->amountPaidSoFar;
                $totalBalanceRemainingForAllProjects           += $ac->amountRemaining;
                if($ac->paidInFull != 1) {
                    $totalProjectsBalanceIsOutstanding++;
                    $ac->projectBalanceIsOutstanding    = 1;
                }
                else {
                    $totalProjectsBalanceIsPaidInFull++;
                    $ac->projectBalanceIsOutstanding    = 0;
                }
            }
            $acctData = collect([
                'totalBalanceRemainingForAllProjects'  => $totalBalanceRemainingForAllProjects,
                'totalBalanceOwedForAllProjects'       => $totalBalanceOwedForAllProjects,
                'totalBalancePaidForAllProjects'       => $totalBalancePaidForAllProjects,
                'totalProjectsBalanceIsOutstanding'    => $totalProjectsBalanceIsOutstanding,
                'totalProjectsBalanceIsPaidInFull'     => $totalProjectsBalanceIsPaidInFull
            ]);
        }


        $data = $acctData->merge($accounting);
        return response()->json(['data' => $data], 200);
    }

    public function changePaymentStatus($projectID, $accountingID, $apmID, Request $request) {



        $status         = $request->status;
        $user           = AuthHelpers\getAuthorizedUser();

        if ($user->isNotA('subcontractor')) {
            $message = "User tried to change status of payment to {$status} for Project {$projectID}, 
                        AccID: ${accountingID}, apmID: ${apmID} but is not a subcontractor";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);

        }
        else if (AuthHelpers\checkIfBuilderOrSCCanViewAccountingByID($accountingID) == false) {
            $message = "User tried to change status of payment to {$status} for Project {$projectID}, 
                        AccID: ${accountingID}, apmID: ${apmID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        $accounting             = Accounting::find($accountingID);
        $apm                    = Accounting_Payments_Made::find($apmID);
        $now                    = GeneralHelpers\getDateTime();

        if ($apm->status != 'waiting') {
            $message = "User tried to change status of payment to {$status} for Project {$projectID}, 
                        AccID: ${accountingID}, apmID: ${apmID} but the payment status has already been changed";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }
        if ($status === 'confirm') {
            $apm->status                = 'approved';
            $apm->scConfirmedPaymentOn  = $now;
            $msg                        = 'received';
            $paymentAmount                  = $apm->amountPaid;
            $amtRemaining                   = $accounting->amountRemaining;

            $newRemaining                   = $amtRemaining - $paymentAmount;
            $accounting->amountRemaining    =  $newRemaining;
            $accounting->lastPaymentMadeOn  = $now;
            $accounting->amountPaidSoFar    = $accounting->amountPaidSoFar + abs($paymentAmount);

            if ($newRemaining >= $amtRemaining || $newRemaining < 0) {
                $accounting->paidInFull         = 1;
                $accounting->paidInFullOn       = $now;
                $accounting->amountRemaining    = 0.00;
            }

            $apm->accountingID                  = $accountingID;
            $apm->amountPaid                    = $paymentAmount;
            $apm->paymentMadeOn                 = $now;
        }
        else if ($status === 'reject') {
            $apm->status                    = 'rejected';
            $msg                            = 'not received';

            $apm->scUnConfirmedPaymentOn    = GeneralHelpers\getDateTime();
        }
        else {
            $message = "User tried to change status of payment to illegal parameter {$status} for Project {$projectID}, 
                        AccID: ${accountingID}, apmID: ${apmID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        try {
            $apm->save();
            $accounting->save();

            $when = GeneralHelpers\getDateTime();

            dispatch(((new App\Jobs\SCHasChangedPaymentStatusJob($accounting, $apm))
                        ->onQueue('emails')
                        ->delay($when)));

            $builder = App\User::find($accounting->builderID);

            $builder
                ->notify((new App\Notifications\SCHasChangedPaymentStatusNotification($apm, $accounting))
                ->onQueue('queue')
                ->delay($when->addSeconds(10)));
        }
        catch(Exception $ex) {
            return DBHelpers\returnDBError($ex);
        }

        return response()->json(['message' => "The Payment has been marked as {$msg}"], 200);

    }
}
