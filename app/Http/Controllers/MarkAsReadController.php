<?php

namespace App\Http\Controllers;

use App\Models\Bids\BuilderBidsOut;
use App\Models\Bids\SCBidsOut;
use App\Models\Chat\Message;
use Illuminate\Http\Request;
use App\Helpers;
class MarkAsReadController extends Controller
{


    public function markBBOAsReadBySC($projectID, $bidID, $bboID, Request $request)
    {
        if (Helpers\AuthHelpers\checkIfSCCanViewBuilderBidOutByBBOID($bboID) == false) {
            $message = "User tried to mark BBO as Read for Project: $projectID, BBO ID: $bboID";
            return Helpers\AuthHelpers\buildResponseWithMessageAndLog($message, $request);
        }

        $user       = Helpers\AuthHelpers\getAuthorizedUser();
        $nots       = $user
                        ->unreadNotifications
                        ->where('type', "App\Notifications\NewBBONotification")
                        ->where('data.projectID', $projectID)
                        /*->where('data.bidID', $bidID)
                        ->where('data.bboID', $bboID)
                        ->where('data.scID', $user->id)*/
                        ->first();


        $bbo                = BuilderBidsOut::find($bboID);


        try {
            if (count($nots) && $nots->count > 1) {
                $nots->count--;
                $nots->save();
            }
            else if (count($nots) && $nots->count <= 1) {
                $nots->delete();
            }
            if ($bbo->scViewed == 0 || $bbo->scViewed == null) {
                $bbo->scViewed          = 1;
                $bbo->scViewedAt        = Helpers\GeneralHelpers\getDateTime();
                $bbo->save();
            }
        }
        catch (\Exception $ex) {
            $message = "Could not mark notification as read for BBO $bboID projectiD $projectID";
            return Helpers\AuthHelpers\buildNotificationErrorReadWithDebug($ex, $message);
        }
        $message = "BBO marked as read. BBOID: $bboID projectiD $projectID";
        return Helpers\AuthHelpers\buildNotificationMarkedAsReadWithDebug($message);
    }

    public function markSCBOAsReadByBuilder($projectID, $bidID, $scboID, Request $request)
    {
        if (Helpers\AuthHelpers\checkIfBuilderCanViewSCBidOutBySCBOID($scboID) == false) {
            $message = "Tried to mark SCBO as Read for Project: $projectID, SCBO ID: $scboID";
            return Helpers\AuthHelpers\buildResponseWithMessageAndLog($message, $request);
        }

        $user       = Helpers\AuthHelpers\getAuthorizedUser();
        $nots       = $user
                        ->unreadNotifications
                        ->where('type', "App\Notifications\NewSCBONotification")
                        ->where('data.projectID', $projectID)
                        /*->where('data.bidID', $bidID)
                        ->where('data.scboID', $scboID)
                        ->where('data.builderID', $user->id)*/
                        ->first();

        $scbo       = SCBidsOut::find($scboID);

        try {
            if (count($nots) && $nots->count >= 1) {
                $nots->count--;
                $nots->save();
            }
            else if (count($nots) && $nots->count <= 1) {
                $nots->delete();
            }
            if ($scbo->builderViewed == 0 || $scbo->builderViewed == null) {
                $scbo->builderViewed    = 1;
                $scbo->builderViewedAt  = Helpers\GeneralHelpers\getDateTime();
                $scbo->save();
            }
        }
        catch (\Exception $ex) {
            $message = "Could not mark notification as read for SCBO $scboID projectID $projectID";
            return Helpers\AuthHelpers\buildNotificationErrorReadWithDebug($ex, $message);
        }

        $message = "SCBO marked as read. SCBO $scboID projectID $projectID;";
        return Helpers\AuthHelpers\buildNotificationMarkedAsReadWithDebug($message);

    }

    public function markSCBOAsReadAfterBuilderChangesStatus($projectID, $bidID, $scboID, Request $request)
    {
        if (Helpers\AuthHelpers\checkIfSCCanManageSCBidOutBySCBOID($scboID) == false) {
            $message = "SC tried to mark SCBO as Read (after builder status change) for Project: $projectID, SCBO ID: $scboID";
            return Helpers\AuthHelpers\buildResponseWithMessageAndLog($message, $request);
        }

        $user       = Helpers\AuthHelpers\getAuthorizedUser();
        $nots       = $user
                        ->unreadNotifications
                        ->where('type', "App\Notifications\BuilderAcceptedBidNotification")
                        ->where('data.projectID', $projectID)
                        /*->where('data.bidID', $bidID)
                        ->where('data.scboID', $scboID)
                        ->where('data.builderID', $user->id)*/
                        ->first();

         if (! count($nots)) {
             $nots   = $user
                        ->unreadNotifications
                        ->where('type', "App\Notifications\BuilderRejectedBidNotification")
                        ->where('data.projectID', $projectID)
                        /*->where('data.bidID', $bidID)
                        ->where('data.builderID', $user->id)*/
                        ->first();
         }

        $scbo       = SCBidsOut::find($scboID);

        try {
            if (count($nots) && $nots->count >= 1) {
                $nots->count--;
                $nots->save();
            }
            else if (count($nots) && $nots->count <= 1) {
                $nots->delete();
            }
            if ($scbo->sc_read_ == 0 || $scbo->sc_read == null) {
                $scbo->sc_read      = 1;
                $scbo->sc_read_at  = Helpers\GeneralHelpers\getDateTime();
                $scbo->save();
            }
        }
        catch (\Exception $ex) {
            $message = "Could not mark notification as read for SCBO $scboID projectID $projectID";
            return Helpers\AuthHelpers\buildNotificationErrorReadWithDebug($ex, $message);
        }

        $message = "SCBO marked as read. SCBO $scboID projectID $projectID;";
        return Helpers\AuthHelpers\buildNotificationMarkedAsReadWithDebug($message);

    }

    public function markMessagesAsRead($projectID, $chatUUID, Request $request)
    {
        if (Helpers\AuthHelpers\checkIfBuilderOrSCCanManageChatByChatUUID($chatUUID) == false) {
            $message = "Tried to mark MSG as Read for Project: $projectID, Chat ID: $chatUUID";
            return Helpers\AuthHelpers\buildResponseWithMessageAndLog($message, $request);
        }

        $user       = Helpers\AuthHelpers\getAuthorizedUser();
        $nots       = $user
                        ->unreadNotifications
                        ->where('type', "App\Notifications\NewMessageNotification")
                        ->where('data.projectID', $projectID)
                        ->where('data.chatUUID', $chatUUID)
                        /*->where('data.bidID', $bidID)
                        ->where('data.scboID', $scboID)
                        ->where('data.builderID', $user->id)*/
                        ->first();


        try {
            if (count($nots) && $nots->count >= 1) {
                $nots->count--;
                $nots->save();
            }
            else if (count($nots) && $nots->count <= 1) {
                $nots->delete();
            }
            if ($user->isA('builder')) {
                $msgs = Message::where('chatID', $chatUUID)->where('readByBuilder', 0)->get();

                if ($msgs) {
                    foreach ($msgs as $msg) {
                        $msg->readByBuilder         = 1;
                        $msg->readByBuilderAt       = Helpers\GeneralHelpers\getDateTime();
                        $msg->save();
                    }
                }
            }
            elseif ($user->isA('subcontractor')) {
                $msgs = Message::where('chatID', $chatUUID)->where('readBySC', 0)->get();

                if ($msgs) {
                    foreach ($msgs as $msg) {
                        $msg->readBySC         = 1;
                        $msg->readBySCAt       = Helpers\GeneralHelpers\getDateTime();
                        $msg->save();
                    }
                }
            }
        }
        catch (\Exception $ex) {
            $message = "Could not mark Notification as read for: $projectID, Chat ID: $chatUUID";
            return Helpers\AuthHelpers\buildNotificationErrorReadWithDebug($ex, $message);
        }

        $message = "Notification marked as read for: $projectID, Chat ID: $chatUUID";
        return Helpers\AuthHelpers\buildNotificationMarkedAsReadWithDebug($message);

    }
}
