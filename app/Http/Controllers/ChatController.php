<?php

namespace App\Http\Controllers;

use App\CompanyInfo;
use App\Events\NewMessage;
use App\Models\Chat\Chat;
use App\Models\Chat\Message;
use App\Models\Uploads\ChatUploads;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Kyslik\ColumnSortable\Sortable;
use Exception;
use Illuminate\Support\Facades\File;
use Symfony\Component\Console\Helper\Helper;
use Webpatser\Uuid\Uuid;

class ChatController extends Controller
{
    public function getMessages($projectID, $uuid)
    {
        $user           = Helpers\AuthHelpers\getAuthorizedUser();

        if ($user->isA('builder')) {
            $msgs           = Chat
                              ::join('projects as p', function($join) use($projectID) {
                                  $join->on('chat.projectID', '=', 'p.id')
                                      ->where('chat.projectID', $projectID)
                                      ->where('p.id', $projectID);
                              })
                              ->leftJoin('messages as m',  function($join) use($uuid) {
                                 $join->on( 'chat.uuid', '=', 'm.chatID')
                                     ->where('m.chatID', $uuid);
                              })
                              ->join('company_info as bci', function($join) use ($user) {
                                  $join->on('chat.builderID', '=', 'bci.userID')
                                        //->where('chat.builderID', '=', 'bci.userID')
                                        ->where('bci.userID', '=', $user->id);
                              })
                            ->join('company_info as sci', function($join) {
                                $join->on('chat.scID', '=', 'sci.userID');
                                     //->where('chat.scID', '=', 'sci.userID');
                            })
                            ->select
                            (
                                'chat.uuid as chatUUID', 'chat.projectID', 'chat.builderID', 'chat.scID', 'chat.projectID',
                                'm.*',
                                'bci.company as bci_company', 'bci.userID as currentUserID', 'bci.logoURL as currentUserLogoURL',
                                'sci.company as sci_company', 'sci.logoURL as otherUserLogoURL',
                                'p.address as projectAddress', 'p.address2 as projectAddress2', 'p.city as projectCity',
                                'p.province as projectProvince'
                            )
                            ->where('chat.uuid', $uuid)
                            ->where('chat.builderID', $user->id)
                            ->orderByRaw('m.created_at DESC')
                           ->paginate(500)
                           //->sortBy('id')
                            ;

        }

        else if ($user->isA('subcontractor')) {
            $msgs           = Chat
                                ::join('projects as p', function($join) use($projectID) {
                                    $join->on('chat.projectID', '=', 'p.id')
                                        ->where('chat.projectID', $projectID)
                                        ->where('p.id', $projectID);
                                })
                                ->leftJoin('messages as m',  function($join) use($uuid) {
                                    $join->on( 'chat.uuid', '=', 'm.chatID')
                                        ->where('m.chatID', $uuid);
                                })
                                ->join('company_info as bci', function($join) use ($user) {
                                    $join->on('chat.scID', '=', 'bci.userID')
                                        //->where('chat.builderID', '=', 'bci.userID')
                                        ->where('bci.userID', '=', $user->id);
                                })
                                ->join('company_info as sci', function($join) {
                                    $join->on('chat.scID', '=', 'sci.userID');
                                    //->where('chat.scID', '=', 'sci.userID');
                                })
                                ->select
                                (
                                    'chat.uuid as chatUUID', 'chat.projectID', 'chat.builderID', 'chat.scID', 'chat.projectID',
                                    'm.*',
                                    'bci.company as bci_company', 'bci.userID as currentUserID', 'bci.logoURL as currentUserLogoURL',
                                    'sci.company as sci_company', 'sci.logoURL as otherUserLogoURL',
                                    'p.address as projectAddress', 'p.address2 as projectAddress2', 'p.city as projectCity',
                                    'p.province as projectProvince'
                                )
                                ->where('chat.uuid', $uuid)
                                ->where('chat.scID', $user->id)
                                ->orderByRaw('m.created_at DESC')
                                ->paginate(500);

        }

        $uploads            = ChatUploads
                                ::all('id', 'fileUUID', 'created_at', 'builderID', 'scID', 'sentByID', 'fileName', 'chatUUID', 'fileType')
                                ->where('chatUUID', $uuid)
                                ;

        if ($uploads->count()) {
            foreach ($msgs as $msg) {
                if ($msg->fileUploaded == 1) {
                    $upload = $uploads->find($msg->chatUploadID);
                    $msg->fileUUID = $upload->fileUUID;
                }
            }


            $sp = storage_path('app/public/assets/file_icons/');
            foreach ($uploads as $file) {
                $file->id = null;
                $fp = $sp . $file->fileType . '_icon.png';
                if (File::exists($fp)) {
                    //$file->iconURL = Storage::url('assets/file_icons/' . $file->fileType . '_icon.png');
                    $file->iconURL = 'assets/file_icons/' . $file->fileType . '_icon.png';
                } else {
                    //$file->iconURL = Storage::url('assets/file_icons/default_icon.png');
                    $file->iconURL = 'assets/file_icons/default_icon.png';
                }
            }
        }

        return response()->json(['chat' => $msgs, 'uploads' => $uploads], 200);
    }


    public function sendNewMessageValidator(array $data)
    {

        $rules = [
            'message'            => 'string|required|min:1|max:500',
        ];

        $messages = [
            'message.string'            => 'Your message can only be a string',
            'message.min'               => 'Your message cannot be less than 1 character',
            'message.max'               => 'Your message cannot exceed 500 characters',
            'message.required'          => 'You must enter a value for message',
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function newMessage($projectID, $uuid, Request $request) {

        $newMsg        = $request->message;

        $validator  = $this->sendNewMessageValidator($request->all());

        if ($validator->fails()) { return Helpers\DBHelpers\returnValidationError($validator); }

        if (Helpers\AuthHelpers\checkIfBuilderOrSCCanManageChatByChatUUID($uuid) == false) {
            $message = "User tried to post message for ProjectID: {$projectID}, ChatID: {$uuid}, Message: {$request->get['message']}";
            return Helpers\AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        $chat               = Chat::where('uuid', $uuid)->first();
        $msg                = New Message();
        $msg->message       = $newMsg;
        $msg->scID          = $chat->scID;
        $msg->builderID     = $chat->builderID;
        $msg->chatID        = $uuid;
        $msg->sentByID      = Helpers\AuthHelpers\getAuthorizedUser()->id;

        try {
            $msg->save();
        }
        catch (\Exception $ex) {
            return Helpers\DBHelpers\returnDBError($ex);
        }

        try {
            event(new NewMessage($msg, Helpers\AuthHelpers\getAuthorizedUser()));

        }
        catch (\Exception $ex) {

            // TODO: REMOVE RETURN
            return $ex;
            return Helpers\DBHelpers\returnDBErrorWithCustomMessage($ex, "An error has occurred. Please refresh your page");
        }

        return response()->json(['message' => 'Message has been sent'], 200);


    }

    public function uploadFileValidator(array $data)
    {

        $rules = [
            'file'     => 'required|max:100000',
        ];

        $messages = [
            'file.required'    => 'You must choose an File',
            'image.max'         => 'The File cannot be larger than 100 mb',
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function uploadFile($projectID, $uuid, Request $request)
    {
        $validator  = $this->uploadFileValidator($request->all());

        if ($validator->fails()) { return Helpers\DBHelpers\returnValidationError($validator); }


        if (Helpers\AuthHelpers\checkIfBuilderOrSCCanManageChatByChatUUID($uuid) == false) {
            $message = "User tried to upload file for ProjectID: {$projectID}, ChatID: {$uuid}";
            return Helpers\AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }


/*        $uploadDir = storage_path("app/uploads/project/$projectID/chat/$uuid");

        if (!File::exists($uploadDir)) {
            File::makeDirectory($uploadDir, 0775);
        }*/

        $storagePath        = storage_path('app/uploads');
        $projectPath        = ($storagePath . '/' . "project");
        $projectIDPath      = ($projectPath . '/' . $projectID);
        $chatPath           = ($projectIDPath . '/' . "chat");
        $uploadDir          = ("$chatPath/$uuid");

        // Make new folder for uploads
        if (!File::exists($storagePath)) {
            File::makeDirectory($storagePath, 0775);
        }
        if (!File::exists($projectPath)) {
            File::makeDirectory($projectPath, 0775);
        }
        if (!File::exists($projectIDPath)) {
            File::makeDirectory($projectIDPath, 0775);
        }
        if (!File::exists($chatPath)) {
            File::makeDirectory($chatPath, 0775);
        }
        if (!File::exists($uploadDir)) {
            File::makeDirectory($uploadDir, 0775);
        }

        $file           = $request->file('file');
        $orgName        = $file->getClientOriginalName();
        $ext            = $file->getClientOriginalExtension();
        $fileUUID       = Uuid::generate();
        $name           = "$fileUUID.$ext";

        $path                       = $file->move($uploadDir, $name);

        $chat                       = Chat::where('uuid', $uuid)->first();
        $user                       = Helpers\AuthHelpers\getAuthorizedUser();

        $upload                     = new ChatUploads();
        $upload->fileName           = $orgName;
        $upload->fileUUID           = $fileUUID;
        $upload->fileType           = $ext;
        $upload->mimeType           = $file->getClientMimeType();
        $upload->filePath           = $path;
        $upload->fileSize           = Helpers\GeneralHelpers\getFileSize($file->getClientSize());
        $upload->fileURL            = "uploads/chat/$projectID/$uuid/$name";
        $upload->builderID          = $chat->builderID;
        $upload->scID               = $chat->scID;
        $upload->sentByID           = $user->id;
        $upload->chatUUID           = $uuid;

        $msg                        = new Message();
        $msg->chatID                = $uuid;
        $msg->builderID             = $chat->builderID;
        $msg->scID                  = $chat->scID;
        $msg->sentByID              = $user->id;
        $company                    = CompanyInfo::find($user->id);
        $msg->message               = "$company->company sent a file: $orgName";
        $msg->fileUploaded          = 1;
        try
        {
            $upload->save();
            try
            {
                $msg->chatUploadID = $upload->id;
                $msg->save();
            }
            catch (Exception $ex)
            {
                if ($upload->id > 1) { $upload->delete(); }
                $fp = $uploadDir . '/' . $name;
                if (File::exists($fp)) { unlink($fp); }
                return Helpers\DBHelpers\returnDBError($ex);
            }
        }
        catch (Exception $ex)
        {
            $fp = $uploadDir . '/' . $name;
            if (File::exists($fp)) { unlink($fp); }
            return Helpers\DBHelpers\returnDBError($ex);
        }

        try {
            event(new NewMessage($msg));
        }
        catch (\Exception $ex) {
            return Helpers\DBHelpers\returnDBErrorWithCustomMessage($ex, "An error has occurred. Please refresh your page");
        }

        return response()->json(['message' => "$orgName has successfully been uploaded"], 200);
    }

    public function getUploadForChat($projectID, $uuid, $fileUUID, Request $request)
    {
        if (Helpers\AuthHelpers\checkIfBuilderOrSCCanManageChatByChatUUID($uuid) == false) {
            $message = "User tried to view file for ProjectID: {$projectID}, Chat UUID: $uuid, File UUID: {$uuid}";
            return Helpers\AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        $file = ChatUploads::where('fileUUID', $fileUUID)->first();

        return response()->file($file->filePath, [
            'Content-Type'          => $file->mimeType,
            "Content-Disposition'   => 'attachment; filename='$file->fileName''",
        ]);

    }

    public function arraySortByColumn($array, $column, $direction = SORT_ASC)
    {
        $ref_array = array();

        foreach($array as $key=>$row) {
            $ref_array[$key] = $row[$column];
        }

        array_multisort($ref_array, $direction, $array);
    }

    public function getAllMessages(Request $request)
    {
        $user           = Helpers\AuthHelpers\getAuthorizedUser();

        if ($user->isA('builder')) {

            $msgs = Chat
                        ::join('messages as m', 'chat.uuid', '=', 'm.chatID')
                        ->leftJoin('chat_uploads as u', 'chat.uuid', '=', 'u.chatUUID')
                        ->join('company_info as bci', function($query) {
                            $query->on('chat.builderID', '=', 'bci.userID');
                        })
                        ->join('company_info as sci', function($query) {
                            $query->on('chat.scID', '=', 'sci.userID');
                        })
                        ->join('projects as p', function($query) {
                            $query->on('chat.projectID', '=', 'p.id');
                        })
                        ->select
                        (
                            'chat.uuid',
                            'm.*',
                            'u.fileUUID',
                            'bci.company as bci_company', 'bci.companySlug as bci_companySlug', 'bci.logoURL as bci_logoURL',
                            'sci.company as sci_company', 'sci.companySlug as sci_companySlug', 'sci.logoURL as sci_logoURL',
                            'p.address as projectAddress', 'p.address2 as projectAddress2', 'p.city as projectCity',
                            'p.province as projectProvince', 'p.id as projectID'

                        )
                        ->where('chat.builderID', $user->id)
                        //->where('m.builderID', $user->id)
                        //->where('u.builderID', $user->id)
                        ->where('m.sentByID', '!=', $user->id)
                        ->groupBy('chat.id')
                        ->orderByRaw('m.id DESC')
                        ->paginate(50);
        }
        else if ($user->isA('subcontractor')) {

            $msgs = Chat
                        ::join('messages as m', 'chat.uuid', '=', 'm.chatID')
                        ->leftJoin('chat_uploads as u', 'chat.uuid', '=', 'u.chatUUID')
                        ->join('company_info as bci', function($query) {
                            $query->on('chat.builderID', '=', 'bci.userID');
                        })
                        ->join('company_info as sci', function($query) {
                            $query->on('chat.scID', '=', 'sci.userID');
                        })
                        ->join('projects as p', function($query) {
                            $query->on('chat.projectID', '=', 'p.id');
                        })
                        ->select
                        (
                            'chat.uuid',
                            'm.*',
                            'u.fileUUID',
                            'bci.company as bci_company', 'bci.companySlug as bci_companySlug', 'bci.logoURL as bci_logoURL',
                            'sci.company as sci_company', 'sci.companySlug as sci_companySlug', 'sci.logoURL as sci_logoURL',
                            'p.address as projectAddress', 'p.address2 as projectAddress2', 'p.city as projectCity',
                            'p.province as projectProvince', 'p.id as projectID'

                        )
                        ->where('chat.scID', $user->id)
                        //->where('m.builderID', $user->id)
                        //->where('u.builderID', $user->id)
                        ->where('m.sentByID', '!=', $user->id)
                        ->groupBy('chat.id')
                        ->orderByRaw('m.id DESC')
                        ->paginate(50);
        }

        return response()->json(['data' => $msgs], 200);
    }

    public function getAllMessagesForProject($projectID, Request $request)
    {
        $user           = Helpers\AuthHelpers\getAuthorizedUser();

        if ($user->isA('builder')) {

            $msgs = Chat
                        ::join('messages as m', 'chat.uuid', '=', 'm.chatID')
                        ->leftJoin('chat_uploads as u', 'chat.uuid', '=', 'u.chatUUID')
                        ->join('company_info as bci', function($query) {
                            $query->on('chat.builderID', '=', 'bci.userID');
                        })
                        ->join('company_info as sci', function($query) {
                            $query->on('chat.scID', '=', 'sci.userID');
                        })
                        ->join('projects as p', 'chat.projectID', '=', 'p.id')
                        ->select
                        (
                            'chat.uuid',
                            'm.*',
                            'u.fileUUID',
                            'bci.company as bci_company', 'bci.companySlug as bci_companySlug', 'bci.logoURL as bci_logoURL',
                            'sci.company as sci_company', 'sci.companySlug as sci_companySlug', 'sci.logoURL as sci_logoURL',
                            'p.address as projectAddress', 'p.address2 as projectAddress2', 'p.city as projectCity',
                            'p.province as projectProvince', 'p.id as projectID'
                        )
                        ->where('chat.builderID', $user->id)
                        //->where('m.builderID', $user->id)
                        //->where('u.builderID', $user->id)
                        ->where('m.sentByID', '!=', $user->id)
                        ->groupBy('chat.id')
                        ->orderByRaw('m.id DESC')
                        ->paginate(50);
        }
        else if ($user->isA('subcontractor')) {

            $msgs = Chat
                    ::join('messages as m', 'chat.uuid', '=', 'm.chatID')
                    ->leftJoin('chat_uploads as u', 'chat.uuid', '=', 'u.chatUUID')
                    ->join('company_info as bci', function($query) {
                        $query->on('chat.builderID', '=', 'bci.userID');
                    })
                    ->join('company_info as sci', function($query) {
                        $query->on('chat.scID', '=', 'sci.userID');
                    })
                    ->join('projects as p', 'chat.projectID', '=', 'p.id')
                    ->select
                    (
                        'chat.uuid',
                        'm.*',
                        'u.fileUUID',
                        'bci.company as bci_company', 'bci.companySlug as bci_companySlug', 'bci.logoURL as bci_logoURL',
                        'sci.company as sci_company', 'sci.companySlug as sci_companySlug', 'sci.logoURL as sci_logoURL',
                        'p.address as projectAddress', 'p.address2 as projectAddress2', 'p.city as projectCity',
                        'p.province as projectProvince', 'p.id as projectID'
                    )
                    ->where('chat.scID', $user->id)
                    //->where('m.builderID', $user->id)
                    //->where('u.builderID', $user->id)
                    ->where('m.sentByID', '!=', $user->id)
                    ->groupBy('chat.id')
                    ->orderByRaw('m.id DESC')
                    ->paginate(50);
        }

        return response()->json(['data' => $msgs], 200);
    }






}
