<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Http\Requests\Post\PostIndex;
use App\Http\Requests\Post\PostStore;
use App\Http\Requests\Post\PostUpdate;
use Illuminate\Support\Facades\File;
use Webpatser\Uuid\Uuid;
use App\Models\Uploads\PostFile;

class PostController extends Controller
{
    /**
     * @var Post
     */
    private $post;

    /**
     * PostController constructor.
     *
     * @param Team $team
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * @param PostIndex $request
     *
     * @return JsonResponse
     */
    public function index(PostIndex $request)
    {
        $posts = $this->post
            ->with(['user', 'user.company_info'])
            ->paginate(20); 
        return response()->json([
            'posts' => $posts
        ], 200);
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function show(int $id)
    {
        $post = $this->post
            ->with(['user', 'user.company_info'])
            ->findOrFail($id);

        return response()->json([
            'post' => $post
        ], 200);
    }

    /**
     * @param PostStore $request
     *
     * @return JsonResponse
     */
    public function store(PostStore $request)
    {
        $post = $this->post->create([
            'userID' => $request->get('userID'),
            'post' => $request->get('post'),
        ]);

        $post = $this->post
            ->with(['user', 'user.company_info'])
            ->findOrFail($post['id']);
        $post['ago'] = $post->updated_at->diffForHumans();
        return response()->json([
            'message' => 'Post successfully created',
            'post' => $post
        ], 200);
    }

    /**
     * @param PostUpdate $request
     *
     * @return JsonResponse
     */
    public function update(PostUpdate $request)
    {
        $post = $this->post->findOrFail($request->input('id'));
        $post->update([
            'userID' => $request->get('userID'),
            'post' => $request->get('post'),
        ]);

        return response()->json([
            'message' => 'Post successfully updated',
            'post' => $post
        ], 200);
    }

    public function uploadFiles(Request $request)
    {
        $postID = $request->input('postID');
        $files = $request->file('postFiles');
        $post = Post::find($postID);
        $post->dirID = Uuid::generate();
        $post->save();

        $storagePath = storage_path('app/public/post');
        $postPath = ($storagePath . '/' . $post->dirID);

        if (!File::exists($storagePath)) {
            File::makeDirectory($storagePath, 0775);
        }

        if (!File::exists($postPath)) {
            File::makeDirectory($postPath, 0775);
        }
        $postFiles = [];
        if ($request->hasFile('postFiles')) {

            foreach ($files as $file) {
                $fileID = Uuid::generate();
                if (in_array($file->getClientOriginalExtension(), array("jpg", "jpeg", "png", "svg", "bmp"))) {
                    $iconURL = 'post/'. $post->dirID. '/'. $fileID;
                } else {
                    if ($file->getClientOriginalExtension() == 'pdf') {
                        $iconURL = 'post/'. 'defaultIcon/pdf.jpg';
                    } else if (in_array($file->getClientOriginalExtension(), array("doc", "docx"))) {
                        $iconURL = 'post/'. 'defaultIcon/doc.jpg';
                    } else {
                        $iconURL = 'post/'. 'defaultIcon/other.jpg';
                    }
                }
                $postFile = PostFile::create([
                    'postID' => $postID,
                    'fileID' => $fileID,
                    'fileName' => $file->getClientOriginalName(),
                    'fileType' => $file->getClientOriginalExtension(),
                    'fileSize' => $file->getClientSize(),
                    'filePath' => 'post/'. $post->dirID. '/'. $fileID,
                    'iconURL'  => $iconURL,
                    'mimeType' => $file->getClientMimeType()
                ]);
                array_push($postFiles, PostFile::where('fileID', $fileID)->first());
                $path = $file->move($postPath, $fileID);
            }
        }
        return response()->json([
            'message' => "All Post files have been uploaded",
            'postFiles' => $postFiles
        ],  200);
    }

    /*
     * Get Post file by fileID
     */
    public function getFile($fileID)
    {
        /* Remove auth check to get image access */
        //$user = AuthHelpers\getAuthorizedUser();

        $file = PostFile::where('fileID', $fileID)->first();

        return response()->file(storage_path('app/public'). '/'. $file->filePath, [
            'Content-Type'          => $file->mimeType,
            "Content-Disposition'   => 'attachment; filename='$file->fileName''",
        ]);

    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id)
    {
        $this->post->findOrFail($id)->delete();

        return response()->json([
            'message' => 'Post successfully deleted'
        ], 200);
    }
}
