<?php

namespace App\Http\Controllers;

use App\CompanyInfo;
use App\Helpers\AuthHelpers;
use App\Helpers\GeneralHelpers;
use App\Helpers\DBHelpers;
use App\Models\CompanyInfo\Profile;
use App\Models\CompanyInfo\ProfileUploads;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Exception;
use Illuminate\Support\Facades\File;
use Webpatser\Uuid\Uuid;
use App\Models\Subcontractors\Subcontractors;
use App\Models\TradeBuilderGuide\TradeBuilderGuide;
use App\Models\Post;

class ProfileController extends Controller
{
//------------------jonas start
    public function getCompanyInfoByHbgID($hbg_id)
    {
        $cpis = TradeBuilderGuide::with([
            'subcontractor', 'subcontractor.user', 'subcontractor.user.company_info'
            ])->where('hbg_id', $hbg_id)
            ->get();
        return response()->json($cpis);
    }
//------------------jonas end
    public function getProfileInfo()
    {
        $user       = AuthHelpers\getAuthorizedUser();

        $info       = User::with(['trade_info', 'trade_info.subcontractor_trades', 'posts'])
                      ->join('company_info as ci', 'users.id', '=', 'ci.userID')
                      ->join('profile as p', 'users.id', '=', 'p.userID')
                      ->select
                      (
                          'users.id','users.is_terms_accepted','users.references', 'users.associations', 'users.certifications', 'users.is_tutorial_viewed',
                          'p.*',
                          'ci.*', 'ci.created_at as user_registered_on',
                          'ci.uuid'
                      )
                      ->where('users.id', $user->id)
                      ->first();

        $uploads    = ProfileUploads
                        ::where('userID', $user->id)
                        ->limit(10)
                        ->select('isLogo', 'fileID', 'mimeType', 'profileID', 'fileURL')
                        ->get();
        $posts = Post
                ::with(['user', 'user.company_info', 'post_files'])
                ->where('userID', $info['userID'])
                ->orderBy('created_at', 'desc')
                ->take(10)
                ->get();
        foreach($posts as $post) {
            $post['ago'] = $post->updated_at->diffForHumans();
        }
        return response()->json(['profileInfo' => $info, 'profileUploads' => $uploads, 'postsInfo' => $posts], 200);
    }

    public function getProfileForCompany($slug, Request $request)
    {
        $info       = User::with(['trade_info', 'trade_info.subcontractor_trades', 'posts'])
                        ->join('company_info as ci', 'users.id', '=', 'ci.userID')
                        ->join('profile as p', 'p.userID', '=', 'users.id')
                        ->select
                        (
                            'users.id','users.is_terms_accepted','users.references', 'users.associations', 'users.certifications', 'users.is_tutorial_viewed',
                            'ci.uuid', 'users.email',
                            'p.*',
                            'ci.*', 'ci.created_at as user_registered_on'
                        )
                        ->where('ci.companySlug', "$slug")
                        ->first();


        if (! $info->count()) {
            $message = "User tried to retrieve info for non-existing company with slug: {$slug}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        $uploads    = ProfileUploads
                        ::where('userID', $info->userID)
                        ->limit(10)
                        ->select('isLogo', 'fileID', 'mimeType', 'profileID', 'fileURL')
                        ->get();
        $posts = Post
                ::with(['user', 'user.company_info', 'post_files'])
                ->where('userID', $info['userID'])
                ->orderBy('created_at', 'desc')
                ->take(10)
                ->get();
        foreach($posts as $post) {
            $post['ago'] = $post->updated_at->diffForHumans();
        }
        return response()->json(['profileInfo' => $info, 'profileUploads' => $uploads, 'postsInfo' => $posts], 200);
    }

    public function getProfileForCurrentUser(Request $request)
    {

        $user       = AuthHelpers\getAuthorizedUser();

        $info       = User
                        ::join('company_info as ci', 'users.id', '=', 'ci.userID')
                        ->join('profile as p', 'p.userID', '=', 'users.id')
                        ->select
                        (
                            'ci.uuid', 'users.email', 'users.is_terms_accepted','users.references', 'users.associations', 'users.certifications', 'users.is_tutorial_viewed',
                            'p.*',
                            'ci.*', 'ci.created_at as user_registered_on'
                        )
                        ->where('ci.userID', $user->id)
                        ->first();


        $uploads    = ProfileUploads
                        ::where('userID', $user->id)
                        ->limit(10)
                        ->select('isLogo', 'fileID', 'mimeType', 'profileID', 'fileURL')
                        ->get();

        return response()->json(['profileInfo' => $info, 'profileUploads' => $uploads], 200);
    }

    /*
     * For updating user status
     */
    public function updateUserStatus(Request $request)
    {
        $user       = AuthHelpers\getAuthorizedUser();

        $mainUser = User::findOrFail($user->id);
        $termsaccepted = $request->is_terms_accepted;
        $tutorialviewed =$request->is_tutorial_viewed;
        if($termsaccepted!=null) {
            $updateUser = array('is_terms_accepted' => $request->is_terms_accepted);
            $mainUser->update($updateUser);
        }
        if($tutorialviewed!=null) {
            $updateUser = array('is_tutorial_viewed' => $request->is_tutorial_viewed);
            $mainUser->update($updateUser);
        }
        return response()->json(['result' => 'success', 'message' => 'User status updated'], 200);
    }

    public function updateProfileInfoValidator(array $data)
    {

        $rules = [

            // Company Info
            'company'               =>           'required|string|min:2|max:100',
            'contactFirstName'      =>           'required|string|min:2|max:100',
            'contactLastName'       =>           'required|string|min:2|max:100',
            'address'               =>           'required|string|min:2|max:100',
            'address2'              =>           'nullable|string|min:2|max:100',
            'city'                  =>           'required|string|min:2|max:100',
            'postalCode'            =>           'required|string|min:2|max:100',
            'province'              =>           'required|string|min:2|max:100',
            'phoneNumber'           =>           'required|string|max:190',
            'phoneNumber2'          =>           'required|string|max:190',

            // Profile Info
            'aboutUs'               =>           'nullable|string|min:2|max:1000',
            'website'               =>           'nullable|url|min:2|max:191',
            'twitterURL'            =>           'nullable|url|min:2|max:191',
            'facebookURL'           =>           'nullable|url|min:2|max:191',
            'instagramURL'          =>           'nullable|url|min:2|max:191',
            'pinterestURL'          =>           'nullable|url|min:2|max:191',
            'youtube'               =>           'nullable|url|min:2|max:191',
            'googlePlusURL'         =>           'nullable|url|min:2|max:191',

        ];

        $messages = [
            'contactFirstName.required'     =>  'You must enter a value for First Name',
            'contactLastName.required'      =>  'You must enter a value for Last Name',
            'company.required'              =>  'You must enter a value for the Company name',
            'address.required'              =>  'You must enter a value for the Address',
            'postalCode.required'           =>  'You must enter a value for the Postal Code',
            'city.required'                 =>  'City cannot be empty',
            'province.required'             =>  'Province cannot be empty',
            'phoneNumber.required'          =>  'You must enter a Phone Number',
            'phoneNumber2.required'         =>  'You must enter a Phone Number for the contact',

            'twitterURL.url'                => 'The URL entered for Twitter is invalid',
            'facebookURL.url'               => 'The URL entered for Facebook is invalid',
            'instagramURL.url'              => 'The URL entered for Instagram is invalid',
            'pinterestURL.url'              => 'The URL entered for Pinterest is invalid',
            'youtubeURL.url'                => 'The URL entered for Youtube is invalid',
            'googlePlusURL.url'             => 'The URL entered for Google Plus is invalid',

        ];

        return Validator::make($data, $rules, $messages);
    }

    public function updateProfile(Request $request)
    {
        $user       = AuthHelpers\getAuthorizedUser();

        $ci         = CompanyInfo::find($user->id);
        $pi         = Profile::find($user->id);

        $validator = $this->updateProfileInfoValidator($request->all());

        if ($validator->fails()) { return DBHelpers\returnValidationError($validator); }

        $ciCols     = $ci->getFillable();
        $piCols     = $pi->getFillable();

        $ciInput    = $request->only($ciCols);
        $piInput    = $request->only($piCols);

        foreach ($ciCols as $column) {
            if (array_key_exists($column, $ciInput) && !empty($ciInput[$column])
                && $column !== 'isSubcontractor'
                && $column !== 'isBuilder'
                && $column !== 'isAdmin'
                && $column !== 'userType'
                && $column !== 'trade'
                && $column !== 'password_confirmation'
                && $column !== 'password'
                && $column !== 'token') {
                if ($column !== 'company') {
                    $ci->$column = $ciInput[$column];
                }
                else {
                    if ($ci->company != $ciInput['company']) {
                        $ci->$column = $ciInput[$column];
                        $ci->companySlug  = str_slug(str_replace('&', 'and', $ciInput['company']));
                        $count = CompanyInfo::where('companySlug', 'LIKE', '%' . $ciInput['company']. '%')->count();
                        if ($count > 0) {
                            ++$count;
                            $ci->companySlug = $ci->companySlug . $count;
                        }
                    }
                }
            }
        }

        foreach ($piCols as $column) {
            if (array_key_exists($column, $piInput) && !empty($piInput[$column])
                && $column !== 'isSubcontractor'
                && $column !== 'isBuilder'
                && $column !== 'isAdmin'
                && $column !== 'userType'
                && $column !== 'trade'
                && $column !== 'password_confirmation'
                && $column !== 'password'
                && $column !== 'token')
            {
                    $pi->$column = $piInput[$column];
            }
        }

        try
        {
            $ci->save();
            $pi->save();
        }
        catch (Exception $ex)
        {
            return DBHelpers\returnDBError($ex);
        }

        return response()->json(['message' => "The Profile Information for {$ci->company} has been updated!"], 200);
    }

    public function updateImageValidator(array $data)
    {

        $rules = [
            'image'     => 'required|image|mimes:jpeg,png,jpg,svg|max:10240',
        ];

        $messages = [
            'image.required'    => 'You must choose an Image',
            'image.image'       => 'You can only upload images',
            'image.mimes'       => 'The Image format must be one of: jpeg/jpg, png, or svg',
            'image.max'         => 'The Image cannot be larger than 10mb',
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function updateImage(Request $request)
    {
        $fileID         = $request->fileID;
        $profileID      = $request->profileID;

        /*if ($type == null || $type != 'profile' && $type !== 'logo') {
            $message = "User tried to update Images with illegal type: {$type}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        } else*/
        if ($fileID == null) {
            $message = "User tried to update Images without File ID";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }
        if ($profileID == null) {
            $message = "User tried to update Images without Upload ID for FileID: ${fileID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }
        if (AuthHelpers\checkIfUserCanManageProfileInfo() == false) {
            $message = "User tried to illegally update Profile Image for FileID: {$fileID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        $validator = $this->updateImageValidator($request->all());

        if ($validator->fails()) {
            return DBHelpers\returnValidationError($validator);
        }

        $upload =   ProfileUploads::where('fileID', $fileID)->where('profileID', $profileID)->first();
        $ci     =   User
                    ::join('company_info as ci', 'users.id', '=', 'ci.userID')
                    ->where('users.id', AuthHelpers\getAuthorizedUserID())
                    ->select('ci.uuid', 'ci.*')
                    ->first();


        $storagePath = storage_path('app/public');
        $userPath = ($storagePath . '/' . $ci->uuid);
        $uploadDir = ($userPath . '/profile');

        // Make new folder for uploads
        if (!File::exists($storagePath)) {
            File::makeDirectory($storagePath, 0775);
        }
        if (!File::exists($userPath)) {
            File::makeDirectory($userPath, 0775);
        }
        if (!File::exists($uploadDir)) {
            File::makeDirectory($uploadDir, 0775);
        }

        if ($upload->isLogo == 1) {
            $fileNameAndType = "logo.$upload->fileType";
        }
        else {
            $fileNameAndType = "$upload->fileID.$upload->fileType";
        }

        if ($upload->filePath != null) {
            $fp = $uploadDir . '/' . $fileNameAndType;

            if (File::exists($fp)) {
                unlink($fp);
            }
        }

        $file       = $request->file('image');

        $name       = $file->getClientOriginalName();
        $ext        = $file->getClientOriginalExtension();

        if ($upload->isLogo == 1) {
            $fileNameAndType    = "logo.$upload->fileType";
            $compInfo           = CompanyInfo::find($ci->userID);
            $compInfo->logoURL  = "$ci->uuid/profile/$fileNameAndType";

            try
            {
                $compInfo->save();
            }
            catch (Exception $ex)
            {
                return DBHelpers\returnDBError($ex);
            }
        }
        else {
            $fileNameAndType = "$upload->fileID.$ext";
        }

        $path                       = $file->move($uploadDir, $fileNameAndType);

        $upload->fileName           = $name;
        //$upload->fileID             = Uuid::generate();
        $upload->fileType           = $ext;
        $upload->mimeType           = $file->getClientMimeType();
        $upload->filePath           = $uploadDir;
        $upload->fileSize           = GeneralHelpers\getFileSize($file->getClientSize());
        $upload->fileURL            = "$ci->uuid/profile/$fileNameAndType";

        $upload->save();

        return response()->json(['message' => "The image has been updated", 'profileUploads' => $upload],  200);

    }

    public function deleteImage(Request $request)
    {
        $fileID         = $request->fileID;
        $profileID      = $request->profileID;

        if ($fileID == null) {
            $message = "User tried to update Images without File ID";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }
        if ($profileID == null) {
            $message = "User tried to update Images without Upload ID for FileID: ${fileID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }
        if (AuthHelpers\checkIfUserCanManageProfileInfo() == false) {
            $message = "User tried to illegally update Profile Image for FileID: {$fileID}";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        $upload     =   ProfileUploads::where('fileID', $fileID)->where('profileID', $profileID)->first();
        $ci         =   User
                        ::join('company_info as ci', 'users.id', '=', 'ci.userID')
                        ->where('users.id', AuthHelpers\getAuthorizedUserID())
                        ->select('ci.uuid', 'ci.*')
                        ->first();


        $storagePath = storage_path("app/public/{$ci->uuid}/profile");

        if ($upload->isLogo == 1) {
            $fileNameAndType = "logo.$upload->fileType";
        }
        else {
            $fileNameAndType = "$upload->fileID.$upload->fileType";
        }

        if ($upload->filePath != null) {
            $fp = $storagePath . '/' . $fileNameAndType;
            if (File::exists($fp)) {
                unlink($fp);

                $upload->fileName       = null;
                $upload->filePath       = null;
                $upload->mimeType       = null;
                $upload->fileType       = null;
                $upload->fileSize       = null;
                $upload->fileURL        = null;

                if ($upload->isLogo == 1) {
                    $upload->fileURL = "assets/images/no-image-found.png";
                }

                try
                {
                    $upload->save();
                }

                catch(Exception $ex)
                {
                    return DBHelpers\returnDBError($ex);
                }
            }
        }


        return response()->json(['message' => 'The image has been deleted', 'profileUploads' => $upload], 200);


    }
}
