<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\ResetsPasswords;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Helpers\AuthHelpers;
use App\Helpers\DBHelpers;
class PasswordController extends Controller
{
    use ResetsPasswords;

    public function __construct()
    {
        $this->broker = 'users';
    }

    // TODO: PASSWORD & PWD CONFIRMATION REGEX

    public function passwordChangeValidator(array $data) {
        $rules = [
            'email'             => 'required|string|email|max:255',
            'oldPassword'       => 'required|string|min:4',
            'password'          => 'required|string|min:4|confirmed',
        ];

        $messages = [
            'email.required'            =>  'Email is required',
            'oldPassword.required'      =>  'Old Password cannot be empty',
            'password.required'         =>  'New Password cannot be empty',
            'oldPassword.min'           => 'The password cannot be less than 4 characters',
            'password.min'              => 'The password cannot be less than 4 characters',
            'password.confirmed'        => 'Passwords do not match'
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function changePassword(Request $request)
    {

        $thisUser   = JWTAuth::parseToken()->authenticate();
        $user       = User::find($thisUser->id);

        $credentials = $request->only(['email', 'oldPassword', 'password_confirmation', 'password']);
        $validate = $this->passwordChangeValidator($credentials);

        if ($validate->fails())
        {
            return DBHelpers\returnValidationError($validate);
        }

        $newPassword = Hash::make($request->password);

        if ($request->email == $user->email) {
            if(Hash::check($request->oldPassword, $user->password)) {
                $user->password = $newPassword;
                $user->save();
                return response()->json(['message' => 'Your password has been changed'], 200);
            }
            else {
                $message = "User tried to change password but entered incorrect password";
                AuthHelpers\LogAndNoResponse($message, $request);
                return response()->json(['message' => 'Your credentials are incorrect'], 400);
            }
        }
        else {
            $message = "User tried to change password but entered incorrect email {$request->email}";
            AuthHelpers\LogAndNoResponse($message, $request);
            return response()->json(['message' => 'Your email is incorrect'], 400);

        }
    }

    public function changeEmailValidator(array $data) {
        $rules = [
            'email'             => 'required|string|email|max:255',
            'oldEmail'          => 'required|string|email|max:255',
            'password'          => 'required|string|min:8|confirmed',
        ];

        $messages = [
            'email.required'            =>  'Email is required',
            'oldEmail.required'         =>  'Current email is required',
            'password.required'         =>  'New Password cannot be empty',
            'password.min'              => 'The password cannot be less than 8 characters',
            'password.confirmed'        => 'Passwords do not match'
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function changeEmail(Request $request)
    {

        $thisUser   = JWTAuth::parseToken()->authenticate();
        $user       = User::find($thisUser->id);

        $credentials = $request->only(['oldEmail', 'password', 'password_confirmation', 'email']);
        $validate = $this->changeEmailValidator($credentials);

        if ($validate->fails())
        {
            return DBHelpers\returnValidationError($validate);
        }

        if ($request->oldEmail == $user->email) {
            if(Hash::check($request->password, $user->password)) {
                $user->email = $request->email;
                $user->save();
                return response()->json(['message' => 'Your Email has been changed'], 200);
            }
            else {
                $message = "User tried to change password but entered incorrect password";
                AuthHelpers\LogAndNoResponse($message, $request);
                return response()->json(['message' => 'Your credentials are incorrect'], 400);
            }
        }
        else {
            $message = "User tried to change email but entered incorrect email";
            AuthHelpers\LogAndNoResponse($message, $request);
            return response()->json(['message' => 'Your email is incorrect'], 400);

        }
    }
}