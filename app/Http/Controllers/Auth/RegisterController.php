<?php

namespace App\Http\Controllers\Auth;

use App\Jobs\CreateChatForEachProjectOnSignUp;
use App\Mail\AccountVerification;
use App\Mail\TestEmail;
use App\Models\Builder\Builders;
use App\Models\CompanyInfo\Profile;
use App\Models\CompanyInfo\ProfileUploads;
use App\Models\Subcontractors\Subcontractors;
use App\User;
use App\CompanyInfo;
use App\Helpers\AuthHelpers;
use App\Helpers\GeneralHelpers;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Mail\Markdown;
use Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Exception;
use App\Helpers\DBHelpers;
use Illuminate\Support\Str;
use Webpatser\Uuid\Uuid;
use Bouncer;
use Illuminate\Support\Facades\Password;
use App\Jobs\SendAccountVerificationEmail;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function createValidator(array $data)
    {

        // TODO: PASSWORD & PWD CONFIRMATION REGEX
        $rules = [
            'email'                 =>           'required|email|unique:users',
            'password'              =>           'required|string|min:2|max:100|confirmed',
            'userType'              =>           'required|string|min:2|max:50',
           // 'trade'                 =>           'numeric:unique: subcontractor_trades, id',
            'company'               =>           'required|string|min:2|max:100',
            'contactFirstName'      =>           'required|string|min:2|max:100',
            'contactLastName'       =>           'required|string|min:2|max:100',
            'address'               =>           'required|string|min:2|max:100',
            'address2'              =>           'nullable|string|min:2|max:100',
            'city'                  =>           'required|string|min:2|max:100',
            'postalCode'            =>           'required|string|min:2|max:100',
            'province'              =>           'required|string|min:2|max:100',
            'phoneNumber'           =>           'required|string|max:190',
            'phoneNumber2'           =>           'required|string|max:190',
        ];

        $messages = [
            'email.required'                =>  'Email cannot be empty',
            'password.required'             =>  'Password cannot be empty',
            'contactFirstName.required'     =>  'First Name cannot be empty',
            'contactLastName.required'      =>  'Last Name cannot be empty',
            'company.required'              =>  'Company cannot be empty',
            'address.required'              =>  'Address cannot be empty',
            'city.required'                 =>  'City cannot be empty',
            'province.required'             =>  'Province cannot be empty',
            //'postalCode.required'         =>  'Postal Code cannot be empty',
            'phoneNumber.required'          =>  'Phone Number cannot be empty',
            'phoneNumber2.required'          => 'Contacts Number cannot be empty',
            //'password.confirmed'          => 'The passwords you entered do not match',
            'email.unique'                  => 'The email you entered has already been taken',
            //'postalCode.regex'            => 'You must enter a Postal Code in the format A0A0A0',
            'password.min'                  => 'The password cannot be less than 6 characters'
        ];

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {

        //Mail::to('harneilbrar@gmail.com')->send(new TestEmail());


        $user = new User;
        $sc = new Subcontractors();
        $builder = new Builders();

        $userInput = $request->only($user->getFillable());
        $userColumns = $user->getFillable();

        $company = new CompanyInfo;
        $companyInput = $request->only($company->getFillable());
        $companyColumns = $company->getFillable();

        $validator = $this->createValidator(array_merge($userInput, $companyInput));

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 422);
        }

        $userInput['password'] = bcrypt($userInput['password']);
        $userType = $request->input('userType');


//        if ($userType !== "subContractor") {
//            return DBHelpers\returnInvalidInput("User Type ". $userType);
//        }
//        elseif ($userType !== "builder") {
//            return DBHelpers\returnInvalidInput("User Type ". $userType);
//        }

        foreach ($userColumns as $column) {
            if (array_key_exists($column, $userInput) && !empty($userInput[$column])
                && $column !== 'isSubcontractor'
                && $column !== 'isBuilder'
                && $column !== 'isAdmin'
                && $column !== 'userType'
                && $column !== 'trade'
                && $column !== 'password_confirmation'
                && $column !== 'token') {
                $user->$column = $userInput[$column];
            }
        }
        foreach ($companyColumns as $column2) {
            if (array_key_exists($column2, $companyInput)
                && !empty($companyInput[$column2])
                && $column2 !== 'isAdmin'
                && $column2 !== 'isBuilder'
                && $column2 !== 'userType'
                && $column2 !== 'trade'
                && $column2 !== 'isSubcontractor'
                && $column2 !== 'password_confirmation'
                && $column2 !== 'token') {
                $company->$column2 = $companyInput[$column2];
            }
        }

        try {
            $user->token = Password::getRepository()->createNewToken();
            $company->companySlug = str_slug(str_replace('&', 'and', $companyInput['company']));
            $company->uuid = GeneralHelpers\generateUniqueString("company_info", "uuid", 16);
            $company->logoURL = 'assets/images/no-image-found.png';
            if ($userType === 'subcontractor') {
                if ($trade = $request->input('trade')) {
                    $user->isSubContractor = 1;
                    $user->userType = 'subcontractor';
                    $sc->scTradeID = $trade;

                    $user->save();
                    $sc->userID = $user->id;
                    $sc->save();
                    $user->assign('subcontractor');
                    //\Bouncer::assign('subcontractor')->to($user);

                }
            } else if ($userType === 'builder') {
                $user->isBuilder = 1;
                $user->userType = 'builder';
                $user->save();
                $user->assign('builder');

                //TODO:: UNCOMMENT AFTER NEXT MIGRATION
                $builder->userID = $user->id;
                $builder->save();
            } else {
                return response()->json(['error' => 'You are doing something you are unauthorized to do'], 401);
            }
        } catch (Exception $exception) {
            if ($user) {
                $user->delete();
            }
            if ($sc) {
                $sc->delete();
            }
            if ($builder) {
                $builder->delete();
            }
            return DBHelpers\returnDBError($exception);
        }
        try {
            $company->userID = $user->id;
            $count = CompanyInfo::where('companySlug', 'LIKE', '%' . $company->companySlug . '%')->count();
            if ($count > 0) {
                ++$count;
                $company->companySlug = $company->companySlug . $count;
            }
            $company->save();

            $profile = new Profile();
            $profile->userID = $user->id;
            $profile->save();

            for ($i = 0; $i < 8; $i++) {
                $profUploads = new ProfileUploads();
                $profUploads->userID = $user->id;
                $profUploads->profileID = $user->id;
                $profUploads->isLogo = 0;
                $profUploads->fileID = Uuid::generate();
                $profUploads->save();
            }
            $profUploads = new ProfileUploads();
            $profUploads->userID = $user->id;
            $profUploads->profileID = $user->id;
            $profUploads->isLogo = 1;
            $profUploads->fileURL = "assets/images/no-image-found.png";
            $profUploads->fileID = Uuid::generate();
            $profUploads->save();

            $profUploads = new ProfileUploads();
            $profUploads->userID = $user->id;
            $profUploads->profileID = $user->id;
            $profUploads->isLogo = 2;
            $profUploads->fileID = Uuid::generate();
            $profUploads->save();

           // Bouncer::ownedVia('userID');
            Bouncer::allow($user)->toManage($profile);

            event(new Registered($user));
            // dispatch(((new SendAccountVerificationEmail($user))
            //         ->onQueue('emails')
            //         ->delay(Carbon::now()->addSeconds(30))));

            /*Bouncer::ownedVia('userID');
            Bouncer::allow($user)->toManage($company);*/

            //AuthHelpers\giveProfilePermissionToUserByProfile($profile);
            //AuthHelpers\giveCompanyPermissionToUserByCompanyID($user->id);
        } catch (Exception $exception) {

            //return $exception;


            if ($sc && $sc->id != 0) {
                $sc->delete();
            }
            if ($builder && $builder->id != 0) {
                $builder->delete();
            }
            if ($user && $user->id != 0) {
                $user->delete();
            }
            //return response(['message' => 'An Error has occurred. Please try again later '], 400);
            // TODO:: CHANGE ON DEPLOY
            //return $exception;
            return DBHelpers\returnDBError($exception);
        }


        return response()->json(['message' => 'Registration successful. Check your email to validate your email'], 200);


    }

    public function verify($uuid, $token, Request $request)
    {

        $ci     = CompanyInfo::where('uuid', $uuid)->first();
        $user   = User::where('token', $token)->where('id', $ci->userID)->first();


        if ($user->count()) {

            if ($user->verified == 1)
            {
                return 'Your account has already been verified';
            }
            $user->verified = 1;

            try {
                $user->save();

                if ($user->isA('subcontractor')) {

                    $job = (((new CreateChatForEachProjectOnSignUp($user))
                                ->onQueue('queue2')
                                ->delay(10)));

                    $this->dispatch($job);
                }
            } catch (Exception $ex) {
                return DBHelpers\returnDBError($ex);
            }
        } else {
            $message = "User tried to verify account (uuid: $uuid) with token $token";
            return AuthHelpers\buildUnAuthorizedResponseAndLog($message, $request);
        }

        return view('accountVerified');

    }

}
