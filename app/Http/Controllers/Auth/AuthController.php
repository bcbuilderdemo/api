<?php

namespace App\Http\Controllers\Auth;

use App\Bid;
use App\Models\Bids\BuilderBidsOut;
use App\Models\Bids\SCBidsOut;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\AuthHelpers;
use App\Helpers\DBHelpers;
use App\Helpers\GeneralHelpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Silber\Bouncer\Bouncer;
use Silber\Bouncer\Database\Queries\Roles;
use Symfony\Component\Console\Helper\Table;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class  AuthController extends Controller
{
    public function getPermissions()
    {
        $user       = AuthHelpers\getAuthorizedUser();
        $array      = array();

        $roles      = DB
                      ::table('roles')
                      ->join('assigned_roles as ar', 'roles.id', '=', 'role_id')
                      ->where('ar.entity_id', $user->id)
                      ->get();

        for ($i=0; $i<$roles->count(); $i++) {
            $array[$i] = $roles[$i]->name;
        }


        return response(['data' => $array],200);

    }

    public function checkIfSCOrBuilderCanViewOrManageBidOut($bidID, Request $request)
    {
            $userType       = $request->userType;
            $permission     = $request->permission;


            $userTypeArray          = ['builder', 'sc'];
            $permissionArray        = ['view', 'manage'];

            if ( ! in_array($userType, $userTypeArray)) {
                return AuthHelpers\returnCustomAuthorizedResponse('validated', false, 200);
            }
            else if ( ! in_array($permission, $permissionArray)) {
                return AuthHelpers\returnCustomAuthorizedResponse('validated', false, 200);

            }

            $user           = AuthHelpers\getAuthorizedUser();

            if ($userType == 'builder')
            {
                $bidOut  = ($permission = 'view')   ?   Bid::find($bidID)
                                                    :   BuilderBidsOut::where('bidID', $bidID)->first();
            } else if ($userType == 'sc')
            {
                $bidOut  = ($permission = 'view') ?     BuilderBidsOut::where('bidID', $bidID)->first()
                                                  :     SCBidsOut::where('bidID', $bidID)->first();
            }

            $validated      = $user->can($permission, $bidOut) ? true : false;

            // TODO: LOG UNAUTHORIZED
            return AuthHelpers\returnCustomAuthorizedResponse('validated', $validated, 200);

    }

    public function checkProjectOwnership(Request $request)
    {
        $project    = Project::find($request->id);
        $permission = (Auth::user()->can('manage', $project)) ? "AL" : "NAL";
        // TODO:: LOG USER TIRED VIEWING UNAUTHORIZED PROJECT
        // if $permission == "NAL" { log }
        return response()->json(['type' => $permission], 200);
    }

    public function startupRefreshToken() {
        $current = JWTAuth::getToken();

        if ( ! $current) {
            return response()->json(['error' => 'No Token Provided'], 401);
        }


        //$token = JWTAuth::refresh($current);
        // Changed to fix token_expired on start up
        $token = JWTAuth::refresh();
        $user       = AuthHelpers\getAuthorizedUser();
        $array      = array();

        // Lazy way to refresh the token for a builder so new project modal doesnt open on refresh
        // TODO: FIND A BETTER WAY TO REFRESH CLAIMS/PAYLOAD
        if ($user->isA('builder')) {
            $customClaims = [
                'isSC'          => $user->isSubContractor,
                'isBuilder'     => $user->isBuilder,
                'isAdmin'       => $user->isAdmin,
                'userType'      => $user->type,
                'hasProject'    => true
            ];
            $token = JWTAuth::fromUser($user, $customClaims);
        }

        $roles      =   DB
                        ::table('roles')
                        ->join('assigned_roles as ar', 'roles.id', '=', 'role_id')
                        ->where('ar.entity_id', $user->id)
                        ->get();


        for ($i=0; $i<$roles->count(); $i++) {
            $array[$i] = $roles[$i]->name;
        }


        return response()->json(['token' => $token, 'permissions' => $array], 200);
    }
}
