<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;

use Illuminate\Support\Facades\App;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Helpers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    public function loginValidator(array $data) {
        $rules = [
            'email'             => 'required|string|email|max:255',
            'password'          => 'required|string|min:6',
            'userType'          => "required|in:builder,subcontractor"
        ];

        $messages = [
            'email.required'            =>  'Email cannot be empty',
            'password.required'         =>  'Password cannot be empty',
            'password.min'              =>  'The password cannot be less than 6 characters'
        ];

        return Validator::make($data, $rules, $messages);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password', 'userType');

        $validate = $this->loginValidator($credentials);

        if ($validate->fails()) {

            return response()->json(['error' => $validate->errors()->first()], 401);
        }

        $user = User::where('email', $credentials['email'])->first();

        if ( ! $user) {
            return response()->json(['error' => 'Your email or password is incorrect'], 422);
        }

        if ($user->verified == 0) {
            return response()->json(['error' => 'Your account has not been verified. Please check your email and verify your account'], 422);
        }

        if ( ! Hash::check($credentials['password'], $user->password)) {
            return response()->json(['error' => 'Your email or password is incorrect'], 422);
        }

        if ($user->userType !== $credentials['userType']) {
            if ($credentials['userType'] === 'builder') {
                return response()->json(['error' => 'Builders can only log in'], 422);
            }
            else {
                return response()->json(['error' => 'Subcontractors can only log in'], 422);
            }
        }

        try {
            $userType = '';
            $hasProject = true;

            if      ($user->isSubContractor) { $userType = 'sc'; }
            else if ($user->isAdmin)         { $userType = 'admin'; }

            else if ($user->isBuilder) {
                $userType = 'builder';
                // If Builder hasn't created a project on first login open the new project modal
                if ($user->hasCreatedAProject != 1) {
                    $hasProject = false;
                    $user->hasCreatedAProject = 1;
                    $user->save();
                }
            }

            $customClaims = [
                                'isSC'          => $user->isSubContractor,
                                'isBuilder'     => $user->isBuilder,
                                'isAdmin'       => $user->isAdmin,
                                'userType'      => $userType,
                                'hasProject'    => $hasProject
                            ];
            $token = JWTAuth::fromUser($user, $customClaims);
        } catch (JWTException $ex) {
            //dd($ex);
            return response()->json(['error' => 'An error has occurred. Please try again later'], 500);
        }
        return response()->json(['token' => $token], 200);
    }

    public function refreshToken() {
        $current = JWTAuth::getToken();
        $user = Helpers\AuthHelpers\getAuthorizedUser();

        if ( ! $current) {
            return response()->json(['error' => 'No Token Provided'], 401);
        }

        /*if ($user->isA('builder') && $user->hasCreatedAProject != 1) {
            $user->hasCreatedAProject = 1;
            $user->save();

        }*/

        //$token = JWTAuth::refresh($current);
        // Changed to fix problem where token_invalid
        $token = JWTAuth::refresh();

        return response()->json(['token' => $token], 200);
    }
}
