<?php

namespace App\Mail;

use App\Bid;
use App\CompanyInfo;
use App\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendBBOEmailMailable extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $bbo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bbo)
    {
        $this->bbo = $bbo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $bci         = CompanyInfo::find($this->bbo->builderID);
        $sci         = CompanyInfo::find($this->bbo->scID);
        //$project     = Project::find($this->bbo->projectID);


        return $this
                ->markdown('emails.NewBBOEmail')
                ->subject("$bci->company has sent you a new bid")
                ->with
                ([
                    'bciCompany'    => $bci->company,
                    'sciCompany'    => $sci->company,
                    'created_at'    => $this->bbo->created_at
                ]);
    }
}
