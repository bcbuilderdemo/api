<?php

namespace App\Mail;

use App\CompanyInfo;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountVerification extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    //public $maxTries = 10;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $ci         = CompanyInfo::find($this->user->id);

        return $this
            ->markdown('emails.accountverification')
            ->subject("Please verify your BC Builder account")
            ->with
            ([
                'token'     => $this->user->token,
                'email'     => $this->user->email,
                'company'   => $ci->company,
                'uuid'      => $ci->uuid,
            ]);
    }

}
