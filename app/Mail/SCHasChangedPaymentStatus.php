<?php

namespace App\Mail;

use App\CompanyInfo;
use App\Models\Accounting\Accounting;
use App\Models\Accounting\Accounting_Payments_Made;
use App\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SCHasChangedPaymentStatus extends Mailable
{
    use Queueable, SerializesModels;


    protected $act;
    protected $apm;
    protected $project;
    protected $sci;
    protected $bci;

    public function __construct(Accounting $act, Accounting_Payments_Made $apm)
    {
        $this->act      = $act;
        $this->apm      = $apm;

        $this->project  = Project::find($this->act->projectID);
        $this->sci      = CompanyInfo::find($this->act->scID);
        $this->bci      = CompanyInfo::find($this->act->builderID);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->markdown('emails.accounting.PaymentStatusUpdate')
                ->subject("{$this->sci->company} changed the payment status for {$this->project->address} {$this->project->address2}")
                ->with([
                    'bci'                       => $this->bci->company,
                    'sci'                       => $this->sci->company,
                    'projectAddress'            => $this->project->address,
                    'projectAddress2'           => $this->project->address2,
                    'projectCity'               => $this->project->city,
                    'projectProvince'           => $this->project->postalCode,
                    'amountPaid'                => $this->apm->amountPaid,
                    'scConfirmedPayment'        => $this->apm->scConfirmedPayment,
                    'scConfirmedPaymentOn'      => $this->apm->scConfirmedPaymentOn,
                    'scUnConfirmedPaymentOn'    => $this->apm->scUnconfirmedPaymentOn,
                    'status'                    => $this->apm->status,
                    'paymentMadeOn'             => $this->apm->paymentMadeOn,
                ]);
    }
}
