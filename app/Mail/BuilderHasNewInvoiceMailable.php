<?php

namespace App\Mail;

use App\Bid;
use App\CompanyInfo;
use App\Models\Accounting\Accounting;
use App\Models\Accounting\Accounting_Payments_Made;
use App\Models\Bids\BuilderBidsOut;
use App\Models\Bids\SCBidsOut;
use App\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BuilderHasNewInvoiceMailable extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $bid;
    protected $act;
    protected $bbo;

    protected $bci;
    protected $sci;
    protected $project;

    public function __construct(Bid $bid, BuilderBidsOut $bbo, SCBidsOut $scbo, Accounting $act)
    {
        $this->bid    = $bid;
        $this->act    = $act;
        $this->bbo    = $bbo;
        $this->scbo   = $scbo;

        $this->bci     = CompanyInfo::find($scbo->builderID);
        $this->sci     = CompanyInfo::find($scbo->scID);
        $this->project = Project::find($scbo->projectID);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->markdown('emails.accounting.BuilderNewInvoiceEmail')
                ->subject("Congratulations on accepting the Quote from {$this->sci->company}. You have a new invoice!")
                ->with
                ([
                    'bciCompany'            => $this->bci->company,
                    'sciCompany'            => $this->sci->company,
                    'projectAddress'        => $this->project->address,
                    'projectAddress2'       => $this->project->address2,
                    'projectCity'           => $this->project->city,
                    'projectProvince'       => $this->project->postalCode,
                    'totalAmount'           => $this->act->totalAmount
                ]);
    }
}
