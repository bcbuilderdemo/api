<?php

namespace App\Mail;

use App\CompanyInfo;
use App\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Helpers;

class SenSCBOEmailMailable extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $scbo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($scbo)
    {
        $this->scbo = $scbo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $project    = Project::find($this->scbo->projectID);
        $sc         = CompanyInfo::find($this->scbo->scID);
        $builder    = CompanyInfo::find($this->scbo->builderID);
        $feURL      = Helpers\GeneralHelpers\getFrontEndURL();
        //$URL        = $feURL.'/projects/'.$this->scbo->id.'/bids/'.$this->scbo->bidID.'/builder/quote';

        return $this
                ->markdown('emails.NewSCBOEmail')
                ->subject('You have a new Quote from '. $sc->company)
                ->with([
                    //'id'                => $this->scbo->id,
                    //'bidID'             => $this->scbo->bidID,
                    //'builderID'         => $this->scbo->builderID,
                    //'scID'              => $this->scbo->scID,
                    'projectID'         => $project->id,
                    'projectAddress'    => $project->address,
                    'projectAddress2'   => $project->address2,
                    'projectCity'       => $project->city,
                    'projectProvince'   => $project->province,
                    'sciCompany'        => $sc->company,
                    'bciCompany'        => $builder->company,
                    //'URL'               => $URL,
                ]);
    }
}
