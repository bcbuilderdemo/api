<?php

namespace App\Mail;

use App\Bid;
use App\CompanyInfo;
use App\Models\Accounting\Accounting;
use App\Models\Accounting\Accounting_Payments_Made;
use App\Models\Bids\BuilderBidsOut;
use App\Models\Bids\SCBidsOut;
use App\Project;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BuilderHasChangedSCBOStatusMailable extends Mailable
{
    use Queueable, SerializesModels;

    protected $bid;
    protected $act;
    protected $bbo;

    protected $bci;
    protected $sci;
    protected $scbo;
    protected $project;
    //protected $user;

    public function __construct(Bid $bid, BuilderBidsOut $bbo, SCBidsOut $scbo, Accounting $act)
    {
        $this->bid    = $bid;
        $this->act    = $act;
        $this->bbo    = $bbo;
        $this->scbo   = $scbo;

        $this->bci     = CompanyInfo::find($scbo->builderID);
        $this->sci     = CompanyInfo::find($scbo->scID);
        $this->project = Project::find($scbo->projectID);
        //$this->user   = User::find($scbo->builderID);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        if ($this->scbo->builderAccepted == 1) {
            $status = "accepted";
            $date   = $this->scbo->builder_accepted_at;
        }
        else {
            $status = "rejected";
            $date   = $this->scbo->builder_rejected_at;
        }
        return $this
                ->markdown('emails.SCBO.SCBOStatusChange')
                ->subject("{$this->bci->company} has {$this->scbo->status} your quote")
                ->with
                ([
                    'bciCompany'            => $this->bci->company,
                    'sciCompany'            => $this->sci->company,
                    'projectAddress'        => $this->project->address,
                    'projectAddress2'       => $this->project->address2,
                    'projectCity'           => $this->project->city,
                    'projectProvince'       => $this->project->postalCode,
                    'status'                => $status,
                    'date'                  => $date
                ]);
    }
}
