<?php
namespace App\Helpers\GeneralHelpers;
use Carbon\Carbon;
use DB;
function getDateTime() {
    return Carbon::now('America/Vancouver');
}

function returnResponse($type, $message, $code) {
    return response()->json([$type => $message], $code);
}

function getFrontEndURL() {
    if (config('app.debug')) {
        return "http://localhost:4200";
    }
    else {
        return "http://www.neilbrar.com/bcb";
    }
}

function getFileSize($bytes)
{
    $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

    for ($i = 0; $bytes > 1024; $i++) {
        $bytes /= 1024;
    }
    return round($bytes, 2) . ' ' . $units[$i];
}


function generateUniqueString($table, $col, $chars = 16){

    $unique = false;

    // Store tested results in array to not test them again
    $tested = [];

    do{

        // Generate random string of characters
        $random = str_random($chars);

        // Check if it's already testing
        // If so, don't query the database again
        if( in_array($random, $tested) ){
            continue;
        }

        // Check if it is unique in the database
        $count = DB::table($table)->where($col, '=', $random)->count();

        // Store the random character in the tested array
        // To keep track which ones are already tested
        $tested[] = $random;

        // String appears to be unique
        if( $count == 0){
            // Set unique to true to break the loop
            $unique = true;
        }

        // If unique is still false at this point
        // it will just repeat all the steps until
        // it has generated a random string of characters

    }
    while(!$unique);


    return $random;
}

/*array_sort_by_column($array, 'date');


function array_sort_by_column(&$array, $column, $direction = SORT_ASC) {
    $reference_array = array();

    foreach($array as $key => $row) {
        $reference_array[$key] = $row[$column];
    }

    array_multisort($reference_array, $direction, $array);
}*/




