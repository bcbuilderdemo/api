<?php

namespace App\Helpers\DBHelpers;
use Exception;

function returnDBError(Exception $ex)
{
    if (config('app.debug')) {
        return response()->json(['error' => $ex], 500);
    }
    return response()->json(['error' => 'An error has occurred please try again'], 500);
}

function returnInvalidInput($input)
{
    if (config('app.debug')) {
        return response()->json(['error' => $input. ' is invalid'], 500);
    }
    return response()->json(['error' => 'You have entered invalid input'], 500);
}

function returnValidationError($validator) {
    return response()->json(['error' => $validator->errors()->first()], 422);
}

function returnDBErrorWithCustomMessage(Exception $ex, $message)
{
    if (config('app.debug')) {
        return response()->json(['error' => $ex], 500);
    }
    return response()->json(['error' => $message], 500);
}


