<?php

namespace App\Helpers\AuthHelpers;
use App\CompanyInfo;
use App\Models\Accounting\Accounting;
use App\Models\Bids\BuilderBidsOut;
use App\Bid;
use App\Models\Bids\SCBidsOut;
use App\Models\Chat\Chat;
use App\Models\CompanyInfo\Profile;
use App\Models\CompanyInfo\ProfileUploads;
use App\ReportUnauthorizedActivity;
use App\User;
use App\Models\Subcontractors\Subcontractors;
use App\Models\Uploads\FileUploads;
use App\Models\Uploads\Upload;
use Illuminate\Support\Facades\Auth;
use App\Project;
use App\Helpers\AuthHelpers;
use Illuminate\Http\Request;
use Bouncer;
use function Sodium\crypto_box_publickey_from_secretkey;

function getAuthorizedUser() {
    return Auth::user();
}

function getAuthorizedUserID() {
    return Auth::user()->id;
}

function returnUnAuthorizedResponse() {
    return response()->json(['error' => 'You are not authorized to do this'], 401);
}

function LogAndNoResponse($message, Request $request) {
    $method     = $request->route()->getActionMethod();
    $route      =  $request->path();
    $ip         = $request->ip();
    return AuthHelpers\returnUnAuthorizedResponseAndLog($message, $method, $route, $ip);
}

function log($message, $method, $route, $ip) {
    $report             = new ReportUnauthorizedActivity();
    $report->message    = $message;
    $report->method     = $method;
    $report->userID     = AuthHelpers\getAuthorizedUserID();
    $report->ip         = $ip;
    $report->route      = $route;
    $report->save();

}

function buildUnAuthorizedResponseAndLog($message, Request $request) {
    $method     = $request->route()->getActionMethod();
    $route      =  $request->path();
    $ip         = $request->ip();
   return AuthHelpers\returnUnAuthorizedResponseAndLog($message, $method, $route, $ip);
}

function returnUnAuthorizedResponseAndLog($message, $method, $route, $ip) {
    if (!config('app.debug')) {
        $res =  response()->json(['error' => 'You are not authorized to do this'], 401);
    }
    else
    {
        $res = response()->json(['error' => $message], 401);
    }
    $report             = new ReportUnauthorizedActivity();
    $report->message    = $message;
    $report->method     = $method;
    $report->userID     = AuthHelpers\getAuthorizedUserID();
    $report->ip         = $ip;
    $report->route      = $route;
    $report->save();
    return $res;

}

function buildResponseWithLogMessageAndResponseMessage($responseMessage, $logMessage, Request $request) {
    $method     = $request->route()->getActionMethod();
    $route      =  $request->path();
    $ip         = $request->ip();
    return AuthHelpers\saveLogMessageAndSendResponse($responseMessage, $logMessage, $method, $route, $ip);
}

function saveLogMessageAndSendResponse($responseMessage, $logMessage, $method, $route, $ip) {

    $res                = response()->json(['error' => $responseMessage], 401);
    $report             = new ReportUnauthorizedActivity();
    $report->message    = $logMessage;
    $report->method     = $method;
    $report->userID     = AuthHelpers\getAuthorizedUserID();
    $report->ip         = $ip;
    $report->route      = $route;
    $report->save();
    return $res;

}

function buildResponseWithMessageAndLog($message, Request $request) {
    $method     = $request->route()->getActionMethod();
    $route      =  $request->path();
    $ip         = $request->ip();
    return AuthHelpers\returnCustomMessageResponseAndLog($message, $method, $route, $ip);
}

function buildNotificationMarkedAsReadWithDebug($message) {

    if (config('app.debug')) {
        return response()->json(['message' => $message], 200);
    } else {
        return response()->json(['status' => true], 200);
    }
}

function buildNotificationErrorReadWithDebug($ex, $msg) {

    if (config('app.debug')) {
        return response()->json(['error' => $ex, 'error_msg' => $msg], 401);
    } else {
        return response()->json(['error' => 'Notification could not be marked as read'], 401);
    }
}



function returnCustomMessageResponseAndLog($message, $method, $route, $ip) {

    $res                = response()->json(['error' => $message], 401);
    $report             = new ReportUnauthorizedActivity();
    $report->message    = $message;
    $report->method     = $method;
    $report->userID     = AuthHelpers\getAuthorizedUserID();
    $report->ip         = $ip;
    $report->route      = $route;
    $report->save();
    return $res;

}

function returnUnAuthorizedResponseWithDebug($type) {
    if (config('app.debug')) {
        return response()->json(['error' => $type], 401);
    } else {
        return response()->json(['error' => 'You are not authorized to do this'], 401);
    }
}

function returnCustomAuthorizedResponse($type, $message, $code) {
    return response()->json([$type => $message], $code);
}

/********************************************
               CHECK PERMISSIONS
 *********************************************/
function checkProjectOwnershipByProjectID($id) {
    $user           = AuthHelpers\getAuthorizedUser();
    $project        = Project::find($id);
    $boolean        = $user->can('manage', $project);
    return $boolean ? true : false;

}

function checkBidOwnershipByBidID($id) {
    $user           = AuthHelpers\getAuthorizedUser();
    $bid            = Bid::find($id);
    return $user->can('manage', $bid) ? true : false;
}

function checkBBOOwnershipByBBOID($id) {
    $user           = AuthHelpers\getAuthorizedUser();
    $bid            = BuilderBidsOut::find($id);
    return $user->can('manage', $bid) ? true : false;
}


function checkFileUploadOwnershipByID($id) {
    $user           = AuthHelpers\getAuthorizedUser();
    $file           = FileUploads::find($id);
    return $user->can('manage', $file) ? true : false;
}


function checkFileUploadOwnershipByFile(FileUploads $file) {
    $user           = AuthHelpers\getAuthorizedUser();
    return $user->can('manage', $file) ? true : false;
}

function checkIfSCCanManageSCBidOutByBid($bidOut) {
    $user   = AuthHelpers\getAuthorizedUser();
    return $user->can('manage', $bidOut) ? true : false;
}

function checkIfBuilderCanViewSCBidOutByBid($bidOut) {
    $user   = AuthHelpers\getAuthorizedUser();
    return $user->can('view', $bidOut) ? true : false;
}

// Jan 23: Added where clause to check for users ID
function checkIfBuilderCanManageOrSCCanViewBuilderBidOutByBidID($bidID) {
    $user   = AuthHelpers\getAuthorizedUser();
    if      ($user->userType == 'subcontractor')
    {
        $bid    = Bid::where('id', $bidID)->where('subcontractorID', $user->id)->first();
        //return $user->can('view', $bid) ? true : false;
        return true;
    }
    else if ($user->userType == 'builder')
    {
        $bid    = Bid::where('id', $bidID)->where('builderID', $user->id)->first();
        return true;
        //return $user->can('manage', $bid) ? true : false;
    }
}



/*
 *
 * Commented out Jan 23 b/c you cant just check for BID ID
 *
function checkIfSCCanManageSCBidOutByBidID($bidID) {
    $scbo   = SCBidsOut::where('bidID', $bidID)->first();
    $user   = AuthHelpers\getAuthorizedUser();
    return $user->can('manage', $scbo) ? true : false;
}
*/

function checkIfSCCanManageSCBidOutBySCBOID($scboID) {
    $scbo   = SCBidsOut::find($scboID);
    $user   = AuthHelpers\getAuthorizedUser();
    return $user->can('manage', $scbo) ? true : false;
}

function checkIfSCCanViewBuilderBidOutByBidID($bidID) {
    $scbo   = Bid::where('id', $bidID)->first();
    $user   = AuthHelpers\getAuthorizedUser();
   /* return $user->can('view', $scbo) ? true : false;*/
   return true;
}

function checkIfSCCanViewBuilderBidOutByBidIDAndProjectID($bidID, $projectID) {
    $scbo   = BuilderBidsOut::where('bidID', $bidID)->where('projectID', $projectID)->first();
    $user   = AuthHelpers\getAuthorizedUser();
    return $user->can('view', $scbo) ? true : false;
}


function checkIfSCCanViewBuilderBidOutByBBOID($bboID) {
    $scbo   = BuilderBidsOut::find($bboID);
    $user   = AuthHelpers\getAuthorizedUser();
    return $user->can('view', $scbo) ? true : false;
}

// Jan 23: Added where clause to check for user id
function checkIfBuilderCanViewSCBidOutByBidID($bidID) {
    $user   = AuthHelpers\getAuthorizedUser();
    $scbo   = SCBidsOut::where('bidID', $bidID)->where('builderID', $user->id)->first();
    return $user->can('view', $scbo) ? true : false;
}

function checkIfBuilderCanViewSCBidOutBySCBOID($scboID) {
    $scbo   = SCBidsOut::find($scboID);
    $user   = AuthHelpers\getAuthorizedUser();
    return $user->can('view', $scbo) ? true : false;
}


function checkIfSCCanViewProjectByProjectID($projectID) {
    $project = Project::find($projectID);
    $sc      = AuthHelpers\getAuthorizedUser();
    return $sc->can('view', $project) ? true : false;
}

function checkIfBuilderCanManageProjectByProjectID($projectID) {
    $project = Project::find($projectID);
    $builder = AuthHelpers\getAuthorizedUser();
    return $builder->can('manage', $project) ? true : false;
}

function checkIfBuilderOrSCCanViewAccountingByID($accountingID) {
    $accounting         = Accounting::find($accountingID);
    $user               = AuthHelpers\getAuthorizedUser();
    return $user->can('view', $accounting) ? true : false;
}

// Jan 23: May need to change this and check by builder or sc id
function checkIfBuilderOrSCCanViewAccountingByProjectIDAndscID($projectID, $scID) {
    $accounting         = Accounting::where('projectID', $projectID)->where('scID', $scID)->first();
    $user               = AuthHelpers\getAuthorizedUser();
    return $user->can('view', $accounting) ? true : false;
}


function checkIfUserCanManageAccountingByAccountingIDAndUserObject($accountingID, $user) {
    $accounting         = Accounting::find($accountingID);
    return $user->can('manage', $accounting) ? true : false;
}

function checkIfUserCanManageProfileInfo() {
    $user       = AuthHelpers\getAuthorizedUser();
    $pi         = Profile::find($user->id);
    return $user->can('manage', $pi) ? true : false;
}

function checkIfUserCanManageCompanyInfo() {
    $user       = AuthHelpers\getAuthorizedUser();
    $ci         = CompanyInfo::find($user->id);
    return $user->can('manage', $ci) ? true : false;
}


function checkIfBuilderOrSCCanManageChatByChatUUID($chatUUID) {
    $user            = AuthHelpers\getAuthorizedUser();
    $chat            = Chat::where('uuid', $chatUUID)->first();
    return $user->can('manage', $chat) ? true : false;
}
/********************************************
                GIVE PERMISSIONS
 *********************************************/

// Doesnt work
/*function  giveCompanyAndProfilePermissionsToUserByObjects($company, $profile) {
    Bouncer::ownedVia('userID');
    $user           = AuthHelpers\getAuthorizedUser();
    Bouncer::allow($user)->toManage($company);
    Bouncer::allow($user)->toManage($profile);
}*/

function  giveProfilePermissionToUserByProfile(Profile $profile) {
    //Bouncer::ownedVia('userID');
    $user           = AuthHelpers\getAuthorizedUser();
    //$profile        = Profile::find($profileID);
    Bouncer::allow($user)->toManage($profile);
}

function  giveCompanyPermissionToUserByCompanyID($profileID) {
    Bouncer::ownedVia('userID');
    $user           = AuthHelpers\getAuthorizedUser();
    $ci             = CompanyInfo::find($profileID);
    Bouncer::allow($user)->toManage($ci);
}

function  giveUploadPermissionToUser(Upload $upload) {
    $user           = AuthHelpers\getAuthorizedUser();
    Bouncer::allow($user)->toManage($upload);
}

function  giveBidPermissionToBuilderByBid(Bid $bid) {
    $user           = AuthHelpers\getAuthorizedUser();
    Bouncer::allow($user)->toManage($bid);
}

function giveFileUploadsPermissionToUser(FileUploads $upload) {
    $user           = AuthHelpers\getAuthorizedUser();
    Bouncer::allow($user)->toManage($upload);
    //return true;
}

function giveBuilderAndSCBidOutPermissions($bidOut) {
    $builder    = User::find($bidOut->builderID);
    $sc         = User::find($bidOut->scID);
    Bouncer::allow($builder)->toManage($bidOut);
    Bouncer::allow($sc)->to('view', $bidOut);
}

function giveSCBidOutPermissionsToBuilderAndSC($bidOut) {
    $builder    = User::find($bidOut->builderID);
    $sc         = User::find($bidOut->scID);
    Bouncer::allow($sc)->toManage($bidOut);
    Bouncer::allow($builder)->to('view', $bidOut);
}

function giveSCProjectViewPermissionByProjectID($projectID, $scID) {
    $project       = Project::find($projectID);
    $sc            = User::find($scID);
    Bouncer::allow($sc)->to('view', $project);
}

function giveAccountingOwnershipToBuilderAndSCByID($accountingID, $builderID, $scID) {
    $accounting         = Accounting::find($accountingID);
    $builder            = User::find($builderID);
    $sc                 = User::find($scID);
    Bouncer::allow($builder)->toManage($accounting);
    Bouncer::allow($sc)->toManage($accounting);
}

function giveChatOwnershipToBuilderIDAndSCIDByChat(Chat $chat, $builderID, $scID) {
    $builder            = User::find($builderID);
    $sc                 = User::find($scID);
    Bouncer::allow($builder)->toManage($chat);
    Bouncer::allow($sc)->toManage($chat);
}

/********************************************
            REVOKE PERMISSIONS
 *********************************************/

function revokeSCProjectViewPermissionByProjectID($projectID) {
    $project       = Project::find($projectID);
    $sc            = AuthHelpers\getAuthorizedUser();
    Bouncer::disallow($sc)->to('view', $project);
}





