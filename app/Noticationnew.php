<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Noticationnew extends Model
{

    protected $table = 'noticationnew';
    protected $fillable = ['read_at'];
    
}