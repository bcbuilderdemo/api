<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\models\HomeBuildersGuide\HomeBuildersGuide;
use App\Bid;
use App\Models\Uploads\ProjectFile;
class Subproject extends Model
{
    protected $table = 'subprojects';

    protected $fillable = [
        'project_id',
        'hbg_id',
        'is_filled',
        'is_available',
        'comments',
        'description',
        'is_trade',
        'start_subproject_date',
        'bid_close_date'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hbg()
    {
        return $this->belongsTo(HomeBuildersGuide::class, 'hbg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function bid()
    {
        return $this->hasOne(Bid::class, 'subprojectID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function project_files()
    {
        return $this->hasMany(ProjectFile::class, 'projectID');
    }
}
