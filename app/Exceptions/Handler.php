<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
Use Response;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\JWTAuth;
Use Tymon\JWTAuth\Exceptions\JWTException;
use App\Helpers\DBHelpers;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        /*if ($exception instanceof TokenExpiredException)
        {
            return Response::json(['error' => 'Token Expired'],$exception->getStatusCode());
        }
        else if ($exception instanceof TokenBlacklistedException)
        {
            return Response::json(['error' => 'Token Blacklisted'],$exception->getStatusCode());
        }
        else if ($exception instanceof TokenInvalidException)
        {
            return Response::json(['error' => 'Token Invalid'],$exception->getStatusCode());
        }*/
        if ($exception instanceof ModelNotFoundException) {
            return response()->json(['message' => 'Not Found'], 404);
        }

        if($exception instanceof JWTAuth)
        {
            return DBHelpers\returnDBError($exception);
            //return Response::json(['error' => 'TOKEN ERROR'], $exception->getStatusCode());
        }
        else if ($request->ajax() || $request->wantsJson()) {
            return response()->json(
                $this->getJsonMessage($exception),
                $this->getExceptionHTTPStatusCode($exception)
            );
        }
        return parent::render($request, $exception);
    }

    protected function getJsonMessage($e)
    {
        // You may add in the code, but it's duplication
        return [
            'status' => 'false',
            'message' => $e->getMessage()
        ];
    }

    protected function getExceptionHTTPStatusCode($e)
    {
        // Not all Exceptions have a http status code
        // We will give Error 500 if none found
        return method_exists($e, 'getStatusCode') ?
            $e->getStatusCode() : 500;
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $request->expectsJson()
                ? response()->json(['message' => 'Unauthenticated.'], 401)
                : redirect()->guest(route('login'));
    }
}
